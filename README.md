# README

Este repositório contém os arquivos relativos ao protótipo construído para o sistema HandsInHome.

O HandsInHome foi um sistema proposto como uma interface gestual para o controle
de um sistema de domótica, o HouseHub. 

O sistema foi descontinuado, devido ao fim do projeto HouseHub e à descontinuidade 
do OpenNI (biblioteca utilizada pelo projeto para interação gestual).
