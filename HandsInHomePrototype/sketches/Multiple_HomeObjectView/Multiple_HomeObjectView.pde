import java.awt.Point;


PImage lampOn;
PImage lampOff;
PImage background;

BlockService servLampOn;
BlockService servLampOff;

////ListView container;
CarouselListContainer container;
ListLayout horizontalLayout;
ListLayout verticalLayout;

boolean dragged;

HorizontalAligner horAligner;
VerticalAligner verAligner;

void setup(){
  dragged = false;
  
  size(640,480);
  lampOn = loadImage("images/lampadaLigada.png");
  lampOff = loadImage("images/lampadaDesligada.png");
  background = loadImage("images/background.png");
  background.resize(this.width,this.height);
  
  servLampOn = new BlockService("ligar", lampOn);
  servLampOff = new BlockService("desligar", lampOff);
  
  horizontalLayout = new ListHorizontalLayout();
  verticalLayout = new ListVerticalLayout();
  
  container = new CarouselListContainer(20,height/4, 400, 500);
  addNodes(container);
}

void addNodes(Container container){
    container.addComponent(makeNodeView());
    container.addComponent(makeNodeView());
    container.addComponent(makeNodeView());
    container.addComponent(makeNodeView());
    container.addComponent(makeNodeView());
}

HomeNodeView makeNodeView(){
  HomeNodeView nodeView = new HomeNodeView(0,0,100,100);
  nodeView.setImage(lampOff);
  nodeView.addServiceButton(servLampOn);
  nodeView.addServiceButton(servLampOff);
  nodeView.setServicesLayout(verticalLayout);
  return nodeView;
}

void draw(){
    background(background);
//////  image(servLampOn.getIcon(),50,height/2);
//////  image(servLampOff.getIcon(),250,height/2);
////  lampOnView.draw(this.g);
////  lampOffView.draw(this.g);
    container.draw(this.g);
}


void mouseMoved(){
//    if(container.isOver(mouseX,mouseY)){
//          for(Component comp : container.getComponents()){
//              HomeNodeView nodeV = (HomeNodeView)comp;
//              
//              Point currentLocal = convertToLocal(container,mouseX, mouseY);
//              Point previousLocal = convertToLocal(container,pmouseX, pmouseY);
//              
//              boolean isOver = nodeV.isOver((int)currentLocal.getX(), (int)currentLocal.getY());
//              boolean wasOver = nodeV.isOver((int)previousLocal.getX(), (int)previousLocal.getY());
//              if(isOver && !wasOver){
//                  nodeV.showServices();
//              }
//              else if(!isOver && wasOver){
//                  nodeV.hideServices();
//              }
//              
//              println("isOver " + isOver + " wasOver " + wasOver);
//          }
//    }

        Component currentNode  = container.getComponentAt(mouseX,  mouseY);
        Component previousNode = container.getComponentAt(pmouseX, pmouseY);
        
        //println("IsOver " + currentNode + " && wasOver " + previousNode );
        
        if(currentNode != previousNode){ //mouseEnter
            if(currentNode != null  && (currentNode instanceof HomeNodeView)){
              ((HomeNodeView)currentNode).showServices();
            }
            if(previousNode != null && (previousNode instanceof HomeNodeView)){
              ((HomeNodeView)previousNode).hideServices();
            }
        }
}

  
  protected Point convertToLocal(Component origin, int x, int y){
    return new Point(x - origin.getX(), y - origin.getY());
  }
  
void keyPressed(){
  if(key == CODED){
//      if(keyCode == RIGHT){
//        this.container.selectNext();
//      }
//      else if(keyCode == LEFT){
//        this.container.selectPrevious();
//      }
  }
  else{
      switch(key){
          case 'l':
            changeLayout();
            break;
          case 'a':
            changeHorAlignment(HorizontalAligner.HorizontalAlignment.Left);
            break;
          case 's':
            changeHorAlignment(HorizontalAligner.HorizontalAlignment.Center);
            break;
          case 'd':
            changeHorAlignment(HorizontalAligner.HorizontalAlignment.Right);
            break;
          case 'q':
            changeVerAlignment(VerticalAligner.VerticalAlignment.Top);
            break;
          case 'w':
            changeVerAlignment(VerticalAligner.VerticalAlignment.Center);
            break;
          case 'e':
            changeVerAlignment(VerticalAligner.VerticalAlignment.Bottom);
            break;
      }
      if(key == 'l'){
      }
      
  }
}

void changeHorAlignment(HorizontalAligner.HorizontalAlignment alignment){
    horAligner.setAlignment(alignment);
    println("ChangeAlignment to " + horAligner.getAlignment());
}
void changeVerAlignment(VerticalAligner.VerticalAlignment alignment){
    verAligner.setAlignment(alignment);
    println("ChangeAlignment to " + verAligner.getAlignment());
}

void changeLayout(){
////    if(container.getLayout() == horizontalLayout){
////        container.setLayout(verticalLayout);
////    }
////    else if(container.getLayout() == verticalLayout){
////        container.setLayout(horizontalLayout);
////    }
//    if(nodeView.getServicesLayout() == horizontalLayout){
//        nodeView.setServicesLayout(verticalLayout);
//    }
//    else if(nodeView.getServicesLayout() == verticalLayout){
//        nodeView.setServicesLayout(horizontalLayout);
//    }
}

void mousePressed(){
}

void mouseReleased(){
  if(mouseButton == LEFT){
//      if(!dragged){
//          Component comp = container.getComponentAt(mouseX, mouseY);
//          if(comp != null){
//              container.removeComponent(comp);
//          }
//          else{
//              container.addComponent(new ServiceButton(new BlockService("somebody", lampOn),0,0,100,100));
//          }
//      }
//      else{
//          dragged = false;
//      }
  }
}

void mouseDragged(){
  if(mouseButton == LEFT){
      dragged = true;
      container.setLocation(mouseX,mouseY);
//      nodeView.setLocation(mouseX,mouseY);
      
  }
}
