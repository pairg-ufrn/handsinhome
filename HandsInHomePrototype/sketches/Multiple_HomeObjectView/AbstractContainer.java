//package gui.components;
import java.util.LinkedList;
import java.awt.Point;
import processing.core.PGraphics;
import java.util.List;

public abstract class AbstractContainer extends AbstractComponent implements Container
{
  protected List<Component> components;
  
  public AbstractContainer(){
    this(0,0,0,0);
  }
  public AbstractContainer(int x, int y, int width, int height){
    super(x,y,width, height);
    components = createCollection();
  }
  
  public void addComponent(Component comp){
    components.add(comp);
    updateLayout();
  }
  public void removeComponent(Component comp){
    components.remove(comp);
    updateLayout();
  }
  public void clear(){
    components.clear();
    updateLayout();
  }
  public Iterable<Component> getComponents(){
    return components;
  }
  public int getComponentCount(){
    return components.size();
  }
  public boolean contains(Component comp){
    return components.contains(comp);
  }
  
  protected Component getComponent(int index){
    return this.components.get(index);
  }
  protected void addComponentAt(int index, Component comp){
    this.components.add(index, comp);
  }
  
  public Component getComponentAt(int x, int y){
    if(this.isOver(x,y)){
      Point localPos = this.convertToLocal(x,y);
      int localX = (int)localPos.getX();
      int localY = (int)localPos.getY();
      
      Component comp = null;
      for(Component c : this.getComponents()){
        if(comp == null){
          if(c instanceof Container){//TODO:(?) mover metodo getComponentAt para Component ???
            comp = ((Container)c).getComponentAt(localX,localY);
          }
          else{
//            System.out.println(c + ".isOver("+localX+","+localY+")?" + c.isOver(localX,localY));
            comp = (c.isOver(localX,localY)? c : null);
          }
        }
      }
      return (comp == null? this : comp);
    }
    
    return null;
  }
  
  protected Point convertToLocal(int x, int y){
    return new Point(x - this.getX(), y - this.getY());
  }
  
  protected List<Component> createCollection(){
    return new LinkedList<Component>();
  }
  public abstract void updateLayout();
  public abstract void draw(PGraphics graphics);
}

