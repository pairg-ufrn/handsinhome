public class VerticalAligner implements ComponentAligner{
    public enum VerticalAlignment{Top,Center,Bottom};
    
    private VerticalAlignment alignment;
    
    public VerticalAligner(){
        this(VerticalAlignment.Top);
    }
    public VerticalAligner(VerticalAlignment alignment){
        this.alignment = alignment;
    }
    
    public VerticalAlignment getAlignment(){return alignment;}
    public void setAlignment(VerticalAlignment alignment){this.alignment = alignment;}
    
    
    @Override
    public void align(int xOrigin, int yOrigin, Component comp){
        if(alignment == VerticalAlignment.Top){
            comp.setY(yOrigin);
        }
        else if(alignment == VerticalAlignment.Center){
            comp.setY(yOrigin - comp.getHeight()/2);
        }
        else if(alignment == VerticalAlignment.Bottom){
            comp.setY(yOrigin - comp.getHeight());
        }
    }

}
