public interface ComponentAligner{
    public void align(int xOrigin, int yOrigin, Component comp);
    
}
