//package gui.components;
import processing.core.PGraphics;

public class ListView extends AbstractContainer{
  private int gap = 10;
  
  ListLayout layout;
  
  public ListView(){
    this(0,0);
  }
  public ListView(int x, int y){
    super(x,y,0,0);
    layout = new ListHorizontalLayout();
  } 
  
  public void addComponentAt(int index, Component comp){
    if(index >= 0 && index < this.getComponentCount()){
      super.addComponentAt(index, comp);
      this.updateLayout();
    }
    else{
      this.addComponent(comp);
    }
  }
  
  public void setLayout(ListLayout layout){
      this.layout = layout;
  }
  public ListLayout getLayout(){ return this.layout;}
  
  public void updateLayout(){
    layout.doLayout(this, null);
  }
  public void draw(PGraphics graphics){ 
      graphics.pushMatrix();
          graphics.translate(this.getX(),this.getY());
          for(Component comp : this.getComponents()){
            comp.draw(graphics);
          }
      graphics.popMatrix();
  }
}

