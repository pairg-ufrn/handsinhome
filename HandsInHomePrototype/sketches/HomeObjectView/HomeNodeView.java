//package gui.home;

import java.awt.event.MouseEvent;
import java.awt.Point;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import processing.core.PGraphics;
import processing.core.PImage;

//import gui.components.CarouselListContainer;
//import gui.components.Component;
//import gui.components.ImageComponent;
//import gui.components.ListView;
//import gui.*;
//import home.BlockService;

public class HomeNodeView extends AbstractComponent {
    private CarouselListContainer servicesContainer;
    private ImageComponent nodeIcon;
    private CompositeAligner servicesAligner;
    
    private Map<BlockService, ServiceButton> serviceButtons;
    private boolean serviceIsActive;
      
    public HomeNodeView(int x, int y, int width, int height){
        super(x,y,width,height);
        serviceButtons = new HashMap<BlockService, ServiceButton>();
        serviceIsActive = false;
        
        servicesContainer = new CarouselListContainer(0, height,500,500);//FIXME: definir tamanho máximo para serviços
        nodeIcon = new ImageComponent(0,0,width,height);
        servicesAligner = new CompositeAligner();
        servicesAligner.getHorizontalAligner().setAlignment(HorizontalAligner.HorizontalAlignment.Center);
        servicesAligner.getVerticalAligner().setAlignment(VerticalAligner.VerticalAlignment.Top);
        
        System.out.println(servicesAligner.getHorizontalAligner().getAlignment());
        System.out.println(servicesAligner.getVerticalAligner().getAlignment());
        
    }
      
    public void addServiceButton(BlockService service){
        ServiceButton servButton = new ServiceButton(service, 0,0,this.getWidth()/2, this.getHeight()/2);
        serviceButtons.put(service, servButton);
        servicesContainer.addComponent(servButton);
    }
    public void removeServiceButton(BlockService service){
        servicesContainer.removeComponent(serviceButtons.get(service));
        serviceButtons.remove(service);
    }
    public Collection<ServiceButton> getServiceButtons(){ return serviceButtons.values();}
    
    public PImage getImage() { return nodeIcon.getImage();}
    public ListLayout getServicesLayout(){ return servicesContainer.getLayout();}
  
    public void setImage(PImage img) { nodeIcon.setImage(img);}
    public void setServicesLayout(ListLayout layout){ servicesContainer.setLayout(layout);}
    
    @Override
    public boolean isOver(int x, int y){
        Point ptLocal = convertToLocal(x,y);
        return nodeIcon.isOver((int)ptLocal.getX(),(int)ptLocal.getY()) || (serviceIsActive && servicesContainer.isOver((int)ptLocal.getX(),(int)ptLocal.getY()));
    }
    
    private Point convertToLocal(int x, int y){
        return new Point(x - this.getX(), y - this.getY()); //FIXME: criar metodo auxiliar
    }
    
    public void showServices(){  
        System.out.println("Show!");
        serviceIsActive = true;
    }
    public void hideServices(){  
        System.out.println("Hide!");
        serviceIsActive = false;
    }
      
    public void draw(PGraphics graphic) {
        graphic.pushMatrix();
            graphic.translate(this.getX(), this.getY());
            nodeIcon.draw(graphic);
            if(serviceIsActive){
              //Desenha serviços abaixo do objeto
//              servicesContainer.setY(this.getHeight());
              servicesAligner.align(this.getWidth()/2, this.getHeight(), servicesContainer);
              servicesContainer.draw(graphic);
              graphic.ellipse(0, this.getHeight(), 10,10);
            }
        graphic.popMatrix();
    }
}

