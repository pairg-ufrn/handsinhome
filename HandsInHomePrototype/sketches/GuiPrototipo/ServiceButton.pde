import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Map.Entry;

public class ServiceButton{
   /*private BlockService service;
   
   public BlockService getService(){ return service;}
   public void setService(BlockService service){ this.service = service;}
  
  public ServiceButton(int x, int y, int radius){
    super(x,y,radius);
    this.service = null;
  }
  
  public void cursorMoved(int x, int y){
    super.cursorMoved(x,y);
    if(serviceIsActive){
      
      for(CircularButton btn : serviceButtons.values()){
        int newX = x - this.getBoundBox().getX();
        int newY = y - this.getBoundBox().getY();
        btn.cursorMoved(newX,newY);
      }
    }
  }
  
  public void cursorPressed(int x, int y){
    super.cursorPressed(x,y);
    if(serviceIsActive){
      for(CircularButton btn : serviceButtons.values()){
        int newX = x - this.getBoundBox().getX();
        int newY = y - this.getBoundBox().getY();
        btn.cursorPressed(newX,newY);
        
        println(newX + ", " + newY);
      }
    }
  }
  
  public void onCursorEnter(){
    super.onCursorEnter();
    this.showServices();
  }
  public void onCursorLeave(){
    super.onCursorLeave();
    //this.hideServices();
  }
  
  protected void showServices(){
    int dist = this.getBoundBox().getRadius() * 2;
    double deltaAngle =  (Math.PI*2)/serviceButtons.size();
    double angle = Math.PI/2;
    
    for (Entry<BlockService, CircularButton> entry : serviceButtons.entrySet())
    {
      int x = (int)(dist * Math.sin(angle));
      int y = (int)(dist * Math.cos(angle));
      
      CircularButton btn = entry.getValue();
      btn.setPosition(x,y);
      btn.setBackgroundImage(entry.getKey().getIcon());
      
      angle += deltaAngle;
    }

    serviceIsActive = true;
  }
  protected void hideServices(){
    
  }
  
  public void draw(PGraphics graphic){
    if(serviceIsActive){
      graphic.pushMatrix();
        translate(this.getBoundBox().getX(), this.getBoundBox().getY());
        for(CircularButton btn : serviceButtons.values()){
          btn.draw(graphic);
        }
      graphic.popMatrix();
    }
    super.draw(graphic);
  }*/
  
}
