public class BlockService{
  private String name;
  private PImage icon;
  
  public String getName(){ return name;}
  public PImage getIcon(){ return icon;}
  
  public void setName(String name){ this.name = name;}
  public void setIcon(PImage icon){ this.icon = icon;}
  
  public BlockService(String name){
    this.name = name;
    icon = null;
  }
  
}
