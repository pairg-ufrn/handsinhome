import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Map.Entry;

import processing.core.PGraphics;

public class ObjectButton extends CircularButton {
  Map<BlockService, CircularButton> serviceButtons;
  boolean serviceIsActive;
  float showingServicesProgress;
  double angle;

  public ObjectButton(int x, int y, int radius) {
    super(x, y, radius);
    serviceButtons = new HashMap<BlockService, CircularButton>();
    serviceIsActive = false;
    showingServicesProgress = 1;
  }

  public void addServiceButton(BlockService service) {
    System.out.println("add " + service.getName());
    serviceButtons.put(service, new CircularButton(0, 0, this.getBoundBox()
      .getRadius() / 2));
    if (serviceIsActive) {
      this.showServices();
    }
  }

  public void removeServiceButton(BlockService service) {
    serviceButtons.remove(service);
    if (serviceIsActive) {
      this.showServices();
    }
  }

  public Collection<CircularButton> getServiceButtons() {
    return serviceButtons.values();
  }

  public void cursorMoved(int x, int y) {
    super.cursorMoved(x, y);
    if (serviceIsActive) {

      for (CircularButton btn : serviceButtons.values()) {
        int newX = x - this.getBoundBox().getX();
        int newY = y - this.getBoundBox().getY();
        btn.cursorMoved(newX, newY);
      }
    }
  }

  public void cursorPressed(int x, int y) {
    super.cursorPressed(x, y);
    if (serviceIsActive) {
      for (CircularButton btn : serviceButtons.values()) {
        int newX = x - this.getBoundBox().getX();
        int newY = y - this.getBoundBox().getY();
        btn.cursorPressed(newX, newY);

        System.out.println(newX + ", " + newY);
      }
    }
  }

  public void onCursorEnter() {
    super.onCursorEnter();
    this.showServices();
  }

  public void onCursorLeave() {
    super.onCursorLeave();
    println("Passou aqui");
    //this.hideServices();
  }

  protected void showServices() {
    int dist = this.getBoundBox().getRadius() * 2;
    double deltaAngle = (Math.PI * 2) / serviceButtons.size();
    deltaAngle = (deltaAngle > (Math.PI / 4) ? (Math.PI / 4) : deltaAngle);

    angle = Math.PI/2;

    for (Entry<BlockService, CircularButton> entry : serviceButtons
				.entrySet()) {
      int x = (int) (dist * -Math.cos(angle));
      int y = (int) (dist * -Math.sin(angle));
      CircularButton btn = entry.getValue();
      btn.setPosition(x, y);
      btn.setBackgroundImage(entry.getKey().getIcon());

      angle += deltaAngle;
    }

    serviceIsActive = true;
  }

  protected void hideServices() {
  }

  public void draw(PGraphics graphic) {
    if (serviceIsActive) {
      graphic.pushMatrix();
      println(this.getBoundBox().getX() +"---"+ this.getBoundBox().getY());
      graphic.translate(this.getBoundBox().getX(), this.getBoundBox().getY());
      for (CircularButton btn : serviceButtons.values()) {
        drawServiceButton(graphic, btn);
      }

      if (showingServicesProgress > 0) {
        showingServicesProgress -= 0.03;
      }
      graphic.popMatrix();
    }
    super.draw(graphic);
  }

  public void drawServiceButton(PGraphics graphic, CircularButton btn) {

    graphic.pushMatrix();

    int middleX = btn.getBoundBox().getX()/ 2;
    int middleY = btn.getBoundBox().getY()/ 2;
    float dist = (float) Math.sqrt((middleX*middleX) + (middleY*middleY));

    
    graphic.translate(middleX, middleY);
    float angleRot = (float) (showingServicesProgress * (-Math.PI));
    graphic.rotate(angleRot);

    btn.setPosition(middleX, middleY);
    // graphic.rotate(angleRot);
    // btn.setPosition(0, dist);
    btn.draw(graphic);
    btn.setPosition(middleX * 2, middleY * 2);
//    graphic.translate(this.getBoundBox().getX(), this.getBoundBox()
//        .getY());
    graphic.rotate((float)Math.PI);

    graphic.popMatrix();
  }
  // public void drawServiceButton(PGraphics graphic, CircularButton btn) {
  // float angle;
  // float porc;
  //
  // //graphic.pushMatrix();
  // int middleX = (int) (Math.cos(angle) * (dist / 2));
  // int middleY = (int) (Math.sin(angle) * (dist / 2));
  // graphic.translate(middleX, middleY);
  // float angleRot = (float) (porc * (Math.PI / 2));
  //
  // int x = (int) (Math.cos(angleRot) * (dist / 2));
  // int y = (int) (Math.sin(angleRot) * (dist / 2));
  //
  // graphic.ellipse(x, y, 25, 25);
  //
  // if (porc < 1.0) {
  // porc += 0.03;
  // }
  //
  // graphic.popMatrix();
  //
  // }
}

