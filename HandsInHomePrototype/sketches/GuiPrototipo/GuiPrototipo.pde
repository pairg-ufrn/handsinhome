ObjectButton btn;
ObjectButton btn2;

void setup() {
  size(640, 480);
  btn = new ObjectButton(this.width/4, this.height/2, 50);
  btn2 = new ObjectButton(3* this.width/4, this.height/2, 50);

  PImage portaAberta = loadImage("portaAberta.png");
  PImage portaFechada = loadImage("portaFechada.jpg");
  println(portaAberta);

  btn.setBackgroundImage(portaFechada);

  BlockService abrir = new BlockService("abrir");
  abrir.setIcon(portaAberta);
  btn.addServiceButton(abrir);

  BlockService fechar = new BlockService("fechar");
  fechar.setIcon(portaFechada);
  btn.addServiceButton(fechar);

  btn.addServiceButton( new BlockService("olho mágico"));
  btn.addServiceButton( new BlockService("trancar"));
  btn.addServiceButton( new BlockService("destrancar"));
  btn.addServiceButton( new BlockService("outro"));


  btn2.addServiceButton( new BlockService("olho mágico"));
  //btn2.addServiceButton( new BlockService("trancar"));
//  btn2.addServiceButton( new BlockService("destrancar"));
//  btn2.addServiceButton( new BlockService("outro"));
}

void draw() {
  background(200);
  btn.draw(this.g);
  btn2.draw(this.g);
}

void mouseMoved() {
  btn.cursorMoved(mouseX, mouseY);
  btn2.cursorMoved(mouseX, mouseY);
}

void mousePressed() {
  btn.cursorPressed(mouseX, mouseY);
  btn2.cursorPressed(mouseX, mouseY);
}

