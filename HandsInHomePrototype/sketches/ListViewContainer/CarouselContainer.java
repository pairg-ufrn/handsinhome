import processing.core.PGraphics;
import java.util.List;
import java.util.ArrayList;

public class CarouselContainer extends BasicComponent{
  protected List<Component> components;
  protected List<Component> visibleComponents;
  protected int minGap, currentGap;
  protected int currentIndex;
  protected int maxWidth, maxHeight;
  
  public int getMaxWidth(){return this.maxWidth;}
  public int getMaxHeight(){return this.maxHeight;}
  public int getGap(){return this.currentGap;}
  public int getMinimumGap(){return this.minGap;}
  public int getCurrentIndex(){return this.currentIndex;}
  
  //@Override
  public void setX(int x){
    if(x != this.getX()){
      super.setX(x);
      this.repositionComponents();
    }
  }
  public void setMaxWidth(int Width){
    if(Width != this.maxWidth){
      this.maxWidth = Width;
      this.repositionComponents();
    }
  }
  public void setMaxHeight(int Height){this.maxHeight = Height;}
  public void setMinimumGap(int MinimumGapValue){
    if(MinimumGapValue != this.minGap){
      this.minGap = MinimumGapValue;
      this.repositionComponents();
    }
  }
  public void setCurrentIndex(int index){
    if(index >= 0 && index < this.components.size() && index!=currentIndex){
      this.currentIndex = index;
      this.repositionComponents();
    }
  }
  
  public CarouselContainer(int X,int Y, int MaxWidth, int MaxHeight){
    super(X,Y,0,0);
    this.minGap = 15;
    this.maxWidth = MaxWidth;
    this.maxHeight = MaxHeight;
    this.components = new ArrayList<Component>();
    this.visibleComponents = new ArrayList<Component>();
    this.currentIndex = 0;
  }
  
  public void addComponent(Component comp){
    this.components.add(comp);
    repositionComponents();
    System.out.println(components.size() + "  " + visibleComponents.size());
  }
  
  protected void repositionComponents(){
    if(!this.components.isEmpty()){
      visibleComponents.clear();
      visibleComponents.add(components.get(currentIndex));
      int currentWidth = this.components.get(currentIndex).getWidth();
      
      int count = this.components.size();
      for(int i=1; i <= count/2; ++i){
        int leftIndex  = (currentIndex - i >= 0 ? currentIndex - i : currentIndex + count - i);
        int rightIndex = (currentIndex + i < count ? currentIndex + i : currentIndex - count + i);
        
        System.out.println("l: " + leftIndex + " r: " +  rightIndex);
        visibleComponents.add(components.get(leftIndex));
        if(leftIndex != rightIndex){
          visibleComponents.add(components.get(rightIndex));
        }
//        if(insertIfFit(components.get(leftIndex), 0, currentWidth)){
//          currentWidth += components.get(leftIndex).getWidth() + minGap;
//          if(leftIndex != rightIndex){
//            if(insertIfFit(components.get(rightIndex), visibleComponents.size()-1,currentWidth)){
//              
//            }
//            else{
//            }
//          }
//        }
//        else{
//        }
      }
      this.currentGap = minGap;
    }
  }
  private boolean insertIfFit(Component comp, int index, int currentWidth){
    if(comp.getWidth() + currentWidth + this.minGap <= this.getMaxWidth()){
      visibleComponents.add(index, comp);
      return true;
    }
    return false;
  }
  private boolean hasSpace(int width, Component comp){
    return (width + minGap + comp.getWidth() < this.getMaxWidth());
  }
  
  public void draw(PGraphics graphics){
    graphics.pushStyle();
    graphics.noFill();
    graphics.rect(this.getX(),this.getY(), this.getWidth(), this.getHeight());
    graphics.popStyle();
    
    graphics.pushMatrix();
      graphics.translate(this.getX(), this.getY());
      
      for(Component comp : visibleComponents){
        comp.draw(graphics);
        graphics.translate(comp.getWidth() + this.getGap(), 0);
      }
    graphics.popMatrix();
  }
}
