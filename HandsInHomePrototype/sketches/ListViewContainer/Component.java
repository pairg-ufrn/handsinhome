import processing.core.*;

public interface Component{
  public int getX();
  public int getY();
  public void setX(int x);
  public void setY(int y);
  public void setLocation(int x, int y);
  public int getWidth();
  public int getHeight();
  public void setDimension(int width, int height);
  public boolean isOver(int x, int y);
  
  public void draw(PGraphics graphics);
}
