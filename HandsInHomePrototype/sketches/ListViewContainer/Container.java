public interface Container extends Component{
  public void addComponent(Component comp);
  public void removeComponent(Component comp);
  public void clear();
  public Iterable<Component> getComponents();
  public int getComponentCount();
  public boolean contains(Component comp);
  public Component getComponentAt(int x, int y);
  public void updateLayout();
}
