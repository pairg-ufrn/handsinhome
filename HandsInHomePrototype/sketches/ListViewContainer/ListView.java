import processing.core.PGraphics;

class ListView extends BasicContainer{
  private int gap = 10;
  
  public ListView(){
    super();
  }
  public ListView(int x, int y){
    super(x,y,0,0);
  } 
  
  public void addComponentAt(int index, Component comp){
    if(index >= 0 && index < this.getComponentCount()){
      super.addComponentAt(index, comp);
      this.updateLayout();
    }
    else{
      this.addComponent(comp);
    }
  }
  
  public void updateLayout(){
    int totalWidth = 0;
    int maxHeight = 0;
    for(Component comp : this.getComponents()){
      comp.setX(totalWidth);
      totalWidth += comp.getWidth() + gap;
      
      maxHeight = (comp.getHeight() > maxHeight ? comp.getHeight() : maxHeight);
    } 
    this.setWidth(totalWidth);
    this.setHeight(maxHeight);
  }
  public void draw(PGraphics graphics){
    
    graphics.pushMatrix();
    graphics.translate(this.getX(),this.getY());
    for(Component comp : this.getComponents()){
      comp.draw(graphics);
    }
    graphics.popMatrix();
  }
}
