//package gestures;

public enum Direction {
	NORTH, SOUTH, EAST, WEST, FRONT, BACK;
	  
	public String toString(){
		String name = super.toString();
		    
		return name.substring(0, 1).concat(name.substring(1).toLowerCase());
	}
}
