import processing.opengl.*;
import SimpleOpenNI.*;

SimpleOpenNI kinect;

final PVector xAxis = new PVector(1,0,0);
final PVector yAxis = new PVector(0,1,0);
final PVector zAxis = new PVector(0,0,1);

void setup() {
  size(1028, 768, OPENGL);
  kinect = new SimpleOpenNI(this);
  kinect.enableDepth();
  kinect.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
  kinect.setMirror(true);
}

void draw() {
  kinect.update();
  background(255);
  translate(width/2, height/2, 0);
  rotateX(radians(180));
  
//  rotateX(TWO_PI*0.1);
//  rotateY(map(mouseX,0,width,0,TWO_PI));
  drawAxis();
    
  IntVector userList = new IntVector();
  kinect.getUsers(userList);
  if (userList.size() > 0) {
    int userId = userList.get(0);
    if ( kinect.isTrackingSkeleton(userId)) {
      drawSkeleton(userId);
      showRelativeAngles(userId);
    }
  }
}

void showRelativeAngles(int userId){
    PVector shoulderPos = new PVector();
    PVector elbowPos = new PVector();
    PVector handPos = new PVector();
    
    kinect.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER,  shoulderPos);
    kinect.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW,  elbowPos);
    kinect.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_HAND,  handPos);
    
    PVector dir1 = PVector.sub(elbowPos, shoulderPos);
    dir1.normalize();
    
    PMatrix3D rot = rotationFromTo(dir1,zAxis);
    PVector rotShoulder = new PVector();
    PVector rotElbow = new PVector();
    PVector rotHand = new PVector();
    
    rot.mult(shoulderPos,rotShoulder);
    rot.mult(elbowPos,rotElbow);
    rot.mult(handPos,rotHand);
    
    PVector dir2 =  PVector.sub(handPos, elbowPos);
    dir2.normalize();
    PVector rotDir2 = new PVector();
    rot.mult(dir2,rotDir2);
    showOrientation(rotDir2);
    
    PVector toOrigin = PVector.sub(new PVector(0,0,0),rotShoulder);
    pushMatrix();
    translate(toOrigin.x,toOrigin.y,toOrigin.z);
    drawLine(rotShoulder,rotElbow,color(255,0,255));
    drawLine(rotElbow,rotHand,color(255,0,255));
    popMatrix();
}

void showOrientation(PVector dir){
  float angleX = PVector.angleBetween(dir,xAxis);
  float angleZ = PVector.angleBetween(dir,zAxis);
  
  
  print("XAngle: " + degrees(angleX) + " ");
  print("ZAngle: " + degrees(angleZ) + " ");
  
  if(Math.abs(dir.z) > 0.3){
    if(dir.z > 0){
        print("Frente ");
    }
    else{
        print("Back ");
    }
  }
    
  if(Math.abs(dir.y) > 0.3){
    if(dir.y > 0){
        print("Norte ");
    }
    else{
        print("Sul ");
    }
  }
    
  if(Math.abs(dir.x) > 0.3){
    if(dir.x < 0){
        print("Leste ");
    }
    else{
        print("Oeste ");
    }
  }
    println();
}

PMatrix3D rotationFromTo(PVector dirFrom, PVector dirTo){
  //Obter angulo entre vetores
  float angle = PVector.angleBetween(dirFrom,dirTo);
  //Obter vetor normal
  PVector vecNormal = new PVector();
  PVector.cross(dirFrom,dirTo,vecNormal);
  vecNormal.normalize();
  
//  drawVector(vecNormal, 150, color(255,255,0));
  
  //Criar matriz de rotacao
  PMatrix3D mRot = new PMatrix3D();
  mRot.rotate(angle,vecNormal.x,vecNormal.y,vecNormal.z);//Rotaciona sobre eixo da normal
  
  return mRot;
}

void drawLine(PVector point0, PVector point1, color c){
    pushStyle();
    stroke(c);
    line(point0.x,point0.y,point0.z,point1.x,point1.y,point1.z);
    popStyle();
}

void drawAxis(){
  PVector xAxis = new PVector(1,0,0);
  PVector yAxis = new PVector(0,1,0);
  PVector zAxis = new PVector(0,0,1);
  drawVector(xAxis,100,color(255,0,0));
  drawVector(yAxis,100,color(0,255,0));
  drawVector(zAxis,100,color(0,0,255));
}

void drawVector(PVector vector, int length, color c){
  pushStyle();
  stroke(c);
  PVector vec = new PVector();
  vec.set(vector);
  vec.normalize();
  vec.mult(length);
  line(0,0,0,vec.x,vec.y,vec.z);
  popStyle();
}


void drawSkeleton(int userId) {
  
  drawLimb(userId, SimpleOpenNI.SKEL_HEAD, SimpleOpenNI.SKEL_NECK);
  drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
  drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER,  SimpleOpenNI.SKEL_LEFT_ELBOW);
  drawLimb(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND);
  drawLimb(userId, SimpleOpenNI.SKEL_NECK,  SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
  drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW,   SimpleOpenNI.SKEL_RIGHT_HAND);
  drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER,   SimpleOpenNI.SKEL_TORSO);
  drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER,  SimpleOpenNI.SKEL_TORSO);
  drawLimb(userId, SimpleOpenNI.SKEL_TORSO,   SimpleOpenNI.SKEL_LEFT_HIP);
  drawLimb(userId, SimpleOpenNI.SKEL_LEFT_HIP,  SimpleOpenNI.SKEL_LEFT_KNEE);
  drawLimb(userId, SimpleOpenNI.SKEL_LEFT_KNEE,   SimpleOpenNI.SKEL_LEFT_FOOT);
  drawLimb(userId, SimpleOpenNI.SKEL_TORSO,  SimpleOpenNI.SKEL_RIGHT_HIP);
  drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP,  SimpleOpenNI.SKEL_RIGHT_KNEE);
  drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_KNEE,  SimpleOpenNI.SKEL_RIGHT_FOOT);
  drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP,   SimpleOpenNI.SKEL_LEFT_HIP);
  
}
void drawLimb(int userId, int jointType1, int jointType2)
{
  PVector jointPos1 = new PVector();
  PVector jointPos2 = new PVector();
  float confidence;
  confidence = kinect.getJointPositionSkeleton(userId, jointType1,  jointPos1);
  confidence += kinect.getJointPositionSkeleton(userId, jointType2,  jointPos2);
  stroke(100);
  strokeWeight(5);
  if (confidence > 1) {
    line(jointPos1.x, jointPos1.y,  jointPos1.z, jointPos2.x, jointPos2.y, jointPos2.z);
  }
}
// user-tracking callbacks!
void onNewUser(int userId) {
  println("start pose detection");
  kinect.startPoseDetection("Psi", userId);
}
void onEndCalibration(int userId, boolean successful) {
  if (successful) {
    println(" User calibrated !!!");
    kinect.startTrackingSkeleton(userId);
  }
  else {
    println(" Failed to calibrate user !!!");
    kinect.startPoseDetection("Psi", userId);
  }
}
void onStartPose(String pose, int userId) {
  println("Started pose for user");
  kinect.stopPoseDetection(userId);
  kinect.requestCalibrationSkeleton(userId, true);
}

