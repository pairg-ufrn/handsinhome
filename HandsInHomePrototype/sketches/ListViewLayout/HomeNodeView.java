//package gui.home;

import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import processing.core.PGraphics;
import processing.core.PImage;

//import gui.components.CarouselListContainer;
//import gui.components.Component;
//import gui.components.ImageComponent;
//import gui.components.ListView;
//import gui.*;
//import home.BlockService;

public class HomeNodeView extends ImageComponent {

  ListView servicesContainer;
  
  Map<BlockService, ServiceButton> serviceButtons;
  boolean serviceIsActive;
  public PImage dialog;
    
  public HomeNodeView(int x, int y, int width, int height){
      super(x,y,width,height);
      serviceButtons = new HashMap<BlockService, ServiceButton>();
      serviceIsActive = false;
      
      dialog = null;
      
      //FIXME:!
      servicesContainer = new ListView(0, 0);
  }
    
  public void addServiceButton(BlockService service){
      System.out.println("add "+ service.getName());
      
      int diameter = calcDiameter(this);
      ServiceButton servButton = new ServiceButton(service, 0,0,diameter/2, diameter/2);
      serviceButtons.put(service, servButton);
      servicesContainer.addComponent(servButton);
      if(serviceIsActive){
        this.showServices();
      }
  }
    
  private int calcDiameter(Component comp){
    return (int)Math.sqrt(comp.getHeight()*comp.getHeight() + comp.getWidth()*comp.getWidth());
  }
    
  public void removeServiceButton(BlockService service){
      servicesContainer.removeComponent(serviceButtons.get(service));
      serviceButtons.remove(service);
      if(serviceIsActive){
        this.showServices();
      }
  }
    
  public Collection<ServiceButton> getServiceButtons(){
      return serviceButtons.values();
  }
  public void cursorMoved(int x, int y){
      if(serviceIsActive){
        
        //Distancia até a "borda externa" dos servicos
        int distanceToBorder = (int)(this.calcDiameter(this)*3.0f/2.0f);
          
        if(this.getDistance(this, x, y) > distanceToBorder){
            this.onCursorLeave();
        }
      }
      else if(super.isOver(x, y)){
        onCursorEnter();
      }
  }
    
    public void onCursorEnter(){
      this.showServices();
    }
    public void onCursorLeave(){
      this.hideServices();
    }
    
    int getDistance(Component comp, int x, int y){
      int deltaX = x - comp.getX();
      int deltaY = y - comp.getY();
      return (int)Math.sqrt(deltaX*deltaX + deltaY*deltaY);
    }
    
//    public void cursorPressed(int x, int y){
//      super.cursorPressed(x,y);
//      if(serviceIsActive){
//        for(CircularButton btn : serviceButtons.values()){
//          int newX = x - this.getBoundBox().getX();
//          int newY = y - this.getBoundBox().getY();
//          btn.cursorPressed(newX,newY);
//          
//          System.out.println(newX + ", " + newY);
//        }
//      }
//    }
    
  public void showServices(){  
    System.out.println("Show!");
      serviceIsActive = true;
  }
  protected void hideServices(){  
    System.out.println("Hide!");
      serviceIsActive = false;
  }
    
  public void draw(PGraphics graphic) {
      super.draw(graphic);
      if(serviceIsActive){
        graphic.pushMatrix();
        graphic.translate(this.getX(), this.getY() + this.getHeight());
        servicesContainer.draw(graphic);
        graphic.popMatrix();
      }
  }

    

//    @Override
//    public void mouseEntered (MouseEvent e) {
//      if(serviceIsActive){
//            
//          for(ServiceButton btn : serviceButtons.values()){
//            int newX = e.getX() - this.getX();
//            int newY = e.getY() - this.getY();
//            btn.mouseEntered(new MouseEvent((Component) e.getSource(), 
//                            e.getID(), 
//                            e.getWhen(), 
//                            e.getModifiers(), 
//                            newX, 
//                            newY, 
//                            e.getXOnScreen(), 
//                            e.getYOnScreen(), 
//                            e.getClickCount(), 
//                            false, 
//                            e.getButton()));
//          }
//          
//          //Distancia até a "borda externa" dos servicos
//          int distanceToBorder = (this.boundBox.getRadius() * 2 + this.boundBox.getRadius()/2);
//          
//          if(this.boundBox.getDistance(x, y) > distanceToBorder){
//            this.hideServices();
//          }
//        }
//    }
//    @Override
//    public void mouseExited (MouseEvent e) {
//      if(serviceIsActive){
//        MouseEvent evt = changeRelativePosition(e);
//          for(ServiceButton btn : serviceButtons.values()){
//            btn.mouseExited(evt);
//          }
//
//          this.hideServices();
//        }
//    }
//    
//    private MouseEvent changeRelativePosition(MouseEvent e){
//          int newX = e.getX() - this.getX();
//          int newY = e.getY() - this.getY();
//          return new MouseEvent((Component) e.getSource(), 
//                          e.getID(), 
//                          e.getWhen(), 
//                          e.getModifiers(), 
//                          newX, 
//                          newY, 
//                          e.getXOnScreen(), 
//                          e.getYOnScreen(), 
//                          e.getClickCount(), 
//                          false, 
//                          e.getButton());
//    }
}

