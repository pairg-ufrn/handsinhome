//package gui.components;
import processing.core.PGraphics;
import java.util.List;
import java.util.ArrayList;

public class CarouselListContainer extends AbstractContainer{
    ListView visibleComponents;
  //  protected int minGap;
    protected int currentGap;
    protected int startIndex;
    protected int maxWidth, maxHeight;
    
    public CarouselListContainer(int X,int Y, int MaxWidth, int MaxHeight){
        super(X,Y,0,0);
        this.currentGap = 10;
        this.maxWidth = MaxWidth;
        this.maxHeight = MaxHeight;
        this.visibleComponents = new ListView(X,Y);
        this.startIndex = 0;
    }
    
    public int getMaxWidth(){return this.maxWidth;}
    public int getMaxHeight(){return this.maxHeight;}
    public int getGap(){return this.currentGap;}
  //  public int getMinimumGap(){return this.minGap;}
    public ListLayout getLayout(){return visibleComponents.getLayout();}
    
    public void setMaxWidth(int Width){
        if(this.maxWidth != Width){
            this.maxWidth = Width;
            this.updateLayout();
        }
    }
    public void setMaxHeight(int Height){
        if(this.maxHeight != Height){
            this.maxHeight = Height;
            this.updateLayout();
        }
    }
    public void setGap(int GapValue){
        if(GapValue != this.currentGap){
            this.currentGap = GapValue;
            this.updateLayout();
        }
    }
    public void setLayout(ListLayout layout){
        visibleComponents.setLayout(layout);
        updateLayout();
    }
  //  public void setMinimumGap(int MinimumGapValue){
  //    if(MinimumGapValue != this.minGap){
  //      this.minGap = MinimumGapValue;
  //      this.repositionComponents();
  //    }
  //  }
  
    protected void setStartIndex(int index){
        if(index != this.startIndex && isValidIndex(index)){
            this.startIndex = index;
            this.updateLayout();
        }
    }
    public void selectNext(){
        if(this.getComponentCount() > 0){
            int next = (this.startIndex + 1) % this.getComponentCount();
            this.setStartIndex(next);
        }
    }
    public void selectPrevious(){
        if(this.getComponentCount() > 0){
            //Subtrai de 1 o indice atual e soma ao total de numeros para "dar a volta na lista" (se for necessario) em seguido retira o modulo para corrigir numero
            int previous = ((this.startIndex - 1) + this.getComponentCount()) % this.getComponentCount();            
            this.setStartIndex(previous);
        }
    }
   
   protected boolean isValidIndex(int index){
       return index >= 0 && index < this.getComponentCount();
   }
   
   //@Override
   public Component getComponentAt(int x, int y){
       return this.visibleComponents.getComponentAt(x,y);
   }
  
  //@Override
  public void updateLayout(){
    this.visibleComponents.clear();
    if(this.getComponentCount()>0){
      if(!isValidIndex(this.startIndex)){
        startIndex = 0;
      }
      
      int count = this.getComponentCount();
      int index = startIndex;
      for(int i=0; i < count && (visibleComponents.getWidth() + this.getComponent(index).getWidth() < this.getMaxWidth()); ++i){
        visibleComponents.addComponent(this.getComponent(index));
        index = (i + startIndex + 1) % count;
      }
      
      this.setWidth(visibleComponents.getWidth());
      this.setHeight(visibleComponents.getHeight());
    }
  }
  public void draw(PGraphics graphics){
      graphics.pushStyle();//FIXME: DEBUG
          graphics.noFill();
          graphics.rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
      graphics.popStyle();
      visibleComponents.setLocation(this.getX(),this.getY());
      visibleComponents.draw(graphics);
  }  
}
