//package gui.components;

import processing.core.PGraphics;
import processing.core.PImage;

public class ImageComponent extends AbstractComponent{
  private PImage img;

  public ImageComponent(){
    this(0,0,0,0);
  }
  public ImageComponent(int x,int y, int width, int height){
    this(null, x,y,width,height);
  }
  public ImageComponent(PImage compImg, int x,int y, int width, int height){
    super(x,y,width,height);
    img = compImg;
  }
  
  public PImage getImage() { return img;}
  public void setImage(PImage img) { this.img = img;}
  
  @Override
  public void draw(PGraphics graphics) {
    this.drawImage(graphics, img);
  }
  
    private void drawImage(PGraphics graphics, PImage img){
        //TODO: adicionar alinhamento às imagens (centralizado, topo, ...)
        if(img != null){
            graphics.pushMatrix();
                //img.resize(this.getWidth(), this.getHeight());
                graphics.translate(this.getX(), this.getY());
                
                
                double scale = this.getScale(img);
                
                //TODO: permitir tipos diferentes de alinhamento
                alignImg(graphics,scale,img);
                
                graphics.scale((float)scale);
            
                graphics.image(img, 0, 0);
            graphics.popMatrix();
        }
    }
    
    protected void alignImg(PGraphics graphics, double scale, PImage img){
        int realWidth  = (int)(img.width  * scale);
        int realHeight = (int)(img.height * scale);
        //Centralizar
        int deltaX = this.getWidth() - realWidth;
        int deltaY = this.getHeight() - realHeight;
        graphics.translate(deltaX/2,deltaY/2);
    }

    
    protected double getScale(PImage img){
        double xScale = this.getWidth()/((double)(img.width));
        double yScale = this.getHeight()/((double)(img.height));        
        
        return (xScale <= yScale ? xScale : yScale);
    }
//    protected double getScale(PImage img){
//        double scale;
//        if(img.width >= img.height){
//          scale = (this.getWidth()/(double)img.width);
//        }
//        else{
//            scale = (this.getHeight()/(double)img.height);
//        }
//        return scale;
//    }
    
    double calcDiagonalSize(int width, int height){
        return Math.sqrt(width*width + height*height);
    }

}

