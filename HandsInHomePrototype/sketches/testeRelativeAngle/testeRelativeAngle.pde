import processing.opengl.*;
import java.awt.event.*;

final PVector xAxis = new PVector(1,0,0);
final PVector yAxis = new PVector(0,1,0);
final PVector zAxis = new PVector(0,0,1);

float scale = 1.0f;

PVector pt0, pt1, pt2;
PVector destPt0, destPt1, destPt2;

void setup(){
  addMouseWheelListener(new MouseWheelListener() { 
    public void mouseWheelMoved(MouseWheelEvent mwe) { 
      //mouseWheel(mwe.getWheelRotation());
      scale += 0.1 * mwe.getWheelRotation();
      println(mwe.getWheelRotation());
  }}); 
  
  size(400,400,OPENGL);
  initialize();
}

void initialize(){
  //Cria 3 pontos randomicos com tamanho limite
  createPoints();
    
  //Obtem direcao da primeira "linha"
  PVector dir1 = PVector.sub(pt1,pt0);
  dir1.normalize();
  //Cria rotacao que transforme a linha 1 no eixo z
  PMatrix3D rot = rotationFromTo(dir1, zAxis);
  //Cria os pontos de destino
  destPt0 = new PVector();
  destPt1 = new PVector();
  destPt2 = new PVector();
  //Aplica rotacao nos pontos de origem e armazena nos pontos de destino
  rot.mult(pt0,destPt0);
  rot.mult(pt1,destPt1);
  rot.mult(pt2,destPt2);
  
  verifyConstraints();
}

void mouseClicked(){
    initialize();
}

void createPoints(){
  pt0 = new PVector(0,0,0);
  
  Random rand = new Random();
  //rand.nextFloat gera valores entre 0.0 e 1.0, subtrai 0.5 para permitir valores negativos
  pt1 = new PVector(rand.nextFloat() - 0.5f,rand.nextFloat() - 0.5f,rand.nextFloat() - 0.5f);
  pt1.normalize();
  pt1.mult(50);
  
  pt2 = new PVector(rand.nextFloat() - 0.5f,rand.nextFloat() - 0.5f,rand.nextFloat() - 0.5f);
  pt2.normalize();
  pt2.mult(50);
  pt2.add(pt1);
  
}

//Verifica se pontos de destino estao posicionados corretamente
void verifyConstraints(){
// 1 - Calcular se tamanho das linhas eh igual
    verifySizes();
// 2 - verificar se o angulo entre as joints eh igual
    verifyAngles();
}
void verifySizes(){
    float size1 = pt1.dist(pt0);
    float size2 = pt2.dist(pt1);
    float sizeDest1 = destPt1.dist(destPt0);
    float sizeDest2 = destPt2.dist(destPt1);
    if(sizeDest1 != size1 || sizeDest2 != size2){
        println("ERRO! tamanhos diferentes!");
        println("tamanho 1 original: " + size1 + " tamanho 1 final: " + sizeDest1);
        println("tamanho 2 original: " + size2 + " tamanho 2 final: " + sizeDest2);
    }
    else{
        println("Tamanhos OK!");
    }
}
void verifyAngles(){
    PVector dir1 = PVector.sub(pt1,pt0);
    PVector dir2 = PVector.sub(pt2,pt1);
    PVector dirDest1 = PVector.sub(destPt1,destPt0);
    PVector dirDest2 = PVector.sub(destPt2,destPt1);
    float angle = PVector.angleBetween(dir1,dir2);
    float angleDest = PVector.angleBetween(dirDest1,dirDest2);
    
    if(angle != angleDest){
        println("ERRO! ângulos diferentes!");
        println("ângulo original: " + angle + " ângulo final: " + angleDest);
    }
    else{
        println("Ângulos OK!");
    }
}

void draw(){
  background(255);
  translate(height*0.5,width*0.5);
  rotateX(TWO_PI*0.6);
//  rotateX(TWO_PI*0.1);
  rotateY(map(mouseX,0,width,0,TWO_PI));
  scale(scale);
  
  drawAxis();  
  
  //Pontos de origem
  drawLine(pt0,pt1,color(0));
  drawLine(pt1,pt2,color(0));
  //Pontos de destino
  drawLine(destPt0,destPt1, color(255,255,0));
  drawLine(destPt1,destPt2, color(255,255,0));
}

PMatrix3D rotationFromTo(PVector dirFrom, PVector dirTo){
  //Obter angulo entre vetores
  float angle = PVector.angleBetween(dirFrom,dirTo);
  //Obter vetor normal
  PVector vecNormal = new PVector();
  PVector.cross(dirFrom,dirTo,vecNormal);
  vecNormal.normalize();
  
  drawVector(vecNormal, 150, color(255,255,0));
  
  //Criar matriz de rotacao
  PMatrix3D mRot = new PMatrix3D();
  mRot.rotate(angle,vecNormal.x,vecNormal.y,vecNormal.z);//Rotaciona sobre eixo da normal
  
  return mRot;
}

void drawLine(PVector point0, PVector point1, color c){
    pushStyle();
    stroke(c);
    line(point0.x,point0.y,point0.z,point1.x,point1.y,point1.z);
    popStyle();
}

void drawAxis(){
  PVector xAxis = new PVector(1,0,0);
  PVector yAxis = new PVector(0,1,0);
  PVector zAxis = new PVector(0,0,1);
  drawVector(xAxis,100,color(255,0,0));
  drawVector(yAxis,100,color(0,255,0));
  drawVector(zAxis,100,color(0,0,255));
}

void drawVector(PVector vector, int length, color c){
  pushStyle();
  stroke(c);
  PVector vec = new PVector();
  vec.set(vector);
  vec.normalize();
  vec.mult(length);
  line(0,0,0,vec.x,vec.y,vec.z);
  popStyle();
}
