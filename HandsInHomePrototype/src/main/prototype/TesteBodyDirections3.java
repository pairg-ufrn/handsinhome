package main.prototype;
import gestures.Direction;
import gestures.DirectionGesture;
import gestures.DirectionRecognizer;
import gestures.Gesture;
import gestures.Gesture.RecognitionStatus;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;

import java.util.Collection;

import main.utils.AppDrawSkeleton3DTemplate;
import processing.core.PVector;

//TODO: (?) substituir isso por testes
@SuppressWarnings("serial")
public class TesteBodyDirections3 extends AppDrawSkeleton3DTemplate{
	Skeleton userSkeleton;
    Gesture rightGesture;
    
	@Override
	public void setup(){
		super.setup();

		updater = new SkeletonUpdater(context);
		userSkeleton = new Skeleton();
		rightGesture = new DirectionGesture(new Direction[] { Direction.UP, Direction.RIGHT }, 
				SkeletonPart.RightForearm,
				SkeletonPart.RightArm);
	}
	
	@Override
	protected void drawUser(int userId){
		super.drawUser(userId);

		if (rightGesture.update(userSkeleton) == RecognitionStatus.recognized) {
			stroke(0, 255, 0);
		} else {
			stroke(255, 0, 0);
		}
		
		drawRelativeAxisToBody(userSkeleton, SkeletonPart.RightForearm, SkeletonPart.RightArm);
		drawRelativeAxisToBody(userSkeleton, SkeletonPart.LeftForearm, SkeletonPart.LeftArm);

		if(super.frameCount % 10 == 0){
			showDirectionsPart(userSkeleton, SkeletonPart.RightForearm, SkeletonPart.RightArm); 
			showDirectionsPart(userSkeleton, SkeletonPart.LeftForearm, SkeletonPart.LeftArm); 
		}
		  
		drawBodyDirections(userSkeleton);
	}

	private void drawBodyDirections(Skeleton skeleton) {
		PVector bodyPos = skeleton.getPosition();
		
		PVector[] bodyAxis = new PVector[3];
		bodyAxis[0] = skeleton.getRightDirection();
		bodyAxis[1] = skeleton.getUpDirection();
		bodyAxis[2] = skeleton.getFrontDirection();
		
		pushMatrix();
		pushStyle();
			strokeWeight(5);
			drawAxis(bodyPos, bodyAxis);
		popStyle();
		popMatrix();
	}
	void drawVector(PVector vector, int length, int c) {
		pushStyle();
		stroke(c);
		PVector vec = new PVector();
		vec.set(vector);
		vec.normalize();
		vec.mult(length);
		line(0, 0, 0, vec.x, vec.y, vec.z);
		popStyle();
	}
	
	private void drawRelativeAxisToBody(Skeleton skeleton, SkeletonPart skelPart, SkeletonPart skelPartBase){
		PVector[] baseAxis = DirectionRecognizer.getInstance().getRelativeAxisToBody(skeleton, skelPartBase);

			PVector direction = skeleton.getPart(skelPart).getDirection();
			
			double projX = PVector.dot(direction, baseAxis[0]);
			double projY = PVector.dot(direction, baseAxis[1]);
			double projZ = PVector.dot(direction, baseAxis[2]);

		if(super.frameCount % 10 == 0){
			System.out.println(skelPart + " proj: " + " [" + (int)(projX*100) +", "+ (int)(projY*100) +", "+ (int)(projZ*100)  +"] ");
		}
		
		drawAxis(skeleton.getPart(skelPart).getStartPosition(), baseAxis, color(255,0,0), color(0,255,0), color(0,0,255));
	}

	

	private void showDirectionsPart(Skeleton skeleton, SkeletonPart skelPart,SkeletonPart skelPartBase) {
		Collection<Direction> relativeDirections 
			= DirectionRecognizer.getInstance().getPartDirections(skeleton, skelPart, skelPartBase);

	
		System.out.print("dir["+skelPart+"]: ");
		for(Direction dir: relativeDirections){
			System.out.print(" " + dir);
		}
		System.out.println();
	}

	void drawAxis(PVector loc, PVector[] axis){
		this.drawAxis(loc, axis, this.color(255,0,0), this.color(0,255,0), this.color(0,0,255));
	}
	void drawAxis(PVector loc, PVector[] axis, int colorX, int colorY, int colorZ){
		this.pushMatrix();
			this.translate(loc.x,  loc.y, loc.z);

			drawVector(axis[0], 200, colorX);
			drawVector(axis[1], 200, colorY);
			drawVector(axis[2], 200, colorZ);
		this.popMatrix();
	}
}
