package main.prototype;

import gui.components.ImageManager;
import gui.screens.HomeObjectScreen;
import gui.screens.HomeScreen;
import gui.screens.Screen;
import gui.screens.ScreenManager;

import java.util.ArrayList;
import java.util.List;

import processing.core.PApplet;

public class PrototypeScreenManager extends PApplet {
	private static final long serialVersionUID = 1L;
	
	ScreenManager screenManager;

	static public void main(String args[]) {
		PApplet.main(new String[] { "--bgcolor=#ECE9D8",
				PrototypeScreenManager.class.getName() });
	}

	public PrototypeScreenManager() {

	}

	public void setup() {
		this.size(1100, 600);
		ImageManager.getInstance().setPApplet(this);
		
		this.screenManager = new ScreenManager();
		
		Screen favorite = new HomeObjectScreen("objetos", "objetos.png");
		favorite.setSize(this.width, this.height);
		this.screenManager.addScreen(favorite);
		this.screenManager.addScreen(((Screen) ((HomeObjectScreen) favorite).popUp(favorite.getTitle() + "popUp")));

		
		List<Screen> visibleScreens = new ArrayList<Screen>();
		visibleScreens.add(favorite);
		
		Screen home = new HomeScreen(visibleScreens, "<Nome da Aplicação>", "home.png");
		home.setSize(this.width, this.height);
		this.screenManager.addScreen(home);
		this.screenManager.addScreen(((Screen) ((HomeScreen) home).popUp(home.getTitle() + "popUp")));
		
		this.screenManager.setCurrentScreen(home.getTitle());
	}

	public void draw() {
		this.screenManager.draw(this.g);
	}

	public void mouseMoved() {
		this.screenManager.mouseMoved(mouseX, mouseY, pmouseX, pmouseY);
	}

	public void keyPressed() {
		this.screenManager.keyPressed(key, keyCode);
	}

}
