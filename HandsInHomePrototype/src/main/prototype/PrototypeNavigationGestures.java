package main.prototype;

import gestures.Gesture;
import gestures.Gesture.RecognitionStatus;
import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;
import gui.components.ImageManager;
import gui.screens.HomeObjectScreen;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import main.utils.GestureFactory;
import processing.core.PApplet;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

@SuppressWarnings("serial")
public class PrototypeNavigationGestures extends PApplet{

    public static void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", PrototypeNavigationGestures.class.getName() });
    }

	private HomeObjectScreen favoritesScreen;

	SimpleOpenNI  context;
	final boolean autoCalib=false;
	
	Skeleton userSkeleton;
	SkeletonUpdater updater;
	List<Skeleton.SkeletonPart> visibleParts;

	Map<String, Gesture> gestures;
	String lastGesture;
	
	public PrototypeNavigationGestures(){
		ImageManager.getInstance().setPApplet(this);
		
		favoritesScreen = new HomeObjectScreen();
		gestures = new HashMap<String,Gesture>();
		lastGesture = "";
	}
	
	public void setup() {
		// size(640,480);
		size(1100, 600); //800x2,450
		this.frameRate(15);
		context = new SimpleOpenNI(this);
		context.enableDepth();

		// enable skeleton generation for all joints
		context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
		// context.enableUser(SimpleOpenNI.SKEL_PROFILE_UPPER);
		context.setMirror(true);

		background(220);

		stroke(0, 0, 255);
		strokeWeight(3);
		smooth();

		userSkeleton = new Skeleton();
		updater = new SkeletonUpdater(context);

		addGestures();

		visibleParts = new LinkedList<Skeleton.SkeletonPart>();
		addVisiblePart(visibleParts);

		favoritesScreen.setSize(this.width, this.height);
		//FIXME: permitir definir qualquer tamanho para tela e manter proporções (?)
//		favoritesScreen.setSize(1100, 600);//Tamanho "ótimo"
		favoritesScreen.setup();
	}
	
	private void addGestures() {
		gestures.put("PassRight", GestureFactory.getInstance().createPassRightLHand());
		gestures.put("PassLeft", GestureFactory.getInstance().createPassLeftRHand());
		gestures.put("PassRightRHand", GestureFactory.getInstance().createPassRightRHand());
		gestures.put("PassUp", GestureFactory.getInstance().createPassUpRHand());
//		gestures.put("PassDown", GestureFactory.getInstance().createPassDownLHand());
		gestures.put("PassDownRight", GestureFactory.getInstance().createPassDownRHand());
		gestures.put("Click", GestureFactory.getInstance().createClick());
		gestures.put("GoBack", GestureFactory.getInstance().createGoBack());
		gestures.put("Over", GestureFactory.getInstance().createOver());
//		gestures.put("InitGoBack", GestureFactory.getInstance().createGoBackInitialPose());
//		gestures.put("MoveToBack", new LineMovementGesture(new Direction[]{Direction.BACK}, SkeletonPart.RightHand));
//		gestures.put("DistanceGesture", new DistanceGesture(new SkeletonPoint(SkeletonPart.RightHand)
//										  , new SkeletonPoint(SkeletonPart.RightShoulder,PartPoint.End)
//										  , Direction.RIGHT, 250.0,null));
//		gestures.put("HandLeft", new PositionGesture(new Position[]{Position.Left}
//													, SkeletonPart.RightHand
//													, SkeletonPart.Chest));
	}

	private void addVisiblePart(List<Skeleton.SkeletonPart> parts){
		parts.add(SkeletonPart.RightArm);
		parts.add(SkeletonPart.RightForearm);
		parts.add(SkeletonPart.LeftArm);
		parts.add(SkeletonPart.LeftForearm);
		parts.add(SkeletonPart.Head);
		parts.add(SkeletonPart.Abdomen);
		

		parts.add(SkeletonPart.RightThigh);
		parts.add(SkeletonPart.RightHip);
		parts.add(SkeletonPart.RightLeg);
		parts.add(SkeletonPart.LeftThigh);
		parts.add(SkeletonPart.LeftHip);
		parts.add(SkeletonPart.LeftLeg);
		parts.add(SkeletonPart.RightArm);
	}

	//*********************************************************************************************
	
	public void draw() {
		// Desenha tela
		favoritesScreen.draw(this.g);
		// Desenha todo o resto
//		translate(favoritesScreen.getWidth(), 0);
		
		// update the cam
		context.update();

		int newHeight = this.height/3;
		translate(0,this.height - newHeight);
		scale(1f/3f);
		
		// draw depthImageMap
		image(context.depthImage(), 0, 0);

		pushStyle();

		textSize(18);
		// draw the skeleton if it's available
		int[] userList = context.getUsers();
		for (int i = 0; i < userList.length; i++) {
			if (context.isTrackingSkeleton(userList[i])) {

				updater.updateSkeleton(userList[0], userSkeleton);

				if (lastGesture != null && lastGesture != "") {
					text(lastGesture, 50, 50);
				}

				checkGestures(userSkeleton);
//				showDirection(SkeletonPart.RightArm, SkeletonPart.Body);
				for(SkeletonPart part: SkeletonPart.values()){
					showPart(userSkeleton, part);
				}
			}
		}  
		
		popStyle();

	}
	



	private void showPart(Skeleton skeleton, SkeletonPart part) {
		String partName = part.toString();
		
		drawBodyPart(partName, skeleton.getPart(part));
	}

	private void checkGestures(Skeleton skeleton) {
		
		for(String str: this.gestures.keySet()){
			if(this.gestures.get(str).getCurrentStatus() != RecognitionStatus.recognized
					&& this.gestures.get(str).update(skeleton) == RecognitionStatus.recognized){
				this.lastGesture = str + System.currentTimeMillis()/1000;
				this.fireGesture(str);
				
//				this.gestures.get(str).clearStatus();

				/*TODO: filtrar gestos a serem reiniciados. 
				 * Pois desta forma não é possível haver gestos concorrentes
				 * Talvez considerar apenas gestos realizados com as mesmas partes*/
//				clearOthers(this.gestures.get(str));
				//FIXME: problema com gestos estaticos -> o gesto não para de ser reconhecido
				clearAll();
				break;
			}
		}
	}

	private void clearAll() {
		for(Gesture gesture: this.gestures.values()){
			gesture.clearStatus();
		}
	}
//	private void clearOthers(Gesture gest) {
//		for(Gesture gesture: this.gestures.values()){
//			if(gesture != gest){
//				gesture.clearStatus();
//			}
//		}
//	}

	private void fireGesture(String gestureName) {
		System.out.println(gestureName);
		if(gestureName == "PassRight" || gestureName == "PassRightRHand"){
			favoritesScreen.keyPressed((char)CODED, LEFT);
		}
		else if(gestureName == "PassLeft"){
			favoritesScreen.keyPressed((char)CODED, RIGHT);
			
		}
		else if(gestureName == "PassUp" ){
			favoritesScreen.keyPressed((char)CODED, UP);
			System.out.println("Up");
		}
		else if(gestureName == "PassDown" || gestureName == "PassDownRight"){
			favoritesScreen.keyPressed((char)CODED, DOWN);
			System.out.println("Down");
			
		}
		else if(gestureName == "Click"){
			favoritesScreen.mouseClicked();
			favoritesScreen.keyPressed('c',0);
			System.out.println("Click!");
		}
		else if(gestureName == "GoBack"){
			favoritesScreen.keyPressed('b',0);
		}
		else if(gestureName == "Over"){
			
		}
	}

	public void mouseMoved() {
		favoritesScreen.mouseMoved(mouseX, mouseY, pmouseX, pmouseY);
	}

	public void keyPressed() {
		favoritesScreen.keyPressed(key, keyCode);
	}
	

	public void drawBodyPart(String partName, BodyPart part){
		this.pushStyle();
			PVector center = new PVector();
			part.getCentralPosition(center);
			context.convertRealWorldToProjective(center, center);
			
			drawLimb(part);
			ellipse(center.x,center.y,25,25);
			
			fill(255,0,0);
			text(partName, center.x,center.y);
		this.popStyle();
	}

	private void drawLimb(BodyPart part) {
		PVector start = new PVector();
		PVector end = new PVector();
		context.convertRealWorldToProjective(part.getStartPosition(), start);
		context.convertRealWorldToProjective(part.getEndPosition(), end);
		
		line(start.x,start.y,end.x,end.y);
	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId)
	{
	  println("onNewUser - userId: " + userId);
	  println("  start pose detection");
	  
	  if(autoCalib)
	    context.requestCalibrationSkeleton(userId,true);
	  else    
	    context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
	  println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
	  println("onExitUser - userId: " + userId);
	}

	public void onReEnterUser(int userId)
	{
	  println("onReEnterUser - userId: " + userId);
	}

	public void onStartCalibration(int userId)
	{
	  println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
	  println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);
	  
	  if (successfull) 
	  { 
	    println("  User calibrated !!!");
	    context.startTrackingSkeleton(userId); 
	  } 
	  else 
	  { 
	    println("  Failed to calibrate user !!!");
	    println("  Start pose detection");
	    context.startPoseDetection("Psi",userId);
	  }
	}

	public void onStartPose(String pose,int userId)
	{
	  println("onStartPose - userId: " + userId + ", pose: " + pose);
	  println(" stop pose detection");
	  
	  context.stopPoseDetection(userId); 
	  context.requestCalibrationSkeleton(userId, true);
	 
	}

	public void onEndPose(String pose,int userId)
	{
	  println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

}
