package main.prototype;

import gui.components.list.CarouselListContainer;
import processing.core.PApplet;
import processing.core.PImage;

@SuppressWarnings("serial")
public class PrototipoBackgroundColor extends PApplet{

    static public void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", PrototipoBackgroundColor.class.getName() });
    }

    PImage backgroundImg;
    int backgroundColor;
    
	CarouselListContainer container;
	
	public PrototipoBackgroundColor(){
		backgroundColor = this.color(255,255,255);
	}
	
	public void setup()
	{	  
		size(800, 600);
		this.frameRate(15);

		background(220);

		stroke(0, 0, 255);
		strokeWeight(3);
		smooth();
		textSize(18);
		
		loadResources();
	}
	
	public void loadResources(){
		backgroundImg = this.loadImage("images/background.png");
		backgroundImg.resize(this.width, this.height);
	}

	public void draw()
	{
		if(backgroundImg != null){
			this.tint(backgroundColor);
			this.image(backgroundImg, 0, 0);
		}
	}
	


	public void mouseMoved() {
	}
	
	public void mouseClicked(){
		if(mouseButton == LEFT){
			int pixelNumber = this.mouseX + this.mouseY*this.width;
			this.backgroundColor = (int) PApplet.map(pixelNumber, 0, this.width*this.height, 0, this.color(0,0,0));
			
			System.out.println("Cor: #" + backgroundColor + " [" + this.red(backgroundColor) + ", " + this.green(backgroundColor) + ", " + this.blue(backgroundColor) + "] " );
		}
		else if(mouseButton == RIGHT){
			this.backgroundColor = this.color(255,255,255);
		}
	}

	public void keyPressed() {
	}


}
