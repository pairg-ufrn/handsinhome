package main.prototype;

import gestures.Direction;
import gestures.DirectionGesture;
import gestures.DirectionRecognizer;
import gestures.Gesture;
import gestures.Gesture.RecognitionStatus;
import gestures.SequenceGesture;
import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;
import gui.components.list.CarouselListContainer;
import gui.screens.HomeObjectScreen;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import processing.core.PApplet;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

//TODO: (?) criar teste para checar SequenceGesture
@SuppressWarnings("serial")
public class TesteSequenceGesture extends PApplet{

    static public void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", TesteSequenceGesture.class.getName() });
    }

	CarouselListContainer container;
	private HomeObjectScreen favoritesScreen;

	SimpleOpenNI  context;
	final boolean autoCalib=false;
	
	Skeleton userSkeleton;
	SkeletonUpdater updater;
	List<Skeleton.SkeletonPart> visibleParts;

	Gesture passRight;
	Gesture passLeft;
	
	static final int GEST_NONE = 0;
	static final int GEST_RIGHT = 1;
	static final int GEST_LEFT = 1;
	int lastGesture = GEST_NONE; //GAMBIARRA!!
	
	public TesteSequenceGesture(){
		favoritesScreen = new HomeObjectScreen("objetos", "objetos.png");
	}
	
	public void setup()
	{	  
//	  size(640,480);
	  size(1280,480); 
	  this.frameRate(15);
	  context = new SimpleOpenNI(this);
	  context.enableDepth();
	  
	  // enable skeleton generation for all joints
	  context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
//	  context.enableUser(SimpleOpenNI.SKEL_PROFILE_UPPER);
	  context.setMirror(true);
	 
	  background(220);

	  stroke(0,0,255);
	  strokeWeight(3);
	  smooth();
	  textSize(18);
	  
	  userSkeleton = new Skeleton();
	  updater = new SkeletonUpdater(context);
	  
	  passRight = this.createSwipeRight();
	  passLeft = this.createSwipeLeft();

	  visibleParts = new LinkedList<Skeleton.SkeletonPart>();
	  addVisiblePart(visibleParts);
	  

//		favoritesScreen.setSize(this.width, this.height);
		favoritesScreen.setSize(640, this.height);
		favoritesScreen.setup();
	}
	
	private void addVisiblePart(List<Skeleton.SkeletonPart> parts){
		parts.add(SkeletonPart.RightArm);
		parts.add(SkeletonPart.RightForearm);
		parts.add(SkeletonPart.LeftArm);
		parts.add(SkeletonPart.LeftForearm);
		parts.add(SkeletonPart.Head);
		parts.add(SkeletonPart.RightArm);
	}

	private Gesture createSwipeRight() {
		SequenceGesture gesture = new SequenceGesture();
		
		gesture.addGesture(new DirectionGesture(new Direction[]{Direction.UP, Direction.RIGHT}
		, SkeletonPart.RightForearm, SkeletonPart.RightArm));
		gesture.addGesture(new DirectionGesture(new Direction[]{Direction.UP}
		, SkeletonPart.RightForearm, SkeletonPart.RightArm));
		gesture.addGesture(new DirectionGesture(new Direction[]{Direction.UP, Direction.LEFT}
		, SkeletonPart.RightForearm, SkeletonPart.RightArm));
		
		return gesture;
	}
	private Gesture createSwipeLeft() {
		SequenceGesture gesture = new SequenceGesture();
		
		gesture.addGesture(new DirectionGesture(new Direction[]{Direction.UP, Direction.LEFT}
		, SkeletonPart.LeftForearm, SkeletonPart.LeftArm));
		gesture.addGesture(new DirectionGesture(new Direction[]{Direction.UP}
		, SkeletonPart.LeftForearm, SkeletonPart.LeftArm));
		gesture.addGesture(new DirectionGesture(new Direction[]{Direction.UP, Direction.RIGHT}
		, SkeletonPart.LeftForearm, SkeletonPart.LeftArm));
		
		return gesture;
	}

	public void draw()
	{
		//Desenha tela
		favoritesScreen.draw(this.g);
		//Desenha todo o resto
		translate(640,0);
		  // update the cam
		  context.update();
		  
		  // draw depthImageMap
		  image(context.depthImage(),0,0);
		  
		  // draw the skeleton if it's available
		  int[] userList = context.getUsers();
		  for(int i=0;i<userList.length;i++)
		  {
		    if(context.isTrackingSkeleton(userList[i])){
		//	      drawSkeleton(userList[i]);
		//	      showPartsDirection(userList[i]);
		
		  updater.updateSkeleton(userList[0], userSkeleton);
		
		  if(passRight.update(userSkeleton) == RecognitionStatus.recognized){
			  stroke(0,255,0);
			  if(lastGesture != GEST_RIGHT){
				  text("Direita",0,0);
				  favoritesScreen.keyPressed((char)CODED, RIGHT);
				  lastGesture = GEST_RIGHT;
			  }
		  }
		  else if(passLeft.update(userSkeleton) == RecognitionStatus.recognized){
			  stroke(0,0,255);
			  if(lastGesture != GEST_LEFT){
				  text("Esquerda",0,0);
				  favoritesScreen.keyPressed((char)CODED, LEFT);
				  lastGesture = GEST_LEFT;
			  }
		  }
		  else{
			  stroke(255,255,255);
			  lastGesture = GEST_NONE;
		  }
		  
//		  for(SkeletonPart part : visibleParts){
//			  showDirection(part);
//		  }
		  showDirection(SkeletonPart.LeftForearm, SkeletonPart.LeftArm);
		  showDirection(SkeletonPart.RightForearm, SkeletonPart.RightArm);
		}
	  }    
	  

	}
	


	public void mouseMoved() {
		favoritesScreen.mouseMoved(mouseX, mouseY, pmouseX, pmouseY);
	}

	public void keyPressed() {
		favoritesScreen.keyPressed(key, keyCode);
	}

	private void showDirection(Skeleton.SkeletonPart part, Skeleton.SkeletonPart toPart) {
		Collection<Direction> directions = DirectionRecognizer.getInstance().getPartDirections(userSkeleton, part,toPart);
		String partName = part.toString();
		partName += "[";
		for(Direction dir : directions){
			partName += dir + " ";
		}
		partName += "]";
		
		drawBodyPart(partName, userSkeleton.getPart(part));
	}
	

	public void drawBodyPart(String partName, BodyPart part){
		this.pushStyle();
			PVector center = new PVector();
			part.getCentralPosition(center);
			context.convertRealWorldToProjective(center, center);
			
			drawLimb(part);
			ellipse(center.x,center.y,25,25);
			
			fill(255,0,0);
			text(partName, center.x,center.y);
		this.popStyle();
	}

	private void drawLimb(BodyPart part) {
		PVector start = new PVector();
		PVector end = new PVector();
		context.convertRealWorldToProjective(part.getStartPosition(), start);
		context.convertRealWorldToProjective(part.getEndPosition(), end);
		
		line(start.x,start.y,end.x,end.y);
	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId)
	{
	  println("onNewUser - userId: " + userId);
	  println("  start pose detection");
	  
	  if(autoCalib)
	    context.requestCalibrationSkeleton(userId,true);
	  else    
	    context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
	  println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
	  println("onExitUser - userId: " + userId);
	}

	public void onReEnterUser(int userId)
	{
	  println("onReEnterUser - userId: " + userId);
	}

	public void onStartCalibration(int userId)
	{
	  println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
	  println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);
	  
	  if (successfull) 
	  { 
	    println("  User calibrated !!!");
	    context.startTrackingSkeleton(userId); 
	  } 
	  else 
	  { 
	    println("  Failed to calibrate user !!!");
	    println("  Start pose detection");
	    context.startPoseDetection("Psi",userId);
	  }
	}

	public void onStartPose(String pose,int userId)
	{
	  println("onStartPose - userId: " + userId + ", pose: " + pose);
	  println(" stop pose detection");
	  
	  context.stopPoseDetection(userId); 
	  context.requestCalibrationSkeleton(userId, true);
	 
	}

	public void onEndPose(String pose,int userId)
	{
	  println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

}
