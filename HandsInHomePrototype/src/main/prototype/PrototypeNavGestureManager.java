package main.prototype;

import gestures.CompositeGesture;
import gestures.Direction;
import gestures.DirectionGesture;
import gestures.DirectionRecognizer;
import gestures.Gesture;
import gestures.Gesture.RecognitionStatus;
import gestures.GestureListener;
import gestures.GestureManager;
import gestures.HoldPositionGesture;
import gestures.LineMovementGesture;
import gestures.Position;
import gestures.PositionGesture;
import gestures.SequenceGesture;
import gestures.TrashableGesture;
import gestures.WaitTime;
import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;
import gui.screens.HomeObjectScreen;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import processing.core.PApplet;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

@SuppressWarnings("serial")
public class PrototypeNavGestureManager extends PApplet implements GestureListener{

    static public void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", PrototypeNavGestureManager.class.getName() });
    }

	private HomeObjectScreen favoritesScreen;

	SimpleOpenNI  context;
	final boolean autoCalib=false;
	
	Skeleton userSkeleton;
	SkeletonUpdater updater;
	List<Skeleton.SkeletonPart> visibleParts;

	Map<String, Gesture> gestures;
	String lastGesture;
	
	GestureManager gestureManager;
	
	public PrototypeNavGestureManager(){
		favoritesScreen = new HomeObjectScreen("objetos", "objetos.png");
		gestures = new HashMap<String,Gesture>();
		lastGesture = "";
		gestureManager = new GestureManager();
	}
	
	public void setup() {
		// size(640,480);
		size(1280, 480);
		this.frameRate(15);
		context = new SimpleOpenNI(this);
		context.enableDepth();

		// enable skeleton generation for all joints
		context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
		// context.enableUser(SimpleOpenNI.SKEL_PROFILE_UPPER);
		context.setMirror(true);

		background(220);

		stroke(0, 0, 255);
		strokeWeight(3);
		smooth();
		textSize(18);

		userSkeleton = new Skeleton();
		updater = new SkeletonUpdater(context);

		addGestures();
		gestureManager.addGestureListener(this);

		visibleParts = new LinkedList<Skeleton.SkeletonPart>();
		addVisiblePart(visibleParts);

		// favoritesScreen.setSize(this.width, this.height);
		favoritesScreen.setSize(this.width/2, this.height);
		favoritesScreen.setup();
	}
	
	private void addGestures() {
		gestureManager.addGesture("PassRight", this.createPassRight());
		gestureManager.addGesture("PassLeft", this.createPassLeftRHand());
		gestureManager.addGesture("PassRightRHand", this.createPassRightRHand());
		gestureManager.addGesture("PassUp", this.createPassUpRHand());
		gestureManager.addGesture("PassDown", this.createPassDown());
		gestureManager.addGesture("PassDownRight", this.createPassDownRHand());
		gestureManager.addGesture("Click", this.createClick());
		gestureManager.addGesture("GoBack", this.createGoBack());
		gestureManager.addGesture("Over", this.createOver());
	}

	private void addVisiblePart(List<Skeleton.SkeletonPart> parts){
		parts.add(SkeletonPart.RightArm);
		parts.add(SkeletonPart.RightForearm);
		parts.add(SkeletonPart.LeftArm);
		parts.add(SkeletonPart.LeftForearm);
		parts.add(SkeletonPart.Head);
		parts.add(SkeletonPart.RightArm);
	}

	//******************************CRIAR GESTOS *********************************************************
	private Gesture createPassLeftRHand() {
		LineMovementGesture moveLeft = new LineMovementGesture(
				new Direction[]{Direction.LEFT}, SkeletonPart.RightHand,80);
		LineMovementGesture moveRightTrash = new LineMovementGesture(
				new Direction[]{Direction.RIGHT}, SkeletonPart.RightHand,80);
		TrashableGesture gesture = new TrashableGesture(moveLeft,moveRightTrash);
		
		return gesture;
	}
	private Gesture createPassRightRHand() {
		LineMovementGesture moveRight = new LineMovementGesture(
				new Direction[]{Direction.RIGHT}, SkeletonPart.RightHand,80);
		LineMovementGesture moveLeftTrash = new LineMovementGesture(
				new Direction[]{Direction.LEFT}, SkeletonPart.RightHand,80);
		TrashableGesture gesture = new TrashableGesture(moveRight, moveLeftTrash);
		
		return gesture;
	}
	private Gesture createPassRight() {
		LineMovementGesture gesture = new LineMovementGesture(
				new Direction[]{Direction.RIGHT}, SkeletonPart.LeftHand,80);
		
		return gesture;
	}
	
	private Gesture createPassUpRHand() {
		PositionGesture handBelowChest = new PositionGesture(new Position[]{Position.Below}, SkeletonPart.RightHand,SkeletonPart.Chest);
		LineMovementGesture handMoveUp = new LineMovementGesture(
				new Direction[]{Direction.UP}, SkeletonPart.RightHand);

		SequenceGesture sequenceGesture = new SequenceGesture(handBelowChest, handMoveUp);
		LineMovementGesture handMoveDownTrash = new LineMovementGesture(
				new Direction[]{Direction.DOWN}, SkeletonPart.RightHand);

		TrashableGesture gesture = new TrashableGesture(sequenceGesture, handMoveDownTrash);
		
		return gesture;
	}
	private Gesture createPassDownRHand() {
		PositionGesture handAboveChest = new PositionGesture(new Position[]{Position.Above}, SkeletonPart.RightHand,SkeletonPart.Chest);
		LineMovementGesture handMoveDown = new LineMovementGesture(
				new Direction[]{Direction.DOWN}, SkeletonPart.RightHand);

		SequenceGesture sequenceGesture = new SequenceGesture(handAboveChest, handMoveDown);
		LineMovementGesture handMoveUpTrash = new LineMovementGesture(
				new Direction[]{Direction.UP}, SkeletonPart.RightHand);

		TrashableGesture gesture = new TrashableGesture(sequenceGesture, handMoveUpTrash);
		
		return gesture;
	}
//	private Gesture createPassDownRHand() {
//		PositionGesture handUp = new PositionGesture(new Position[]{Position.Up}, SkeletonPart.RightHand,SkeletonPart.Chest);
//		HoldPositionGesture waitALittle = new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.SmallTime.getTime()/2);
//		
//		CompositeGesture waitHandUp = new CompositeGesture(handUp, waitALittle);
//		
//		LineMovementGesture moveHandDown = new LineMovementGesture(
//				new Direction[]{Direction.DOWN}, SkeletonPart.RightHand);
//		
//		SequenceGesture gesture = new SequenceGesture(waitHandUp,moveHandDown);
//		
//		return gesture;
//	}
	private Gesture createPassDown() {
		LineMovementGesture gesture = new LineMovementGesture(
				new Direction[]{Direction.DOWN}, SkeletonPart.LeftHand);
		
		return gesture;
	}

	private Gesture createClick() {
		SequenceGesture gesture = new SequenceGesture();
		LineMovementGesture firstMove = new LineMovementGesture(
				new Direction[]{Direction.FRONT}, SkeletonPart.LeftHand);
		LineMovementGesture secondMove = new LineMovementGesture(
				new Direction[]{Direction.BACK}, SkeletonPart.LeftHand);

		gesture.addGesture(firstMove);
		gesture.addGesture(secondMove);
		
		return gesture;
	}

	private Gesture createGoBack() {
		DirectionGesture dirForearm 
							= new DirectionGesture(new Direction[]{Direction.UP}, SkeletonPart.RightForearm, SkeletonPart.RightArm);
		LineMovementGesture forearmMove 
							= new LineMovementGesture(new Direction[]{Direction.BACK}, SkeletonPart.RightHand);
		
		SequenceGesture gesture = new SequenceGesture();
		gesture.addGesture(dirForearm);
		gesture.addGesture(forearmMove);
		
		return gesture;
	}

	private Gesture createOver() {
		PositionGesture handFrontBody = new PositionGesture(new Position[]{Position.Front}, SkeletonPart.RightHand, SkeletonPart.Body);
		PositionGesture handUpAbdomen = new PositionGesture(new Position[]{Position.Above}, SkeletonPart.RightHand, SkeletonPart.Abdomen);
		PositionGesture handDownHead  = new PositionGesture(new Position[]{Position.Below}, SkeletonPart.RightHand, SkeletonPart.Head);
		HoldPositionGesture handStopped   = new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.MediumTime);
		
		CompositeGesture gesture = new CompositeGesture();
		gesture.addGesture(handFrontBody);
		gesture.addGesture(handUpAbdomen);
		gesture.addGesture(handDownHead);
		gesture.addGesture(handStopped);
		
		return gesture;
	}

	//*********************************************************************************************
	
	public void draw() {
		// Desenha tela
		favoritesScreen.draw(this.g);
		// Desenha todo o resto
		translate(640, 0);
		// update the cam
		context.update();

		// draw depthImageMap
		image(context.depthImage(), 0, 0);

		// draw the skeleton if it's available
		int[] userList = context.getUsers();
		for (int i = 0; i < userList.length; i++) {
			if (context.isTrackingSkeleton(userList[i])) {

				updater.updateSkeleton(userList[0], userSkeleton);

				if (lastGesture != null && lastGesture != "") {
					text(lastGesture, 50, 50);
				}

				checkGestures(userSkeleton);
				showDirection(SkeletonPart.RightArm, SkeletonPart.Body);
			}
		}  
	  

	}
	


	private void checkGestures(Skeleton skeleton) {
		
		for(String str: this.gestures.keySet()){
			if(this.gestures.get(str).getCurrentStatus() != RecognitionStatus.recognized
					&& this.gestures.get(str).update(skeleton) == RecognitionStatus.recognized){
				this.lastGesture = str;
				this.fireGesture(str);
				
//				this.gestures.get(str).clearStatus();

				/*TODO: filtrar gestos a serem reiniciados. 
				 * Pois desta forma não é possível haver gestos concorrentes
				 * Talvez considerar apenas gestos realizados com as mesmas partes*/
				clearAll();
				break;
			}
		}
	}

	private void clearAll() {
		for(Gesture gesture: this.gestures.values()){
			gesture.clearStatus();
		}
	}

	private void fireGesture(String gestureName) {
		System.out.println(gestureName);
		if(gestureName == "PassRight" || gestureName == "PassRightRHand"){
			favoritesScreen.keyPressed((char)CODED, RIGHT);
		}
		else if(gestureName == "PassLeft"){
			favoritesScreen.keyPressed((char)CODED, LEFT);
			
		}
		else if(gestureName == "PassUp"){
			favoritesScreen.keyPressed((char)CODED, UP);
			
		}
		else if(gestureName == "PassDown" || gestureName == "PassDownRight"){
			favoritesScreen.keyPressed((char)CODED, DOWN);
			
		}
		else if(gestureName == "Click"){
			favoritesScreen.mouseClicked();
		}
		else if(gestureName == "GoBack"){
			
		}
		else if(gestureName == "Over"){
			
		}
	}

	public void mouseMoved() {
		favoritesScreen.mouseMoved(mouseX, mouseY, pmouseX, pmouseY);
	}

	public void keyPressed() {
		favoritesScreen.keyPressed(key, keyCode);
	}

	private void showDirection(Skeleton.SkeletonPart part, Skeleton.SkeletonPart toPart) {
		Collection<Direction> directions = DirectionRecognizer.getInstance().getPartDirections(userSkeleton, part,toPart);
		String partName = part.toString();
		partName += "[";
		for(Direction dir : directions){
			partName += dir + " ";
		}
		partName += "]";
		
		drawBodyPart(partName, userSkeleton.getPart(part));
	}
	

	public void drawBodyPart(String partName, BodyPart part){
		this.pushStyle();
			PVector center = new PVector();
			part.getCentralPosition(center);
			context.convertRealWorldToProjective(center, center);
			
			drawLimb(part);
			ellipse(center.x,center.y,25,25);
			
			fill(255,0,0);
			text(partName, center.x,center.y);
		this.popStyle();
	}

	private void drawLimb(BodyPart part) {
		PVector start = new PVector();
		PVector end = new PVector();
		context.convertRealWorldToProjective(part.getStartPosition(), start);
		context.convertRealWorldToProjective(part.getEndPosition(), end);
		
		line(start.x,start.y,end.x,end.y);
	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId)
	{
	  println("onNewUser - userId: " + userId);
	  println("  start pose detection");
	  
	  if(autoCalib)
	    context.requestCalibrationSkeleton(userId,true);
	  else    
	    context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
	  println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
	  println("onExitUser - userId: " + userId);
	}

	public void onReEnterUser(int userId)
	{
	  println("onReEnterUser - userId: " + userId);
	}

	public void onStartCalibration(int userId)
	{
	  println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
	  println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);
	  
	  if (successfull) 
	  { 
	    println("  User calibrated !!!");
	    context.startTrackingSkeleton(userId); 
	  } 
	  else 
	  { 
	    println("  Failed to calibrate user !!!");
	    println("  Start pose detection");
	    context.startPoseDetection("Psi",userId);
	  }
	}

	public void onStartPose(String pose,int userId)
	{
	  println("onStartPose - userId: " + userId + ", pose: " + pose);
	  println(" stop pose detection");
	  
	  context.stopPoseDetection(userId); 
	  context.requestCalibrationSkeleton(userId, true);
	 
	}

	public void onEndPose(String pose,int userId)
	{
	  println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

}
