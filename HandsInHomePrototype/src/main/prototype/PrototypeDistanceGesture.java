package main.prototype;

import gestures.Direction;
import gestures.DirectionRecognizer;
import gestures.DistanceGesture;
import gestures.Position;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonPoint;
import gestures.skeleton.SkeletonPoint.PartPoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import main.utils.AppDrawSkeleton3DTemplate;
import main.utils.VectorDrawer;
import processing.core.PApplet;
import processing.core.PMatrix3D;
import processing.core.PVector;

@SuppressWarnings("serial")
public class PrototypeDistanceGesture extends AppDrawSkeleton3DTemplate{
    static public void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", PrototypeDistanceGesture.class.getName() });
    }

    static final PVector XAXIS = new PVector(1, 0, 0);
    static final PVector YAXIS = new PVector(0, 1, 0);
    static final PVector ZAXIS = new PVector(0, 0, 1);
    
	private SkeletonPoint endPoint;
	private SkeletonPoint basePoint;
	private VectorDrawer vecDrawer;
	DistanceGesture distGesture;
	
	public void setup(){
		super.setup();
		this.endPoint = new SkeletonPoint(SkeletonPart.RightHand);
		this.basePoint = new SkeletonPoint(SkeletonPart.RightShoulder, PartPoint.End);
		vecDrawer = new VectorDrawer(this.g);
		
		distGesture = new DistanceGesture(new SkeletonPoint(SkeletonPart.RightHand, PartPoint.End)
										, new SkeletonPoint(SkeletonPart.RightShoulder, PartPoint.End)
										, Direction.RIGHT, 200.0, null);
	}
	
	
	public void draw(){
		super.draw();
		
		int[] userList = this.getUsers();
		if(userList.length > 0){
			for(int i=0; i < userList.length; ++i){
				Skeleton skel = this.getSkeleton(userList[i]);
				if(skel != null){
					Collection<Position> positions = calculateRelativePosition(skel);
					Collection<Float> distances = calculateRelativeDistance(skel);
					
					drawDistanceVector(skel);
					drawBodyAxis(skel);
					if(frameCount % 10 == 0){
						System.out.println(distances);
					}
					text(positions.toString(),5,5);
					text(distGesture.update(skel).toString(),5,15);
				}
			}
		}
	}

	private Collection<Position> calculateRelativePosition(Skeleton skel) {
		PMatrix3D rot = DirectionRecognizer.rotationFromTo(skel.getFrontDirection(), PVector.mult(ZAXIS, -1));
		PVector rotBase = DirectionRecognizer.getInstance().rotateVector(this.basePoint.getPosition(skel), rot);
		PVector rotEnd = DirectionRecognizer.getInstance().rotateVector(this.endPoint.getPosition(skel), rot);
		
		Set<Position> positions = new HashSet<Position>();

		positions.add((rotEnd.x > rotBase.x? Position.Right : Position.Left));
		positions.add((rotEnd.y > rotBase.y? Position.Above : Position.Below));
		positions.add((rotEnd.z > rotBase.z? Position.Back : Position.Front));
		
		return positions;
	}
	private Collection<Float> calculateRelativeDistance(Skeleton skel) {
		PMatrix3D rot = DirectionRecognizer.rotationFromTo(skel.getFrontDirection(), PVector.mult(ZAXIS, -1));
		PVector rotBase = DirectionRecognizer.getInstance().rotateVector(this.basePoint.getPosition(skel), rot);
		PVector rotEnd = DirectionRecognizer.getInstance().rotateVector(this.endPoint.getPosition(skel), rot);
		
		List<Float> distances = new ArrayList<Float>(3);

//		distances.add((int)(rotEnd.x - rotBase.x));
//		distances.add((int)(rotEnd.y - rotBase.y));
//		distances.add((int)(rotEnd.z - rotBase.z));
		distances.add((float) Math.round(rotEnd.x - rotBase.x));
		distances.add((float) Math.round(rotEnd.y - rotBase.y));
		distances.add((float) Math.round(rotEnd.z - rotBase.z));
		
		return distances;
	}


	private void drawBodyAxis(Skeleton skel) {
		this.vecDrawer.drawVectors(skel.getAxis(), skel.getPart(SkeletonPart.Body).getCentralPosition(), 100);
	}
	private void drawDistanceVector(Skeleton skeleton) {
		if(skeleton != null){
			

			PVector axis = skeleton.getRightDirection();
			
			PVector endRelativePos = PVector.sub(this.endPoint.getPosition(skeleton), basePoint.getPosition(skeleton));
			endRelativePos.normalize();
			PVector endPointProjection = PVector.mult(axis, PVector.dot(axis, endRelativePos));
			
			pushStyle();
				stroke(255,0,0);
				vecDrawer.draw(endPointProjection , basePoint.getPosition(skeleton),200);
				stroke(255,255,0);
				VectorDrawer.drawVector(this.g, endRelativePos , basePoint.getPosition(skeleton));
			popStyle();
		}
	}

	
//	private float getPositionOverAxis(SkeletonPart skelPart, Skeleton skeleton, PVector axis){
//		return PVector.dot(skeleton.getPart(skelPart).getCentralPosition(), axis);
//	}
//	
//	private float calculateDistanceOverAxis(Skeleton skeleton, PVector[] bodyAxis) {
//		PVector axis = chooseAxis(bodyAxis);
////		PVector direction = PVector.sub(skeleton.getPart(this.part).getCentralPosition()
////				, skeleton.getPart(this.partBase).getCentralPosition());
////		float projection = PVector.dot(direction, axis);
//
//		float positionOverAxisBase = PVector.dot(basePoint.getPosition(skeleton), axis);
//		float positionOverAxis     = PVector.dot(endPoint.getPosition(skeleton), axis);
//
//		float distance = positionOverAxis - positionOverAxisBase;
//		
//		System.out.println("Distance from " + this.endPoint + " to " + this.basePoint + " over " + axis + " is " + distance);
//		
//		return distance;
//	}

//	private PVector chooseAxis(PVector[] bodyAxis) {
////		int axisIndex = DistanceGesture.directionToAxisIndex.get(direction);
//		int axisIndex = 0;
//		return bodyAxis[axisIndex];
//	}
}
