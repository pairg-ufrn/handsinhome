package main.utils;

import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import processing.core.PGraphics;
import processing.core.PVector;

public class SkeletonDrawer3D {
	private final List<SkeletonPart> partsToDraw;
	private float centralPartSize;
	private float endPartSize;
	private float startPartSize;
	
	public SkeletonDrawer3D(){
		partsToDraw = new LinkedList<SkeletonPart>();
		for(SkeletonPart part : SkeletonPart.values()){
			partsToDraw.add(part);
		}

		centralPartSize = 20;
		startPartSize 	= 10;
		endPartSize 	= 10;
	}
	
	public float getCentralPartSize() {
		return centralPartSize;
	}

	public float getEndPartSize() {
		return endPartSize;
	}

	public float getStartPartSize() {
		return startPartSize;
	}

	public void setCentralPartSize(float centralPartSize) {
		this.centralPartSize = centralPartSize;
	}

	public void setEndPartSize(float endPartSize) {
		this.endPartSize = endPartSize;
	}

	public void setStartPartSize(float startPartSize) {
		this.startPartSize = startPartSize;
	}

	public List<SkeletonPart> getPartsToDraw(){
		return Collections.unmodifiableList(this.partsToDraw);
	}
	public void addPartToDraw(SkeletonPart part){
		if(!this.partsToDraw.contains(part)){
			partsToDraw.add(part);
		}
	}
	public void removePartToDraw(SkeletonPart part){
		if(this.partsToDraw.contains(part)){
			partsToDraw.remove(part);
		}
	}
	
	
	public void drawSkeleton(PGraphics graphics, Skeleton skeleton){
		for(SkeletonPart part : partsToDraw){
			drawPart(graphics,skeleton,part);
		}
	}
	
	public void drawPart(PGraphics graphics, Skeleton skeleton, SkeletonPart part){
		BodyPart bodyPart = skeleton.getPart(part);
		
		graphics.pushStyle();			
			drawLimb(graphics,bodyPart);
			drawSphere(graphics, bodyPart.getStartPosition(), 10, graphics.color(0,255,0));
			drawSphere(graphics, bodyPart.getEndPosition(), 10, graphics.color(0,255,0));
			drawSphere(graphics, bodyPart.getCentralPosition(), 20, graphics.color(0,0,255));
		graphics.popStyle();
	}
	private void drawSphere(PGraphics graphics, PVector loc, float size, int color){
		graphics.pushStyle();
		graphics.pushMatrix();
			graphics.noStroke();
			graphics.fill(color);
			graphics.translate(loc.x, loc.y, loc.z);
			graphics.sphere(size);
		graphics.popMatrix();
		graphics.popStyle();
	}
	public void drawLimb(PGraphics graphics, BodyPart part) {
		this.drawLimb(graphics, part, null);
	}
	public void drawLimb(PGraphics graphics, BodyPart part, Integer color) {
		PVector start = part.getStartPosition();
		PVector end = part.getEndPosition();
		graphics.pushStyle();
			if(color != null){
				graphics.stroke(color);
			}
			graphics.line(start.x,start.y,start.z, end.x,end.y,end.z);
		graphics.popStyle();
	}
}
