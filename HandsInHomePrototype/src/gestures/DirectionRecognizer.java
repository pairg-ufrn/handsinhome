package gestures;


import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;

import java.util.Collection;
import java.util.HashSet;

import processing.core.PMatrix3D;
import processing.core.PVector;

public class DirectionRecognizer {
	private static final float defaultMinThreshold = (float)Math.sin(Math.PI / 6.0);
	private float minThreshold;

	private static final DirectionRecognizer singleton = new DirectionRecognizer();
	public static DirectionRecognizer getInstance(){
		return singleton;
	}

	public DirectionRecognizer(){
		this(DirectionRecognizer.defaultMinThreshold);
	}
	public DirectionRecognizer(float minRecognizeThreshold){
		//TODO: (?)permitir alterar threshold após criação
		this.minThreshold = minRecognizeThreshold;
	}
	

    private static final PVector XAXIS = new PVector(1,0,0);
    private static final PVector YAXIS = new PVector(0,1,0);
    private static final PVector ZAXIS = new PVector(0,0,1);
    private static final PVector[] AXIS = new PVector[]{XAXIS, YAXIS, ZAXIS};

	/**
	 * Calcula as direções absolutas de skelPart
	 * */
	public Collection<Direction> getPartDirections(Skeleton skeleton, SkeletonPart skelPart) {
		  return getRelativeDirections(skeleton.getPart(skelPart).getDirection(), AXIS);
	}
	
	/**
	 * Calcula as direções de skelPart relativa a skelPartBase
	 * 		utiliza a direção do corpo como referência.
	 * */
	public Collection<Direction> getPartDirections(Skeleton skeleton, SkeletonPart skelPart, SkeletonPart skelPartBase) {
		  PVector[] baseAxis = getRelativeAxisToBody(skeleton, skelPartBase);
		  
		  return getRelativeDirections(skeleton.getPart(skelPart).getDirection(), baseAxis);
	}
	
	/**
	 * @param direction	- um vetor unitário indicando uma determinada direção
	 * @param axis		- um array contendo 3 vetores unitários que indicam as direções dos 3 eixos (X,Y,Z) relativos 
	 * @return uma coleção contendo as direções relativas do vetor direction em relação aos 3 eixos em axis*/
	public Collection<Direction> getRelativeDirections(PVector direction, PVector[] axis) {
		double projX = PVector.dot(direction, axis[0]);
		double projY = PVector.dot(direction, axis[1]);
		double projZ = PVector.dot(direction, axis[2]);
		
		Collection<Direction> relativeDirections = new HashSet<Direction>();

		if(Math.abs(projX) >= minThreshold){
			relativeDirections.add(projX > 0 ? Direction.RIGHT : Direction.LEFT);
		}
		if(Math.abs(projY) >= minThreshold){
			relativeDirections.add(projY > 0 ? Direction.UP : Direction.DOWN);
		}
		if(Math.abs(projZ) >= minThreshold){
//			relativeDirections.add(projZ > 0 ? Direction.FRONT : Direction.BACK);
			relativeDirections.add(projZ < 0 ? Direction.FRONT : Direction.BACK);
		}
		
		return relativeDirections;
	}

	public PVector[] getRelativeAxisToBody(Skeleton skeleton, SkeletonPart skelPartBase){
		  BodyPart partBase   = skeleton.getPart(skelPartBase);
		  PVector bodyFrontDir = skeleton.getFrontDirection();

		  PMatrix3D rotBase = DirectionRecognizer.rotationFromTo(bodyFrontDir, partBase.getDirection());
		  PVector[] baseAxis = getRotatedAxis(rotBase);
		  
		  return baseAxis;
	}
		
	public PVector[] getRotatedAxis(PMatrix3D rot){
		PVector[] axis = new PVector[3];
		axis[0] = rotateVector(XAXIS, rot);
		axis[1] = rotateVector(YAXIS, rot);
		axis[2] = rotateVector(ZAXIS, rot);
		
		return axis;
	}
	
	public PVector rotateVector(PVector vec, PMatrix3D rot){
		PVector rotatedVector = new PVector();
		rot.mult(vec, rotatedVector);
		
		return rotatedVector;
	}
	
	public static PMatrix3D rotationFromTo(PVector dirFrom, PVector dirTo){
		//Obter angulo entre vetores
		float angle = PVector.angleBetween(dirFrom,dirTo);
		//Obter vetor normal
		PVector vecNormal = new PVector();
		PVector.cross(dirFrom,dirTo,vecNormal);
		vecNormal.normalize();
		  
		//Criar matriz de rotacao
		PMatrix3D mRot = new PMatrix3D();
		mRot.rotate(angle,vecNormal.x,vecNormal.y,vecNormal.z);//Rotaciona sobre eixo da normal
		  
		return mRot;
	}


}
