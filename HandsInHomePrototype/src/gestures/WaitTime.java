package gestures;


public enum WaitTime {
	SmallTime(1000), MediumTime(2000), LargeTime(4000);
	
	private WaitTime(long waitTime){
		this.time = waitTime;
	}
	
	private long time;//Tempo dado em milisegundo
	public long getTime(){
		return time;
	}
}
