package gestures;


import java.util.HashMap;
import java.util.Map;

//TODO: (?) unir Direction e Position em uma só classe?
public enum Position{
	Left, Right, Front, Back, Above, Below;
	
	private static Map<Position, Position> oppositeMap = createOppositeMap();

	private static Map<Position, Position> createOppositeMap() {
		Map<Position, Position> opposite = new HashMap<Position, Position>();
		opposite.put(Left , Right);
		opposite.put(Right, Left);
		opposite.put(Front, Back);
		opposite.put(Back , Front);
		opposite.put(Above   , Below);
		opposite.put(Below , Above);
		
		return opposite;
	}
	
	public Position getOposite(Position pos){
		return oppositeMap.get(pos);
	}
};
