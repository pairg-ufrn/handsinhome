package gestures.skeleton;

import java.io.Serializable;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;

import processing.core.PVector;

public class Skeleton implements Serializable, Cloneable{
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -2268118878518607449L;
	
	public enum SkeletonPart{
		Body,
		
		Head, 
		RightHand	 , LeftHand,
		RightArm	 , LeftArm, 
		RightForearm , LeftForearm,
		RightShoulder, LeftShoulder,
		Chest, 
		Abdomen,
		RightHip	 , LeftHip,
		RightThigh	 , LeftThigh,
		RightLeg	 , LeftLeg
	}
	
	//TODO: criar modo de "conectar" partes do esqueleto
	private Map<SkeletonPart, BodyPart> skeletonParts;

	
	public Skeleton(){
		skeletonParts = new EnumMap<Skeleton.SkeletonPart, BodyPart>(SkeletonPart.class);
		for(SkeletonPart part : SkeletonPart.values()){
//			skeletonParts.put(part, null);
			skeletonParts.put(part, new BodyPart());
		}
	}
	
	public BodyPart getPart(SkeletonPart part){
		if(skeletonParts.containsKey(part)){
			return skeletonParts.get(part);
		}
		return null;
	}
	public void setPart(SkeletonPart part, BodyPart bodyPart){
		if(skeletonParts.containsKey(part)){
			skeletonParts.put(part, bodyPart);
		}
	}
	public Collection<BodyPart> getParts(){
		return skeletonParts.values();
	}

	public PVector getPosition(){
		return skeletonParts.get(SkeletonPart.Chest).getCentralPosition();
	}

	public PVector[] getAxis(){
		return new PVector[]{getRightDirection(), getUpDirection(), PVector.mult(getFrontDirection(), -1)};
	}
	
	public PVector getFrontDirection(){
		PVector horDir = getRightDirection();
		PVector vertDir = getUpDirection();
		
		PVector frontDir = new PVector();
		PVector.cross(vertDir, horDir, frontDir);
		frontDir.normalize();
		return frontDir;
	}
	public PVector getUpDirection(){
		PVector up = skeletonParts.get(SkeletonPart.Chest).getDirection();
		up.mult(-1);
		return up;
	}
	public PVector getRightDirection(){
		PVector endLeftShoulder = this.skeletonParts.get(SkeletonPart.LeftShoulder).getEndPosition().get();
		PVector endRightShoulder = this.skeletonParts.get(SkeletonPart.RightShoulder).getEndPosition().get();
		
		PVector horDir = PVector.sub(endRightShoulder, endLeftShoulder);
		horDir.normalize();
		
		return horDir;
	}
	

	@Override
    public boolean equals(Object obj) {
	    if (this == obj){
		    return true;
	    }
	    if (obj == null || !this.getClass().equals(obj.getClass())){
		    return false;
	    }
	    
	    Skeleton other = (Skeleton) obj;

		for(SkeletonPart part : SkeletonPart.values()){
			BodyPart thisPart = this.getPart(part);
			BodyPart otherPart = other.getPart(part);
			
			if(checkEquals(thisPart, otherPart) == false){
				//Não é igual, encerra
				return false;
			}
		}
	    
		return true;
    }
	private boolean checkEquals(Object someAttr, Object otherAttr) {
	    return (someAttr == null && otherAttr == null) || someAttr.equals(otherAttr);
    }
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
    public int hashCode() { //gerado pelo eclipse
	    final int prime = 31;
	    int result = 1;

		for(SkeletonPart part : SkeletonPart.values()){
		    result = incrementHash(prime, result, this.getPart(part));
		}

	    return result;
    }
	int incrementHash(final int seed, final int currentResult, Object member){
		return seed * currentResult + (member == null ? 0 : member.hashCode());
	}
	
	
	@Override
	public Object clone(){
		Skeleton skel = new Skeleton();
		for(SkeletonPart part : SkeletonPart.values()){
			skel.setPart(part, (BodyPart)this.getPart(part).clone());
		}
		return skel;
	}
}
