package gestures;


import gestures.skeleton.Skeleton;

public class HoldGesture extends AbstractGesture {

	private Gesture holdGesture;
	private long waitTime;
	
	private long startTime;
	
	public HoldGesture(Gesture gesture) {
		this(gesture,WaitTime.MediumTime);
	}
	public HoldGesture(Gesture gesture, WaitTime waitTime) {
		this(gesture, waitTime.getTime());
	}
	public HoldGesture(Gesture gesture, long waitTime) {
		this.holdGesture = gesture;
		this.waitTime = waitTime;
		
		startTime = 0;
	}

	@Override
	public RecognitionStatus update(Skeleton skeleton) {
		RecognitionStatus holdCurrentStatus = this.holdGesture.update(skeleton);
		long currentTime = System.currentTimeMillis();
		

		if(holdCurrentStatus == RecognitionStatus.recognized){	
			if(this.currentStatus == RecognitionStatus.notRecognized)
			{
				//inicia contagem de tempo
				startTime = currentTime;
				this.currentStatus = RecognitionStatus.inProgress;
			}
			else if((currentTime - startTime) >= waitTime){
				this.currentStatus = RecognitionStatus.recognized;
			}
		}
		else{
			this.clearStatus();
		}
		
		return getCurrentStatus();
	}

}
