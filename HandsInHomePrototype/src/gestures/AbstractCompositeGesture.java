package gestures;


import gestures.skeleton.Skeleton;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractCompositeGesture extends AbstractGesture{
	protected List<Gesture> gestures;

	public AbstractCompositeGesture() {
		super();
		this.gestures = this.instantiateList();
	}
	protected List<Gesture> instantiateList(){
		return new LinkedList<Gesture>();
	}
	public AbstractCompositeGesture(Gesture... gestures) {
		this();
		this.addAll(gestures);
	}
	protected void addAll(Gesture... gestures){
		Collections.addAll(this.gestures, gestures);
	}

	public Iterable<Gesture> getGestures() {
		return gestures;
	}
	
	protected Gesture getGesture(int index){
		return this.gestures.get(index);
	}

	public void addGesture(Gesture gesture) {
		gestures.add(gesture);
	}

	public void removeGesture(Gesture gesture) {
		gestures.remove(gesture);
	}

	public abstract RecognitionStatus update(Skeleton skeleton);
	
	@Override
	public void clearStatus(){
		super.clearStatus();
		for(Gesture gesture : this.gestures){
			gesture.clearStatus();
		}
	}
}