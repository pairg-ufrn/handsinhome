package gestures;


import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import processing.core.PVector;

//TODO: utilizar HoldGesture para gerenciar o tempo
public class HoldPositionGesture extends AbstractGesture {	
	/** A distancia máxima que a part pode variar para que o gesto continue válido*/
	private double maxHoldDistance;
	private static double defaultMaxHoldDistance = 60; 

	private long waitTime;	
	private long startTime;
	private SkeletonPart part;
	private PVector startPos;
	
	public HoldPositionGesture(SkeletonPart part) {
		this(part,WaitTime.MediumTime);
	}
	public HoldPositionGesture(SkeletonPart part, WaitTime waitTime) {
		this(part,waitTime.getTime(), defaultMaxHoldDistance);
	}
	public HoldPositionGesture(SkeletonPart part, long waitTime) {
		this(part,waitTime, defaultMaxHoldDistance);
	}
	public HoldPositionGesture(SkeletonPart part, long waitTime, double maxHoldDistance) {
		this.part = part;
		this.maxHoldDistance = maxHoldDistance;
		this.waitTime = waitTime;
		
		startTime = 0;
	}

	@Override
	public RecognitionStatus update(Skeleton skeleton) {
		long currentTime = System.currentTimeMillis();
		PVector currentPos = skeleton.getPart(this.part).getCentralPosition();
		if(this.currentStatus == RecognitionStatus.notRecognized)
		{
			//inicia contagem de tempo e armazena posição inicial
			startTime = currentTime;
			this.startPos = currentPos;
			this.currentStatus = RecognitionStatus.inProgress;
		}
		else{
			double currentDistance = PVector.dist(currentPos, startPos);
			if(currentDistance <= this.maxHoldDistance){		
				if((currentTime - startTime) >= waitTime){
					this.currentStatus = RecognitionStatus.recognized;
				}
			}
			else{
				this.clearStatus();
			}
		}
		
		return getCurrentStatus();
	}
	
	@Override
	public void clearStatus(){
		super.clearStatus();
		this.startTime = 0;
		this.startPos = null;
	}

}
