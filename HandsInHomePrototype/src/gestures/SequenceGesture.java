package gestures;


import gestures.skeleton.Skeleton;

public class SequenceGesture extends AbstractCompositeGesture{
	/** currentGestureIndex representa o indice que aponta para o gesto que está sendo observado agora.
	 * 		currentGestureIndex deve sempre ser um índice válido (entre 0 e o número de gestos)
	 * 			exceto quando não houver nenhum gesto na lista
	 * */
	private int currentGestureIndex; 


	public SequenceGesture(){
		super();
		currentGestureIndex = 0;
	}
	public SequenceGesture(Gesture... gestures){
		super(gestures);
		currentGestureIndex = 0;
	}
	
	@Override
	public void addGesture(Gesture gesture){
		super.addGesture(gesture);
		this.clearStatus();
	}
	@Override
	public void removeGesture(Gesture gesture){
		super.removeGesture(gesture);
		this.clearStatus();
	}
	
	@Override
	public void clearStatus(){
		super.clearStatus();
		currentGestureIndex = 0;
	}

	@Override
	public RecognitionStatus update(Skeleton skeleton) {
		if(gestures.size() > 0 && currentGestureIndex < this.gestures.size()){
			Gesture currentGesture = this.getGesture(currentGestureIndex);
			RecognitionStatus previousGestureStatus = currentGesture.getCurrentStatus();
			RecognitionStatus currentGestureStatus = currentGesture.update(skeleton);
			
			if(currentGestureStatus == RecognitionStatus.recognized){
//				System.out.println(this.toString() + " recognized gesture " + currentGestureIndex + " -> " + currentGesture.toString());
				advanceToNextGesture();
			}
			else if(this.currentStatus != RecognitionStatus.notRecognized){
				boolean currentRegressed = gestureStatusRegressed(currentGesture, previousGestureStatus);
				boolean previousRegressed = previousGestureRegressed(currentGesture, skeleton);
//				if( currentRegressed || previousRegressed)
				if( currentRegressed && previousRegressed) //TODO: testar para ver se está certo!
				{
					this.clearStatus();//reinicia
				}
			}
		}
		
		return currentStatus;
	}
//	@Override
//	public RecognitionStatus update(Skeleton skeleton) {
//		if(gestures.size() > 0){
//			if(currentGestureIndex >= this.gestures.size()){
//				this.clearStatus();
//			}
//			RecognitionStatus currentGestureStatus = this.getGesture(currentGestureIndex).update(skeleton);
//			if(currentGestureStatus == RecognitionStatus.recognized){
//				advanceToNextGesture();
//			}
//			else if(!regressedCurrentStatus(currentGestureStatus) 
//					&& !regressedPreviousStatus(currentGestureStatus,skeleton))
//			{
//				previousCurrentStatus = currentGestureStatus;
//			}
//			else{
//				this.clearStatus();//reinicia
//			}
//		}
//		
//		return currentStatus;
//	}
	private boolean gestureStatusRegressed(Gesture gesture, RecognitionStatus previousGestureStatus) {
		//TODO: (?) considerar também mudança de Recognized para inProgress??
		return previousGestureStatus == RecognitionStatus.inProgress 
				&& gesture.getCurrentStatus()  == RecognitionStatus.notRecognized;
	}
	private boolean previousGestureRegressed(Gesture currentGesture, Skeleton skeleton) {
		//Se tem gesto anterior e o gesto atual não foi reconhecido ainda
		if(currentGestureIndex > 0 && currentGesture.getCurrentStatus() == RecognitionStatus.notRecognized){
			//Retorna verdadeiro se o gesto anterior deixou de ser reconhecido
			return (this.getGesture(currentGestureIndex - 1).update(skeleton) != RecognitionStatus.recognized);
		}
		return false;
	}
	private void advanceToNextGesture() {
		++currentGestureIndex;
		if(currentGestureIndex == this.gestures.size()){
			this.currentStatus = RecognitionStatus.recognized;
		}
		else if(currentGestureIndex == 1){//reconheceu o primeiro gesto
			this.currentStatus = RecognitionStatus.inProgress;
		}
	}

}
