package gui.animations;

import gui.components.Component;

import java.awt.Point;

import processing.core.PGraphics;

public class TranslateAnimation implements ComponentAnimation{
	/** Representa a porcentagem (um valor de 0.0 a 1.0) da animação que foi realizada*/
	private double progress;
	/** O número de frames necessário para concluir a animação*/
	private int frameCount;
	private boolean isReverse;
	private boolean isPlaying;
	
	private Point movement;

	private static final int defaultFrameCount = 100;
	
	public TranslateAnimation(double moveX, double moveY){
		this(moveX, moveY,defaultFrameCount);
	}
	public TranslateAnimation(double moveX, double moveY, int frameCount){
		isReverse = false;
		isPlaying = false;
		progress = 0.0;
		this.frameCount = frameCount;
		this.movement = new Point();
		movement.setLocation(moveX, moveY);
	}
	
	@Override
	public void play() {
		this.isPlaying = true;
	}

	@Override
	public void setReverse(boolean reverse) {
		this.isReverse = reverse;
	}

	@Override
	public void pause() {
		this.isPlaying = false;
	}

	@Override
	public void end() {
		this.isPlaying = false;
		this.progress = 0;
	}

	@Override
	public boolean isPlaying() {
		return this.isPlaying;
	}

	@Override
	public boolean isReverse() {
		return this.isReverse;
	}

	@Override
	public boolean isReversible() {
		return true;
	}

	@Override
	public void next() {
		/* Acrescenta ao progresso a quantia equivalente a 1 frame.
		 * Obtem essa quantia dividindo o total do progresso (1 == 100%)
		 * pelo número de frames que a animação deve durar*/
		this.progress += 1.0/(double)this.frameCount;
		if(this.progress > 1.0){
			this.progress = 1.0;
		}
	}

	@Override
	public void draw(PGraphics graphics, Component component) {
		if(!this.isPlaying){
			component.draw(graphics);
		}
		else{
			graphics.pushMatrix();
				Point currentMove = this.getCurrentMove();
				graphics.translate((float)currentMove.getX(), (float)currentMove.getY());
				component.draw(graphics);
			graphics.popMatrix();
		}
	}

	private Point getCurrentMove() {
		Point currentMove = new Point();
		double currentProgress = (!this.isReverse? this.progress : 1 - this.progress);
		currentMove.setLocation(this.movement.getX() * currentProgress
							  , this.movement.getY() * currentProgress);
		return currentMove;
	}
	@Override
	public boolean hasEnd() {
		return this.progress >= 1.0;
	}

}
