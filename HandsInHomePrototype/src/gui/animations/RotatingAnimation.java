package gui.animations;
import gui.components.Component;
import processing.core.PGraphics;


public class RotatingAnimation implements ComponentAnimation{
	boolean isPlaying;
	private boolean isReverse;
	float progress;
	private float velocity;

	public RotatingAnimation(){
		this(0.1f);
	}
	public RotatingAnimation(float velocity){
		isPlaying = false;
		isReverse = false;
		progress = 0;
		this.velocity = velocity;
	}
	
	/* (non-Javadoc)
	 * @see gui.animations.ComponentAnimation#start()
	 */
	@Override
	public void play() {
		isPlaying = true;
		isReverse = false;
		progress = 0;
	}
	/* (non-Javadoc)
	 * @see gui.animations.ComponentAnimation#startReverse()
	 */
	@Override
	public void setReverse(boolean reverse) {//FIXME: atualizar método
		isPlaying = true;
		this.isReverse = reverse;
		progress = 1;
	}

	/* (non-Javadoc)
	 * @see gui.animations.ComponentAnimation#pause()
	 */
	@Override
	public void pause() {
		isPlaying = false;
	}
	/* (non-Javadoc)
	 * @see gui.animations.ComponentAnimation#end()
	 */
	@Override
	public void end(){
		progress = (!isReverse ? 1 : 0);
		isPlaying = false;
	}

	/* (non-Javadoc)
	 * @see gui.animations.ComponentAnimation#next()
	 */
	@Override
	public void next() {
		if(isPlaying){
			this.progress += (!isReverse? velocity : -velocity);
			if(!isReverse && progress >= 1){
				this.progress = 1;
				this.isPlaying = false;
			}
			else if(isReverse && progress <= 0){
				this.progress = 0;
				this.isPlaying = false;
				
			}
		}
	}

	@Override
	public boolean hasEnd() {
		return (!isReverse && progress >= 1) || (isReverse && progress <= 0);
	}

	/* (non-Javadoc)
	 * @see gui.animations.ComponentAnimation#isPlaying()
	 */
	@Override
	public boolean isPlaying() {
		return isPlaying;
	}

	
	/* (non-Javadoc)
	 * @see gui.animations.ComponentAnimation#draw(processing.core.PGraphics, gui.components.Component)
	 */
	@Override
	public void draw(PGraphics graphic, Component btn){
		//TODO: separar implementação da animação em outra classe
	    graphic.pushMatrix();
		    int middleX = btn.getX()/ 2;
		    int middleY = btn.getY()/ 2;
//		    float dist = (float) Math.sqrt((middleX*middleX) + (middleY*middleY));
		    
		    graphic.translate(middleX, middleY);
		    float angleRot = (float) ((1.0 - progress) * (-Math.PI));
		    graphic.rotate(angleRot);
	
		    btn.setLocation(middleX, middleY);
		    btn.draw(graphic);
		    btn.setLocation(middleX * 2, middleY * 2);
//		    graphic.rotate((float)Math.PI);
	    graphic.popMatrix();
	}

	/* (non-Javadoc)
	 * @see gui.animations.ComponentAnimation#isReverse()
	 */
	@Override
	public boolean isReverse() {
		return isReverse;
	}

	@Override
	public boolean isReversible() {
		return true;
	}
	
	public float getVelocity() {
		return velocity;
	}
	public void setVelocity(float velocity) {
		this.velocity = (velocity <= 1 ? velocity : 1);
	}
}
