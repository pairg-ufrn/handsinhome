package gui.home;

import gui.components.AbstractComponent;
import gui.components.ImageComponent;
import gui.components.ImageManager;
import househub.appliances.ApplianceService;
import processing.core.PGraphics;
import processing.core.PImage;

public class ServiceApplianceView extends AbstractComponent {
	private ApplianceService service;
	private ImageComponent imgComp;
	private int textSize;
	
	public ApplianceService getService() {
		return service;
	}

	public void setService(ApplianceService service) {
		this.service = service;
	}

	public ServiceApplianceView(int x, int y, int width, int height) {
		this(null, x, y, width, height);

	}

	public ServiceApplianceView(ApplianceService service, int x, int y, int width, int height) {
		super(x, y, width, height);
		textSize = 0;
		this.service = service;
		imgComp = new ImageComponent();
		if(service != null){
			updateServiceView();
		}
		updateLayout();
	}

	private void updateServiceView() {
		PImage serviceImg = ImageManager.getInstance().getImage(getService().getView().getNameImage());
		imgComp.setImage(serviceImg);
	}

	@Override
	public void draw(PGraphics graphic) {
		if (this.getService() != null) {
			graphic.pushMatrix();
			graphic.translate(this.getX(), this.getY());
			drawServiceImage(graphic);
			drawServiceName(graphic);
			graphic.popMatrix();
		}

	}


	@Override
	public void setWidth(int Width) {
		this.width = Width;
		this.updateLayout();
	}

	@Override
	public void setHeight(int Height) {
		this.height = Height;
		this.updateLayout();
	}

	@Override
	public void setSize(int width, int height) {
		this.setWidth(width);
		this.setHeight(height);
		this.updateLayout();
	}
	
	private void updateLayout() {
		this.imgComp.setWidth(this.getWidth()); 
//		this.imgComp.setHeight(this.getHeight() - getTextSize(graphic));
		this.imgComp.setHeight(this.getHeight()- textSize);
	}

	private void drawServiceImage(PGraphics graphic) {
		
		if(textSize == 0 && service != null && service.getLabel() != null  && service.getLabel() != "" ){
			textSize = getTextSize(graphic);
			this.imgComp.setHeight(this.getHeight() - textSize);
		}
		
		if (imgComp.getImage() != null) {
			imgComp.draw(graphic);
		}
	}

	private int getTextSize(PGraphics graphic) {
		return (int) (graphic.textAscent() + graphic.textDescent());
	}

	void drawServiceName(PGraphics graphic) {
		graphic.pushStyle();
			int width = this.imgComp.getWidth();
			int height = this.imgComp.getHeight();
			graphic.textAlign(graphic.LEFT, graphic.CENTER);
			graphic.text(this.getService().getView().getLabel(), width, height / 2);
		graphic.popStyle();
	}

}
