package gui.home;

import gui.components.AbstractComponent;
import gui.components.Border;
import gui.components.ComponentAligner;
import gui.components.CompositeAligner;
import gui.components.HorizontalAlignment;
import gui.components.ImageComponent;
import gui.components.ImageManager;
import gui.components.VerticalAlignment;
import gui.components.list.AbstractListLayout;
import gui.components.list.CarouselListContainer;
import gui.components.list.ListLayout;
import gui.components.list.ListSelection;
import gui.components.list.VerticalListLayout;
import househub.appliances.Appliance;
import househub.appliances.ApplianceService;

import java.awt.Point;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import processing.core.PGraphics;
import processing.core.PImage;

public class ApplianceView extends AbstractComponent {
	private CarouselListContainer servicesContainer;
	private Map<ApplianceService, ServiceApplianceView> serviceToView;
	private boolean serviceIsActive;

	private Appliance node;
	private ImageComponent nodeIcon;
	
	public ApplianceView(Appliance node) {
		this(node, 0, 0, 0, 0);
		if (node != null && node.getView().getNameImage() != null) {
			// Obtem altura e largura da imagem utilizada
			this.setSize(this.getImage().width, this.getImage().height);
			this.updateLayout();
		}
	}

	public ApplianceView(Appliance node, int x, int y, int width, int height) {
		super(x, y, width, height);
		this.node = node;
		serviceToView = new HashMap<ApplianceService, ServiceApplianceView>();
		serviceIsActive = false;
		
		servicesContainer = new CarouselListContainer(0, height + 5, 0, 0);
		servicesContainer.setTypeSelection(ListSelection.First);
		servicesContainer.setMaxVisibleComponents(5);
		servicesContainer.setBorder(new Border(45, 0, 0, 0));
		servicesContainer.setComponentBorder(new Border(10, 0, 0, 0));
		servicesContainer.setComponentDimension(width/2, height/2);
		servicesContainer.setLayout(new VerticalListLayout(new Border(5,5,0,0)));
		
		updateNode();
	}

	public void addService(ApplianceService service) {
		ServiceApplianceView servButton = new ServiceApplianceView(service, 0, 0,
				this.getWidth() / 2, this.getHeight() / 2);
		serviceToView.put(service, servButton);
		servicesContainer.addComponent(servButton);
	}

	public void removeService(ApplianceService service) {
		servicesContainer.removeComponent(serviceToView.get(service));
		serviceToView.remove(service);
	}
	
	public Collection<ApplianceService> getServices(){
		return Collections.unmodifiableCollection(this.serviceToView.keySet());
	}

//	public Collection<ServiceNodeView> getServiceButtons() {
//		return serviceToView.values();
//	}

	public Appliance getNode() {
		return node;
	}

	// private PImage getImage() { return nodeIcon.getImage();}
	public PImage getImage() {
//		return (this.node != null ? ImageManager.getInstance().getImage(node.getView().getNameImage()) : null);
		return this.nodeIcon.getImage();
	}

	public ListLayout getServicesLayout() {
		return servicesContainer.getLayout();
	}
	
	public void setNode(Appliance node) {
		this.node = node;
		this.updateNode();
	}

	private void updateNode() {
		nodeIcon = new ImageComponent(0, 0, this.getWidth(), this.getHeight());
		nodeIcon.setImage(ImageManager.getInstance().getImage(node.getView().getNameImage()));
		
		if (node != null) {
			for (ApplianceService serv : node.getServices()) {
				this.addService(serv);
			}
//			this.servicesContainer.getComponentSelected().setSize(
//							this.servicesContainer.getComponentSelected().getWidth() * 2,
//							this.servicesContainer.getComponentSelected().getHeight() * 2);
			this.servicesContainer.setComponentDimension(this.getWidth()/2, this.getHeight()/2);
			this.servicesContainer.updateLayout();
		}
	}

	// public void setImage(PImage img) { nodeIcon.setImage(img);}
	public void setServicesLayout(AbstractListLayout layout) {
		servicesContainer.setLayout(layout);
	}

	@Override
	public boolean isOver(int x, int y) {
		Point ptLocal = convertToLocal(x, y);
		return nodeIcon.isOver((int) ptLocal.getX(), (int) ptLocal.getY())
				|| (serviceIsActive && servicesContainer.isOver(
						(int) ptLocal.getX(), (int) ptLocal.getY()));
	}

	private Point convertToLocal(int x, int y) {
		return new Point(x - this.getX(), y - this.getY()); // FIXME: criar
															// metodo auxiliar
	}

	// TODO: desacoplar showServices e hideServices do mouse
	@Override
	public void mouseEntered() {
		showServices();
	}

	@Override
	public void mouseExited() {
		hideServices();
	}

	public void showServices() {
		serviceIsActive = true;
	}

	public void hideServices() {
		serviceIsActive = false;
	}

	public void draw(PGraphics graphic) {
		graphic.pushMatrix();
			graphic.translate(this.getX(), this.getY());
			nodeIcon.draw(graphic);
			drawNodeName(graphic);
			if (serviceIsActive) {
//				servicesContainer.align(this.getWidth() / 2, this.getHeight() + servicesContainer.getBorder().getTop());
				servicesContainer.draw(graphic);
			}
		graphic.popMatrix();
	}

	void drawNodeName(PGraphics graphic) {
		graphic.pushStyle();
			graphic.textAlign(graphic.CENTER, graphic.TOP);
		
			int width = this.nodeIcon.getWidth();
			int height = this.nodeIcon.getHeight();

			graphic.text(this.getNode().getView().getLabel(), 
					this.nodeIcon.getX(), this.nodeIcon.getY() + height + 5, width, height);
		graphic.popStyle();
	}

	@Override
	public void setWidth(int Width) {
		this.width = Width;
		this.fixServicesDimension();
		this.updateLayout();
	}

	@Override
	public void setHeight(int Height) {
		this.height = Height;
		this.fixServicesDimension();
		this.updateLayout();
	}

	private void fixServicesDimension() {
//		this.servicesContainer.setComponentDimension(this.getWidth() / 2, this.getHeight()/2);
		this.servicesContainer.setComponentDimension(this.getWidth() / 4, this.getHeight()/4);
		this.servicesContainer.updateLayout();
    }

	@Override
	public void setSize(int width, int height) {
		this.setWidth(width);
		this.setHeight(height);
		this.updateLayout();
	}

	public void updateLayout() {
		if (node != null) {
			nodeIcon.setSize(this.getWidth(), this.getHeight());
		} else {
			nodeIcon.setImage(null);
		}
		
		ComponentAligner aligner = new CompositeAligner(HorizontalAlignment.Center, VerticalAlignment.Top);
		aligner.align(this.servicesContainer, this.getWidth() / 2, this.getHeight() + servicesContainer.getBorder().getTop());
	}

	public void select() {
		this.showServices();

	}

	public void deselect() {
		this.hideServices();
	}

	public CarouselListContainer getServicesContainer() {
		return this.servicesContainer;
	}

	public ServiceApplianceView getServiceSelected() {
		return (ServiceApplianceView) this.servicesContainer.getComponentSelected();
	}
}
