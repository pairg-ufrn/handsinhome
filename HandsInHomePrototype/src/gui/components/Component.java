package gui.components;

import processing.core.PGraphics;

public interface Component {
	public int getX();

	public int getY();

	public void setX(int x);

	public void setY(int y);

	public void setLocation(int x, int y);

	public int getWidth();

	public int getHeight();

	public void setWidth(int width);

	public void setHeight(int height);

	public void setSize(int width, int height);

	public boolean isOver(int x, int y);

	public void select();

	public void deselect();

	public void align(int xOrigin, int yOrigin);

	public Border getBorder();

	public void setBorder(Border border);

	public void draw(PGraphics graphics);
}
