package gui.components;

import processing.core.PGraphics;

public class SliderComponent extends AbstractComponent {

	private float value, valueMin, valueMax, ratio;

	public SliderComponent(int X, int Y, int Width, int Height, float valueMax,
			float valueMin, float valueInitial, float ratio) {
		super(X, Y, Width, Height);
		this.value = valueInitial;
		this.valueMax = valueMax;
		this.valueMin = valueMin;
		this.ratio = ratio;
		// Caso não for fornecido a taxa de variação pode ser calculado uma taxa
		// padrão
		// int widthtoheight = this.getWidth() - this.getHeight();
		// this.ratio = (float) this.getWidth() / (float) widthtoheight;

	}

	public void draw(PGraphics graphics) {

		graphics.pushStyle();
		graphics.noStroke();
		graphics.fill(204);
		graphics.rect(this.x, this.y, this.width, this.height, 7, 7);
		graphics.fill(255, 0, 0);
		graphics.ellipse(this.x + this.value, this.y + this.height / 2,
				this.height, this.height);
		drawValue(graphics);
		graphics.popStyle();

	}

	public void drawValue(PGraphics graphics) {
		graphics.pushMatrix();
		graphics.fill(30, 255, 0);
		graphics.text(this.value, this.x + this.width + 30, this.y
				+ this.height);

		graphics.popMatrix();
	}

	public void increase() {
		System.out.println("Aqui");
		if (this.value < this.valueMax) {
			this.value += this.ratio;
		}
		System.out.println(this.value);

	}

	public void decrement() {
		System.out.println("Aqui");
		if (this.value > this.valueMin) {
			this.value -= this.ratio;
		}
		System.out.println(this.value);

	}

}