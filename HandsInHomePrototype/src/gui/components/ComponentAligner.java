package gui.components;

public interface ComponentAligner{
    public void align(Component comp, int xOrigin, int yOrigin);
    public void align(Component comp, int xOrigin, int yOrigin, int width, int height);
    
}
