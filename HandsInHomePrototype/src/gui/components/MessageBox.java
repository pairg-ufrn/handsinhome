package gui.components;

import processing.core.PGraphics;

public class MessageBox extends AbstractComponent {
	private static final long DEFAULT_DURATION = 2500; //(em milisegundos)
	private long startTime;
	private long duration;
	private String message;
	private int backgroundColor;
	private int textColor;
	private int textSize;
	private boolean isVisible;
	
	public MessageBox(String message, Container parent){
		this(message, parent, DEFAULT_DURATION);
	}
	public MessageBox(String message, Container parent, long duration){
		assertPositiveDuration(duration);
		
		this.duration = duration;
		this.message = message;
		backgroundColor = 220;
		startTime = 0;
		isVisible = false;
		textSize = 16;
		this.textColor = 0;
	}
	private void assertPositiveDuration(long duration) {
		if(duration < 0){
			throw new IllegalArgumentException("Duration should be a positive number!");
		}
	}
	@Override
	public void draw(PGraphics graphics) {
		if(isVisible){
			if(startTime == 0){
				startTime = System.currentTimeMillis();
			}
			long currentDuration = (System.currentTimeMillis() - startTime);
			if(currentDuration < this.duration){
				long timeToEnd = (duration - currentDuration);
				int alpha = getAlphaAmount(timeToEnd);
	
				graphics.pushStyle();
					graphics.noStroke();
					graphics.fill(backgroundColor,alpha);
					graphics.rect(this.getX(), this.getY(), this.getWidth(), this.getHeight(), 7,7);
					graphics.textAlign(graphics.CENTER, graphics.CENTER);
					graphics.fill(this.textColor,alpha);
					graphics.textSize(this.textSize);
					graphics.text(this.message,this.getX(),this.getY(),this.getWidth(),this.getHeight());
				graphics.popStyle();
			}
			else{
				isVisible = false;
				startTime = 0;
			}
		}
	}
	private int getAlphaAmount(long timeToEnd) {
		float percent = (timeToEnd/(float)(duration));
		
		return (int)(percent*255);
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isVisible() {
		return isVisible;
	}
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
		if(!isVisible){
			startTime = 0;
		}
	}
	public int getBackgroundColor() {
		return backgroundColor;
	}
	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	public int getTextColor() {
		return textColor;
	}
	public void setTextColor(int textColor) {
		this.textColor = textColor;
	}
	public int getTextSize() {
		return textSize;
	}
	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}
	

}
