package gui.components.list;

import java.awt.Dimension;

import gui.components.AbstractContainer;
import gui.components.Border;
import gui.components.Component;
import processing.core.PGraphics;

public class CarouselListContainer extends AbstractContainer {

//	protected ListSelection typeSelection;
	ListView visibleComponents;
	protected int startIndex;
	protected int maxVisibleComponents;

	public CarouselListContainer(int someX, int someY, int someWidth, int someHeight) {
		this(someX, someY, someWidth, someHeight, ListSelection.Middle);
	}

	public CarouselListContainer(int X, int Y, int width, int height, ListSelection typeSelection) 
	{
		this(X, Y, width, height, typeSelection, 5);
	}

	public CarouselListContainer(int X, int Y, int width, int height, 
			ListSelection typeSelection, int countVisibles) 
	{
		super(X, Y, 0, 0);
		CarouselListLayout layout = new CarouselListLayout(50, 50);
		HorizontalListLayout listLayout = new HorizontalListLayout(new Border(10,10,10,10));
		layout.setListLayout(listLayout);
		
		this.visibleComponents = new ListView(X, Y);
		this.visibleComponents.setLayout(layout);
		this.startIndex = 0;
		this.setTypeSelection(typeSelection);
		this.maxVisibleComponents = countVisibles;
	}

	public ListLayout getLayout() {
		return visibleComponents.getLayout();
	}
	private AbstractListLayout getBaseListLayout(){
		//this is ugly!
		return (AbstractListLayout)((CarouselListLayout)this.visibleComponents.getLayout()).getListLayout();
	}
	
	public Border getComponentBorder(){
		return this.getBaseListLayout().getComponentBorder();
	}
	public int getMaxVisibleComponents() {
		return maxVisibleComponents;
	}
	public Component getComponentSelected() {
		return (visibleComponents.getComponentCount() > 0 
				? visibleComponents.getComponent(getVisibleSelectedIndex())
				: null);
	}
	public int getSelectedIndex() {
		Component selectedComponent = this.getComponentSelected();
		if(selectedComponent != null){
			return this.components.indexOf(selectedComponent);
		}
				
		return -1;
	}
	protected int getVisibleSelectedIndex() {
		return this.getTypeSelection().getSelected(visibleComponents);
	}
	public Dimension getComponentDimension(){
		return this.getCarouselLayout().getComponentDimension();
	}
	private CarouselListLayout getCarouselLayout() {
	    return (CarouselListLayout) this.visibleComponents.getLayout();
    }
	public ListSelection getTypeSelection(){
		return this.getCarouselLayout().getTypeSelection();
	}
	
	public void setComponentBorder(int top, int bottom, int left, int right) {
		this.setComponentBorder(new Border(top, bottom, right, left));
    }
	public void setComponentBorder(Border border) {
		this.getBaseListLayout().setComponentBorder(border);
		this.updateLayout();
    }
	public void setMaxVisibleComponents(int maxVisibleCount) {
		this.maxVisibleComponents = maxVisibleCount;
		this.updateLayout();
	}
	public void setLayout(AbstractListLayout layout) {
		this.getCarouselLayout().setListLayout(layout);
		updateLayout();
	} 
	public void setComponentDimension(int width, int height) {
	    this.setComponentDimension(new Dimension(width,height));
    }
	public void setComponentDimension(Dimension dimension) {
	    this.getCarouselLayout().setComponentDimension(dimension);
    }
	public void setTypeSelection(ListSelection typeSelection) {
	    this.getCarouselLayout().setTypeSelection(typeSelection);
    }

	@Override
	public Component getComponentAt(int x, int y) {
		return this.visibleComponents.getComponentAt(x, y);
	}
	
	public void selectNext() {
		if (this.getComponentCount() > 0) {
			int next = (this.startIndex + 1) % this.getComponentCount();
			this.setStartIndex(next);
		}
	}
	public void selectPrevious() {
		if (this.getComponentCount() > 0) {
			/* Subtrai de 1 o indice atual e soma ao total de numeros para
			*  "dar a volta na lista" (se for necessario) em seguido retira o
			*  modulo para corrigir numero */
			int previous = ((this.startIndex - 1) + this.getComponentCount())
					% this.getComponentCount();
//			System.out.println("SelectPrevious. CurrentStartIndex: " + startIndex 
//					+ " Previous: " + previous);
			this.setStartIndex(previous);
		}
	}
	protected void setStartIndex(int index) {
		if (index != this.startIndex && isValidIndex(index)) {
			this.visibleComponents.getComponent(this.getVisibleSelectedIndex()).deselect();
			
//			System.out.println(this.getClass().getSimpleName() + ".setStartIndex to  " + index);
			this.startIndex = index;
			this.updateLayout();

			this.visibleComponents.getComponent(this.getVisibleSelectedIndex()).select();
		}
	}
	protected boolean isValidIndex(int index) {
		return index >= 0 && index < this.getComponentCount();
	}

	@Override
	protected void doLayout() {		
		this.visibleComponents.clear();
		if (this.getComponentCount() > 0) {
//			if (!isValidIndex(this.startIndex)) {
//				startIndex = 0;
//			}

			int count = Math.min(this.components.size(), maxVisibleComponents);
			int index = startIndex;
			visibleComponents.disableLayout();
			for (int i = 0; i < count; ++i) {
//				System.out.println(this.getClass().getSimpleName() + ".doLayout: addComp to visible" 
//						+ index);
				visibleComponents.addComponent(this.getComponent(index));
				index = (i + startIndex + 1) % this.getComponentCount();
			}
			visibleComponents.enableLayout();
			visibleComponents.updateLayout();
			
			this.setWidth(visibleComponents.getWidth());
			this.setHeight(visibleComponents.getHeight());
		}
	}

	public void draw(PGraphics graphics) {

		graphics.pushStyle();
		graphics.pushMatrix();
			graphics.translate(this.getX(), this.getY());
			int index = 0;
			for (Component comp : visibleComponents.getComponents()) {
				if (index != this.getVisibleSelectedIndex()) {
					graphics.fill(0,0,0, 50);				
					graphics.tint(80, 50);
				} else {
					graphics.fill(0,0,0);	
					graphics.noTint();
				}
				comp.draw(graphics);
				index++;
			}
		graphics.popMatrix();
		graphics.popStyle();
	}

}
