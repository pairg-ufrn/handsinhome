package gui.components;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import processing.core.PApplet;
import processing.core.PImage;

public class ImageManager {
	Map<String, PImage> mapImage;
	PApplet parent;
	static ImageManager instance;

	private static String imgDirectory = "images";
	
	public static ImageManager getInstance() {
		if (instance == null) {
			instance = new ImageManager();
		}
		return instance;

	}

	public ImageManager() {
		this.mapImage = new HashMap<String, PImage>();

	}

	public void setPApplet(PApplet parent) {
		System.out.println(parent);
		this.parent = parent;
	}

	public void removeImage(String nameImage) {
		this.mapImage.remove(nameImage);
	}

	public PImage getImage(String nameImage) {
		if (this.parent != null) {
			if(!this.mapImage.containsKey(nameImage)){
				this.loadImage(nameImage);
			}

			return this.mapImage.get(nameImage);
		}
		else{
			return null;
		}

	}

	private void loadImage(String nameImage) {
		PImage img = null;
		
		String fileRelativePath = ImageManager.imgDirectory + File.separator + nameImage;
		File imgFile = new File(fileRelativePath);
		if(imgFile.exists()){
//			//RequestImage carrega a imagem de forma assíncrona, o que é mais rápido, porém gera alguns bugs
//			img = parent.requestImage(ImageManager.imgDirectory + File.separator + nameImage);
			//TODO: ver como substituir loadImage por requestImage
			img = parent.loadImage(ImageManager.imgDirectory + File.separator + nameImage);
	
		}
		else{
			//try load from web
			try{
				new URL(nameImage);//certifica que é uma URL válida
				img = parent.loadImage(nameImage);
				img.save(imgFile.getAbsolutePath());//FIXME: se o nome da imagem é uma URL, ele irá reconstruir o path da imagem
				
				System.out.println("Save image from web");
			}
			catch(MalformedURLException ex){//Lançada caso 'nameImage' não seja uma imagem
				System.err.println(ex);
			}
		}
		
		if(img != null){
			this.mapImage.put(nameImage, img);
		}
		else{
			System.err.println("A imagem " + nameImage + " não foi encontrada ");
		}
	}

}
