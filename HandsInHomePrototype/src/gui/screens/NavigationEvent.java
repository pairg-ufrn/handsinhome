package gui.screens;

public class NavigationEvent {
	public static final String backDestination = "BACK";
	private Screen origin;
	private String destination;

	public NavigationEvent() {
	}

	public NavigationEvent(Screen origin, String destination) {
		this.origin = origin;
		this.destination = destination;
	}

	public Screen getOrigin() {
		return origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setOrigin(Screen origin) {
		this.origin = origin;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
