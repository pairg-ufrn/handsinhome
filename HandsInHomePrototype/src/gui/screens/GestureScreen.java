package gui.screens;

import gui.components.MessageBox;
import processing.core.PGraphics;
import processing.core.PImage;
import SimpleOpenNI.SimpleOpenNI;

public class GestureScreen extends AbstractScreen implements Screen {
	private static final int DEFAULT_BOX_WIDTH = 200;
	private static final int DEFAULT_BOX_HEIGHT = 50;
	private static final int DEFAULT_BOX_BCGCOLOR = -16720896;//essa cor deve ser um verde
	
	private SimpleOpenNI context;
	private MessageBox messageBox;
	
	public GestureScreen(String title, String nameIcon, SimpleOpenNI context) {
		super(title, nameIcon);
		this.context = context;
		if(context != null){
			context.enableRGB();
		}

		messageBox = new MessageBox("", this);
		messageBox.setSize(DEFAULT_BOX_WIDTH, DEFAULT_BOX_HEIGHT);
		messageBox.setBackgroundColor(DEFAULT_BOX_BCGCOLOR);
		this.addComponent(messageBox);
	}

	@Override
	public void doLayout() {

	}

	@Override
	public void draw(PGraphics graphics){
		graphics.background(this.getBackgroundColor());
		if(context != null && context.isInit()){
			PImage rgbImage = context.rgbImage();
			int deltaX = (this.getWidth() - rgbImage.width)/2;
			int deltaY = (this.getHeight() - rgbImage.height)/2;
			graphics.image(rgbImage, deltaX, deltaY);
		}
		super.draw(graphics);
		
	}
	
	@Override
	public void keyPressed(char key, int keyCode){

		if (key == 'b') {
			System.out.println(NavigationEvent.backDestination);
			this.fireNavigationEvent(new NavigationEvent(this, NavigationEvent.backDestination));
		}
		else if (key == ' ') {
			System.out.println("Gesto Reconhecido!");

			this.messageBox.setVisible(false);
			this.messageBox.setMessage("Gesto Reconhecido!");
			this.messageBox.setLocation(this.width/2 - messageBox.getWidth()/2, this.height/2);
			this.messageBox.setVisible(true);
		}
	}
}
