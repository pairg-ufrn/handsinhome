package gui.screens;

import gui.components.ImageManager;
import gui.components.MessageBox;
import gui.components.list.CarouselListContainer;
import gui.components.list.ListSelection;
import gui.home.ApplianceView;
import househub.HomeManager;
import househub.RemoteHomeManager;
import househub.appliances.Appliance;
import househub.appliances.ApplianceService;

import java.util.Collection;

import processing.core.PGraphics;
import processing.core.PImage;

public class HomeObjectScreen extends AbstractScreen {
	CarouselListContainer container;
	private final PImage backgroundImage;
	PImage imgMicroondas;
	PImage imgTorradeira;
	PImage imgGeladeira;
	PImage imgMaquinaLavar;
	PImage imgVentilador;
	
	MessageBox messageBox;

	public HomeObjectScreen() {
		this("favorite", "favorite.png");
	}

	public HomeObjectScreen(String title, String nameIcon) {
		super(title, nameIcon);
		backgroundImage = ImageManager.getInstance().getImage("background.png");
		container = new CarouselListContainer(0, 0, 1000, 1000, ListSelection.Middle);

		messageBox = new MessageBox("", this);
		messageBox.setSize(200, 50);
		messageBox.setBackgroundColor(-16720896);//essa cor deve ser um verde
	}

	@Override
	public void setup() {
		super.setup();
		backgroundImage.resize(this.getWidth(), this.getHeight());
		container.setSize((int)(this.getWidth()*0.9), (int)(this.getHeight()*0.9));
		
		fillNodes();

		this.addComponent(container);
		this.addComponent(messageBox);
		this.container.setComponentDimension(100, 100);
		this.container.updateLayout();
		
		this.updateLayout();
	}

	@Override
	public void clearSetup() {
		super.clearSetup();
		container.clear();
	}

	private void fillNodes() {
		Collection<Appliance> nodes = HomeManager.getInstance().loadAppliances();
		ApplianceView homeNodeView;
		for (Appliance node : nodes) {
			//FIXME: componentes precisam ter seu tamanho ajustado de acordo com tamanho da tela
			homeNodeView = new ApplianceView(node, 0, 0, 100, 100);
			this.container.addComponent(homeNodeView);
		}
		if(nodes.size() > 0){
//			this.container.getComponentSelected().setSize(200, 200);
			((ApplianceView) this.container.getComponentSelected()).select();
		}
	}

//	private void fillNodes() {
//		Collection<Appliance> nodes = HomeManager.getInstance().loadAppliances();
//		ObjectNodeView homeNodeView;
//		for (Appliance node : nodes) {
//			//FIXME: componentes precisam ter seu tamanho ajustado de acordo com tamanho da tela
//			homeNodeView = new ObjectNodeView(node, 0, 0, 100, 100);
//			this.container.addComponent(homeNodeView);
//		}
//		if(nodes.size() > 0){
//			this.container.getComponentSelected().setSize(200, 200);
//			((ObjectNodeView) this.container.getComponentSelected()).showServices();
//		}
//	}


	@Override
	public void draw(PGraphics graphics) {
		// graphics.background(backgroundImage);
		graphics.image(backgroundImage, 0, 0);
//		graphics.background(255);
		
//		container.draw(graphics);
		super.draw(graphics);
	}

	@Override
	public void doLayout() {
		// Atualizar posição da lista
		// Coloca a Lista de Objetos na posição padrão
		int newY = (int) (this.getHeight() / 8.0f) * 1;
//		int newWidth = this.getWidth() - 40;
//		container.setMaxWidth(newWidth);
		container.setY(newY);
//		container.setMaxHeight(200);
		container.align(this.getWidth() / 2, newY);

	}

	@Override
	public void keyPressed(char key, int keyCode) {
		if (key == 65535) {
			if (keyCode == 39) {
				this.container.selectNext();
			} else if (keyCode == 37) {
				this.container.selectPrevious();
			}
			if(this.container.getComponentCount() > 0){
//				ObjectNodeView selectedNode = ((ObjectNodeView) this.container.getComponentSelected());
				ApplianceView selectedNode = ((ApplianceView) this.container.getComponentSelected());
				if (keyCode == 38) {
//					selectedNode.getServices().selectPrevious();
					selectedNode.getServicesContainer().selectPrevious();
				} else if (keyCode == 40) {
//					selectedNode.getServices().selectNext();
					selectedNode.getServicesContainer().selectNext();
				}
			}
		} else {
			if (key == 'a') {
				System.out.println("HOMESCREEN");
				this.fireNavigationEvent(new NavigationEvent(this, "home"));
			} else if (key == 'b') {
				System.out.println("BACK");
				this.fireNavigationEvent(new NavigationEvent(this, "BACK"));
			} else if (key == 'c') {
				System.out.println("CLICK");
				executeSelectedService();				
			}
		}

	}

	private void executeSelectedService() {
//		ObjectNodeView selectedNodeView = ((ObjectNodeView) this.container.getComponentSelected());
		ApplianceView selectedNodeView = ((ApplianceView) this.container.getComponentSelected());
		if(selectedNodeView != null){
			Appliance selectedNode = selectedNodeView.getNode();
			ApplianceService selectedService = selectedNodeView.getServiceSelected().getService();
			
			//TODO: criar confirmação do usuário para execução do serviço
			if(selectedService.getParameters().size() > 0){
				System.out.println(selectedService.getLabel() + " has " + selectedService.getParameters() + " parameters");
				//TODO: permitir a execução do serviço após o Popup
				//TODO: (?)criar método exclusivo para exibição de Popups no ScreenManager
				this.getPopUpScreen().setParameters(selectedService.getParameters());
				this.fireNavigationEvent(new NavigationEvent(this,  this.getTitle()+"popUp"));
			}
			else{
				System.out.println(selectedService.getLabel() + " has 0 parameters");
				this.messageBox.setVisible(false);
				this.messageBox.setMessage("Executar Serviço: " + selectedService.getLabel());
				this.messageBox.setLocation(this.width/2 - messageBox.getWidth()/2, this.height/2);
				this.messageBox.setVisible(true);
			}
			
			RemoteHomeManager.getInstance().executeApplianceService(selectedNode, selectedService);
		}
	}


}
