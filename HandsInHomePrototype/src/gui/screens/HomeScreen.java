package gui.screens;

import gui.components.ImageManager;
import gui.components.list.CarouselListContainer;
import gui.components.list.ListSelection;

import java.util.LinkedList;
import java.util.List;

import processing.core.PGraphics;
import processing.core.PImage;

public class HomeScreen extends AbstractScreen {
	CarouselListContainer container;
	private final PImage backgroundImage;
	List<Screen> screens;
	
	public HomeScreen(List<Screen> screens, String title, String nameIcon) {
		super(title, nameIcon);
		backgroundImage = ImageManager.getInstance().getImage("backgroundHome.png");
		
//		super.setBackgroundImage(ImageManager.getInstance().getImage("backgroundHome.png"));
		container = new CarouselListContainer(0, 0, 1000, 1000,
				ListSelection.Middle);

//		this.screens = screens;
		this.screens = new LinkedList<Screen>();
		this.screens.addAll(screens);
	}

	@Override
	public void setup() {
		super.setup();
		backgroundImage.resize(this.getWidth(), this.getHeight());
		container.setSize((int) (this.getWidth() * 0.9),(int) (this.getHeight() * 0.9));

		fillScreens();

		this.addComponent(container);
		this.container.setComponentDimension(100, 100);
		this.container.updateLayout();
		// System.out.println("Selected: "
		// + this.container.getComponentSelected().toString());
		this.updateLayout();
	}

	@Override
	public void clearSetup() {
		super.clearSetup();
		container.clear();
	}

	private void fillScreens() {
		for (Screen screen : this.screens) {
			// FIXME: componentes precisam ter seu tamanho ajustado de acordo
			// com tamanho da tela
			ScreenView screenView = new ScreenView(screen,100,100);
			this.container.addComponent(screenView);
		}
//		/*FIXME: isso não deveria ser necessário aki, o próprio carrossel deveria garantir 
//		 * que os objetos fossem redimensionados corretamente*/
//		Component selectedComp = this.container.getComponentSelected();
//		if(selectedComp!=null){
//			selectedComp.setSize(200, 200);
//		}
	}

	@Override
	public void draw(PGraphics graphics) {
		if(backgroundImage != null){
			graphics.image(backgroundImage, 0, 0);
		}
		super.draw(graphics);
	}

	@Override
	public void doLayout() {
		// Atualizar posição da lista
		// Coloca a Lista de Objetos na posição padrão
		int newY = (int) (this.getHeight() / 8.0f) * 1;
//		int newWidth = this.getWidth() - 40;
//		container.setMaxWidth(newWidth);
		container.setY(newY);
//		container.setMaxHeight(200);
		container.align(this.getWidth() / 2, newY);
	}

	@Override
	public void keyPressed(char key, int keyCode) {
		if (key == 65535) {
			if (keyCode == 39) {
				this.container.selectNext();
			} else if (keyCode == 37) {
				this.container.selectPrevious();
			}
//			if (keyCode == 38) {
//				((ObjectNodeView) this.container.getComponentSelected())
//						.getServices().selectPrevious();
//			} else if (keyCode == 40) {
//				((ObjectNodeView) this.container.getComponentSelected())
//						.getServices().selectNext();
//			}
		} else {
			if (key == 'b') {
				System.out.println(NavigationEvent.backDestination);
				this.fireNavigationEvent(new NavigationEvent(this, NavigationEvent.backDestination));
			} else if (key == 'c') {
				System.out.println(((ScreenView) this.container
						.getComponentSelected()).getScreen().getTitle());
				this.fireNavigationEvent(new NavigationEvent(this,
						((ScreenView) this.container.getComponentSelected())
								.getScreen().getTitle()));
			}
		}
	}

}
