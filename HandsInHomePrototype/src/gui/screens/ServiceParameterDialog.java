package gui.screens;

import gui.components.Component;
import gui.components.SliderComponent;
import gui.components.list.CarouselListContainer;
import gui.components.list.ListSelection;
import househub.appliances.Parameter;
import househub.appliances.ViewDefinition.ViewComponents;

import java.util.ArrayList;
import java.util.List;

import processing.core.PGraphics;

public class ServiceParameterDialog extends AbstractScreen {
	private final List<Parameter> parameters;
	private final CarouselListContainer container;
	//FIXME: remover 'slider', as interfaces de controle devem ser criadas dinamicamente
	SliderComponent slider;

	public ServiceParameterDialog(String title) {
		this(title, null);
	}
	public ServiceParameterDialog(String title,List<Parameter> parameters) {
		super(title, null);
		this.slider = new SliderComponent(500,
				250, 200, 30, 100, 0, 50, 1);
		this.container = new CarouselListContainer(0, 0, 100, 100, ListSelection.First);

		this.parameters = new ArrayList<Parameter>();
		if(parameters != null){
			this.setParameters(parameters);
		}
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters.clear();
		this.parameters.addAll(parameters);
		
		this.container.clear();
		for (Parameter parameter : parameters) {
			this.container.addComponent(createParameterComponent(parameter));
		}

	}

	private Component createParameterComponent(Parameter parameter) {
		//FIXME: criar interface dos parâmetros
		return this.slider;
	}

	@Override
	public void clearSetup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(PGraphics graphics) {
		graphics.pushMatrix();
		// graphics.tint(255, 50);
		graphics.pushStyle();
		graphics.noStroke();
		graphics.fill(100, 240);
		graphics.rect(graphics.width / 4, 50, graphics.width / 2,
				graphics.height - 100, 7, 7);
		drawNodeName(graphics);
		drawService(graphics);
		this.container.draw(graphics);
		
		graphics.popStyle();
		graphics.popMatrix();

	}

	/**
	 * Desenha interface gráfica dos parâmetros do serviço
	 * @param graphics os "gráficos" onde a interface será desenhada
	 * */
	public void drawService(PGraphics graphics) {
		for (Parameter p : this.parameters) {
			if (p.getView().getType().equals(ViewComponents.SLIDER)) {
				this.slider.draw(graphics);
			}
		}
	}

	void drawNodeName(PGraphics graphics) {
		graphics.pushMatrix();
		graphics.fill(30, 255, 0);
		graphics.textAlign(graphics.CENTER, graphics.BOTTOM);
		for (Parameter parameter : this.parameters) {
			graphics.text(parameter.getView().getLabel(), graphics.width / 2,
					graphics.height / 2);
		}
		graphics.popMatrix();
	}

	@Override
	public void doLayout() {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(char key, int keyCode) {
		if (key == 65535) {
			if (keyCode == 39) {
				this.slider.increase();
			} else if (keyCode == 37) {
				this.slider.decrement();
			}

		} else {
			if (key == 'a') {
				System.out.println("HOMESCREEN");
				this.fireNavigationEvent(new NavigationEvent(this, "home"));
			} else if (key == 'b') {
				this.fireNavigationEvent(new NavigationEvent(this, "BACK"));
			}
		}

	}

}