package househub.network;


public class RequestAnswer {
	private int answerStatus;
	private String message;
	private String methodName;
	private Object content;

	public RequestAnswer() {
		this(0, "", "", null);
	}
	public RequestAnswer(int answerStatus, String methodName, String message) {
		this(answerStatus, methodName, message, null);
	}
	public RequestAnswer(int answerStatus, String methodName, String message, 
			Object content) 
	{
		super();
		this.answerStatus = answerStatus;
		this.message = message;
		this.methodName = methodName;
		this.content = content;
	}



	public int getAnswerStatus() { return answerStatus;}
	public String getMessage() { return message;}
	public String getMethodName() { return methodName;}
	public Object getContent() { return content;}
	
	public void setAnswerStatus(int answerStatus) { this.answerStatus = answerStatus;}
	public void setMessage(String message) { this.message = message;}
	public void setMethodName(String methodName) { this.methodName = methodName;}
	public void setContent(Object content) { this.content = content;}
	
	public boolean isSuccessful(){ return (answerStatus == 1);}
}
