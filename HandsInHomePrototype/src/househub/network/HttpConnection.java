/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package househub.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HttpConnection {

    public final String USER_AGENT = "Mozilla/5.0";

//    private ArrayList<HttpParameter> parameters = new ArrayList<HttpParameter>();
    private Map<String, HttpParameter> parameters;
    private Map<String, HttpParameter> cookies;

    public HttpConnection(){
    	parameters = new HashMap<String, HttpParameter>();
    	cookies = new HashMap<String, HttpParameter>();
    }
    
    // PHPSESSID=asdhalskhdjashdlahsjads
    public void addParameter(String field, String value) {
        this.parameters.put(field, new HttpParameter(field, value));
    }
    public void removeParameter(String field){
    	this.parameters.remove(field);
    }
    public void clearParameters(){
    	this.parameters.clear();
    }
    
    public String getParameterValue(String field) {
    	return this.parameters.get(field).getValue();
    }
    public void setParameterValue(String field, String value) {
        this.parameters.put(field, new HttpParameter(field, value));
    }
    
    public void addCookie(String field, String value) {
        this.cookies.put(field, new HttpParameter(field, value));
    }
    public void removeCookie(String field){
    	this.cookies.remove(field);
    }
    public void clearCookies(){
    	this.cookies.clear();
    }

    /*TODO: (?) utilizar biblioteca HttpComponents-Client do apache ao invés do
     * HttpURLConnection, pois ela permite encapsular várias coisas do HTTP.
     * Vide: http://hc.apache.org/ */
    public String sendGet(String url) throws IOException, ProtocolException{
        String urlWithParams = this.setParameters(url);
        URL obj = new URL(urlWithParams);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        setGetHeader(con);
        setCookies(con);

        System.out.println("\nSending 'GET' request to URL : " + obj.toString());

        StringBuffer response = receiveResponse(con);
        return response.toString();
    }

	private void setGetHeader(HttpURLConnection con) throws ProtocolException {
		con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
	}

    public String sendPost(String url) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        setPostHeader(con);
        setCookies(con);

        String urlParameters = this.parseParameters();

        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        
        sendPostRequest(con, urlParameters);

        StringBuffer response = receiveResponse(con);
        return response.toString();
    }

	private void setPostHeader(HttpURLConnection con) throws ProtocolException {
		//add request header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	}


	private void setCookies(HttpURLConnection con) {
		if(!this.cookies.isEmpty()){
			StringBuilder cookiesParam = new StringBuilder();

			Iterator<HttpParameter> iterator = this.cookies.values().iterator();
	        while(iterator.hasNext()) 
	        {
				cookiesParam.append(iterator.next().toString());
	    		if (iterator.hasNext()) {
	    		    cookiesParam.append("; ");
	    		}
	        }
	        
	        con.setRequestProperty("Cookie", cookiesParam.toString());
		}
	}

	private StringBuffer receiveResponse(HttpURLConnection con) throws IOException {
		int responseCode = con.getResponseCode();
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
		return response;
	}

	private void sendPostRequest(HttpURLConnection con, String urlParameters)
			throws IOException {
		// Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
	}

    private String setParameters(String url) {
        String result = url;
        if (this.parameters.size() > 0) {
            result += "?" + this.parseParameters();
        }
        return result;
    }

    private String parseParameters() {
        StringBuilder result = new StringBuilder();
        
        Iterator<HttpParameter> iterator = this.parameters.values().iterator();
        while(iterator.hasNext()) 
        {
        	result.append(iterator.next().toString());
    		if (iterator.hasNext()) {
    		    result.append("&");
    		}
        }

        return result.toString();
    }
}
