package househub.network;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class HouseRequester {
	public final static String MethodField = "method";
	public static final String MethodLogin = "login";
	public static final String MethodLogout = "logout";
	
	public static final String UsernameField = "username";
	public static final String PasswordField = "password";
	public static final String SessionField = "PHPSESSID";
	
	private String sessionId;
	private String serverUrl;
	/*FIXME: criar mecanismo de polimorfismo para permitir utilizar outros formatos de codificação
	 * (não apenas Json)*/
	private JsonAnswerParser parser;

	public HouseRequester(){
		this(null);
	}
	public HouseRequester(String serverUrl){
		this.serverUrl = serverUrl;
		this.sessionId = null;
		this.parser = new JsonAnswerParser();
	}
	

	public String getSessionId() {
		return this.sessionId;
	}
	public String getServerUrl() {
		return serverUrl;
	}
	public JsonAnswerParser getParser() {
		return parser;
	}
	
	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}
	public void setParser(JsonAnswerParser parser) {
		this.parser = parser;
	}
	
	public boolean isConnected(){
		return (serverUrl != null && sessionId != null);
	}

	public boolean connect(String user, String pass){
		return this.connect(user, pass,null);
	}
	public boolean connect(String user, String pass, String serverUrl){
		if(serverUrl!= null){
			this.serverUrl = serverUrl;
		}
		
		List<RequestParameter> loginParameters = makeLoginParameters(user,pass);
		RequestAnswer answer = this.requestMethod(MethodLogin, loginParameters);
		
		if(answer != null){
			Object contentObj = answer.getContent();
			if(answer.isSuccessful() && 
					contentObj != null && 
					contentObj instanceof Map<?, ?>)
			{
				Map<?, ?> content = (Map<?, ?>) answer.getContent();
				if(content.containsKey("phpsessid")){
					this.sessionId = (String) content.get("phpsessid");
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	private List<RequestParameter> makeLoginParameters(String user, String pass) {
		List<RequestParameter> parameters = new LinkedList<RequestParameter>();
		parameters.add(new RequestParameter(UsernameField,user));
		parameters.add(new RequestParameter(PasswordField,pass));
		
		return parameters;
	}
	
	public boolean disconnect(){
		if(this.isConnected()){
			RequestAnswer answer = this.requestMethod(MethodLogout);
			
			if(answer != null && answer.isSuccessful()){
				this.sessionId = null;
			}
		}
		return !isConnected();
	}

	public RequestAnswer requestMethod(String methodName){
		return this.requestMethod(methodName, null);
	}
	public RequestAnswer requestMethod(String methodName, List<RequestParameter> parameters){
		HttpConnection connection = new HttpConnection();
		
		if(sessionId != null){
			connection.addCookie(SessionField, this.sessionId);
		}
		
		connection.addParameter(HouseRequester.MethodField, methodName);
		if(parameters != null){
			for(RequestParameter param : parameters){
				connection.addParameter(param.getField(), param.getValue());
			}
		}

		RequestAnswer answer = null;
		if(this.serverUrl != null){
			try {
				answer = sendRequest(connection);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return answer;
		}
		
		return answer;
	}
	
	private RequestAnswer sendRequest(HttpConnection connection) throws IOException {
		String result = connection.sendPost(this.serverUrl);
		if(result !=null){
			return this.parser.parse(result);
		}
		return null;
	}
}
