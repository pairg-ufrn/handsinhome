package househub.appliances;


public class ViewDefinition {
	private String label;
	// TODO: permitir utilizar apenas o nome do ícone e não um PImage (remove
	// acoplamento)
	private String nameImage;
	private ViewComponents type;

	public enum ViewComponents {
		BUTTON, SLIDER, TEXT
	}

	public ViewDefinition() {
		this(ViewComponents.BUTTON, "");
	}

	public ViewDefinition(ViewComponents type, String name) {
		this(type, name, null);
	}

	public ViewDefinition(ViewComponents type, String name, String nameImage) {
		this.type = type;
		this.label = name;
		this.nameImage = nameImage;
	}

	public ViewComponents getType() {
		return type;
	}

	@Override
	public boolean equals(Object other) {
		if(this == other){
			return true;
		}
	    if (other == null || !this.getClass().equals(other.getClass())){
		    return false;
	    }
	    
    	ViewDefinition otherView = (ViewDefinition)other;
    	boolean isEqual = true;
    	isEqual = attrEqual(isEqual, this.getLabel(), otherView.getLabel());
    	isEqual = attrEqual(isEqual, this.getNameImage(), otherView.getNameImage());
    	isEqual = attrEqual(isEqual, this.getType(), otherView.getType());
    	
    	return isEqual;
    }
	private boolean attrEqual(boolean isEqual, Object attrThis, Object attrOther){
		if(attrThis != null && attrOther != null){
			return isEqual && (attrThis.equals(attrOther));
		}
		else{
			return isEqual && (attrThis == attrOther);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
    public int hashCode() { //gerado pelo eclipse
	    final int prime = 31;
	    int result = 1;

		result = incrementHash(prime, result, this.getLabel());
		result = incrementHash(prime, result, this.getNameImage());
		result = incrementHash(prime, result, this.getType());
	    return result;
    }
	int incrementHash(final int seed, final int currentResult, Object member){
		return seed * currentResult + (member == null ? 0 : member.hashCode());
	}
	
	public void setType(ViewComponents type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String name) {
		this.label = name;
	}

	public String getNameImage() {
		return nameImage;
	}

	public void setNameImage(String nameImage) {
		this.nameImage = nameImage;
	}

	public void set(ViewDefinition viewDefinition) {
	    this.label = viewDefinition.getLabel();
	    this.nameImage = viewDefinition.getNameImage();
	    this.type = viewDefinition.getType();
    }

}
