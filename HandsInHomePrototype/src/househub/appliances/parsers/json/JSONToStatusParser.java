package househub.appliances.parsers.json;

import househub.appliances.ApplianceStatus;
import househub.appliances.parsers.StatusParser;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class JSONToStatusParser {
    private StatusParser statusParser;
    
    public JSONToStatusParser(){
    	statusParser = new StatusParser();
    }
    
    public ApplianceStatus parseJSON(String json){
        Object jsonPureObj = JSONValue.parse(json);
        JSONObject jsonObj = (JSONObject) jsonPureObj;
        return parseJSON(jsonObj);
    }
    
    public ApplianceStatus parseJSON(JSONObject json){        
        return statusParser.parse(json);
    }
    
}
