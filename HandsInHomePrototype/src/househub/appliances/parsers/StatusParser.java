package househub.appliances.parsers;

import househub.appliances.ApplianceStatus;

import java.util.Map;

public class StatusParser {    
    private static final String VALUE_KEY = "value";
	private static final String NAME_KEY = "name";
	private static final String ID_KEY = "id";

	public ApplianceStatus parse(Map<?,?> statusEncoded){
        ApplianceStatus status = new ApplianceStatus();
        if(statusEncoded.containsKey(ID_KEY)){
            status.setId(((Number) statusEncoded.get(ID_KEY)).intValue());
        }
        
        if(statusEncoded.containsKey(NAME_KEY)){
            status.setName((String) statusEncoded.get(NAME_KEY));
        }
        
        //FIXME: nem todo status é inteiro!
        if(statusEncoded.containsKey(VALUE_KEY)){
            status.setValue(((Number) statusEncoded.get(VALUE_KEY)).intValue());
        }
        
        return status;
    }
    
}
