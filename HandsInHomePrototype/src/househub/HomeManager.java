package househub;

import househub.appliances.Appliance;
import househub.appliances.ApplianceService;

import java.util.Collection;

public abstract class HomeManager {

	//TODO: (?) rever instanciação padrão
//	private static HomeManager singleton = new RemoteHomeManager();//por padrão RemoteHomeManager
	//Mantido FakeHomeManager como padrão para compatibilidade com protótipos
	private static HomeManager singleton = new FakeHomeManager();//por padrão RemoteHomeManager
	public static HomeManager getInstance() {
		return singleton;
	}
	public static void setInstance(HomeManager manager){
		singleton = manager;
	}

	public abstract void executeApplianceService(Appliance selectedNode, ApplianceService selectedService);
	public abstract Appliance loadAppliance(int id);
	public abstract Appliance loadAppliance(Appliance node);
	public abstract Collection<Appliance> loadAppliances();
	public abstract boolean disconnect();
	public abstract boolean connect(String username, String password, String serverUrl);

}