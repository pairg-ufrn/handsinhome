package househub;

import househub.appliances.Appliance;
import househub.appliances.ApplianceService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Fachada para os serviços de manipulação da casa e comunicação com o 
 * sistema de domótica.
 * */
public class FakeHomeManager extends HomeManager{
	private List<Appliance> appliances;
	
	public FakeHomeManager(){
		appliances = this.makeFakeNodes();
	}
	
	@Override
	public Appliance loadAppliance(int id) {
		for(Appliance appliance : this.appliances){
			if(appliance.getId() == id){
				return appliance;
			}
		}
		return null;
	}
	@Override
	public Appliance loadAppliance(Appliance node) {
		if(node != null){
			return this.loadAppliance(node.getId());
		}
		return null;
	}
	@Override
	public Collection<Appliance> loadAppliances() {
		return this.appliances;
	}
	@Override
	public boolean disconnect() {
		return true;
	}
	@Override
	public boolean connect(String username, String password, String serverUrl) {
		return true;
	}
	
	@Override
	public void executeApplianceService(Appliance selectedNode, ApplianceService selectedService) {
		System.out.println("Executed service " + selectedService+ " for Node " + selectedNode );
	}

	/**
	 * TODO: loadNodes será feito comunicando-se com o core
	 * */
	private List<Appliance> makeFakeNodes() {

		List<Appliance> nodes = new ArrayList<Appliance>();

		Appliance node1 = new Appliance("Microondas da cozinha", "microondas.png");
		node1.addService(new ApplianceService("Cozinhar", "microondas.png"));
		node1.addService(new ApplianceService("Interromper","microondas.png"));
		node1.addService(new ApplianceService("Definir timer", "microondas.png"));
		node1.setId(1);
		nodes.add(node1);
//		ObjectNode node1 = new ObjectNode("Microondas", "microondas.png");
//		node1.addService(new ServiceNode("Abrir", "microondas.png"));
//		node1.addService(new ServiceNode("Fechar","microondas.png"));
//		node1.addService(new ServiceNode("Ver olho mágico", "microondas.png"));
//		node1.addService(new ServiceNode("Trancar", "microondas.png"));
//		node1.addService(new ServiceNode("Destrancar", "microondas.png"));
//		nodes.add(node1);

		Appliance node2 = new Appliance("Torradeira da cozinha", "torradeira.png");
		node2.addService(new ApplianceService("Ligar", "torradeira.png"));
		node2.addService(new ApplianceService("Desligar", "torradeira.png"));
		node2.addService(new ApplianceService("Torrar", "torradeira.png"));
		node2.setId(2);
		nodes.add(node2);

		Appliance node3 = new Appliance("Geladeira da cozinha", "geladeira.png");
		node3.addService(new ApplianceService("Ligar", "geladeira.png"));
		node3.addService(new ApplianceService("Desligar", "geladeira.png"));
		node3.setId(3);
		nodes.add(node3);

		Appliance node4 = new Appliance("Máquina de Lavar", "maquina de lavar.png");
		ApplianceService service = new ApplianceService("Ligar", "maquina de lavar.png");
//		service.addParameters(new Parameter(Parameters.BOOLEAN,
//				new ViewDefinition(ViewComponents.SLIDER, "Ligar")));
		node4.addService(service);
		node4.addService(new ApplianceService("Fechar", "maquina de lavar.png"));
		node4.setId(4);
		nodes.add(node4);

		Appliance node5 = new Appliance("Ventilador do quarto", "ventilador.png");
		node5.addService(new ApplianceService("Ligar", "ventilador.png"));
		node5.addService(new ApplianceService("Desligar", "ventilador.png"));
		node5.addService(new ApplianceService("Alterar Velocidade", "ventilador.png"));
		node5.setId(5);
		nodes.add(node5);

		String doorIcon = "doorIcon.png";
		Appliance node6 = new Appliance("Porta da sala", doorIcon);
		node6.addService(new ApplianceService("Fechar", doorIcon));
		node6.addService(new ApplianceService("Abrir", doorIcon));
		node6.setId(6);
		nodes.add(node6);
		
		String lampIcon = "lightIcon.png";
		Appliance node7 = new Appliance("Lâmpada da sala", lampIcon);
		node7.addService(new ApplianceService("Ligar", lampIcon));
		node7.addService(new ApplianceService("Desligar", lampIcon));
		node7.setId(7);
		nodes.add(node7);

		return nodes;
	}
}
