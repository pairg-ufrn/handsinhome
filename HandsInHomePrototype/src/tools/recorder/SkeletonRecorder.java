package tools.recorder;

import gestures.skeleton.Skeleton;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SkeletonRecorder {
	private ObjectOutputStream outputStream;

	private static final String errorSaveWithNoStream = "You should first start the Record by calling startRecord";
	private static final String errorEndWithNoBegin = errorSaveWithNoStream;

	public void startRecord(String filePath) throws FileNotFoundException, IOException{
		outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(filePath)));
	}
	public void startRecord(File file) throws FileNotFoundException, IOException{
		outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
	}
	
	public void saveFrame(Skeleton skeleton) throws IOException, IllegalStateException{
		if(this.outputStream != null){
			/**Skeleton é clonado, pois caso contrário, se um mesmo Skeleton for salvo em diferentes momentos do tempo,
			 * ao carregá-lo, será carregado apenas um objeto (todas as referências apontarão para o mesmo objeto)*/
			outputStream.writeObject(skeleton.clone());
		}
		else{
			throw new IllegalStateException(errorSaveWithNoStream);
		}
	}
	public void endRecord() throws IOException{
		if(outputStream != null){
			outputStream.flush();
			outputStream.close();
			outputStream = null;
		}
		else{
			throw new IllegalStateException(errorEndWithNoBegin);
		}
	}
}
