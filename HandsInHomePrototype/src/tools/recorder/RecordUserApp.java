package tools.recorder;

import gestures.skeleton.Skeleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;

import main.utils.Skeleton2DAppTemplate;
import processing.core.PApplet;

public class RecordUserApp extends Skeleton2DAppTemplate{	
	private static final long serialVersionUID = 7947423009234522950L;

	public static void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", RecordUserApp.class.getName() });
    }

	static Logger LOG = Logger.getLogger(RecordUserApp.class.getName() + ".LOG");
	private SkeletonRecorder recorder;
	static final String errorFileMsg = "Arquivo não pode ser criado, encerrando a aplicação";
	static final String errorFileNotFound = "Arquivo não pode ser criado, encerrando a aplicação";
	static final String errorFileStartRecord = "Ocorreu um erro ao iniciar a gravação do arquivo, encerrando a aplicação";
	
//	private boolean startRecord = false;
//	private boolean recording = false;


	Skeleton savingSkeleton;
	private String savedPath;
	
	public RecordUserApp(){
		super();
		this.recorder = new SkeletonRecorder();
		
		savingSkeleton = new Skeleton();
	}

	
	
	@Override
	public void setup(){
		// set system look and feel 
		 super.setup();
		setupLookAndFeel(); 
		this.savedPath = createFile().getAbsolutePath();
		try {
			this.recorder.startRecord(savedPath);
			
		} catch (FileNotFoundException e) {
			System.err.println(errorFileNotFound);
			LOG.log(Level.WARNING, errorFileNotFound, e);
			System.exit(2);
		} catch (IOException e) {
			System.err.println(errorFileStartRecord);
			LOG.log(Level.WARNING, errorFileStartRecord, e);
			System.exit(3);
		}
	}
	
	boolean record = false;
	boolean savedSkeleton = false;
	
	@Override
	public void draw(){
		super.draw();
		
		if(!savedSkeleton){
			trySaveSkeleton();
		}
		else{
			showSkeleton();
		}
	}

	boolean loadedSkeleton = false;
	List<Skeleton> skeletons = new LinkedList<Skeleton>();
	
	private void showSkeleton() {
		if(!loadedSkeleton){
			try {
				loadSkeletons();
			} 
			catch (Exception e) {
				System.err.println("Failed to load skeletons, exiting app.");
				e.printStackTrace();
				
				System.exit(3);
			}
			loadedSkeleton = true;
		}
		else{
			showSkeletonFrame();
		}
	}

	private void loadSkeletons() throws FileNotFoundException, IOException, IllegalStateException, ClassNotFoundException {
		SkeletonLoader loader = new SkeletonLoader();
		
		loader.startLoad(this.savedPath);
//		Skeleton loaded = loader.loadFrame();
//		while(loaded != null){
//			this.skeletons.add(loaded);
//			loaded = loader.loadFrame();
//		}
		boolean hasFinished = false;
		do{
			Skeleton loaded = loader.loadFrame();
			System.out.println("Loaded Skeleton " + loaded);
			if(loaded!=null){
				this.skeletons.add(loaded);
			}
			else{
				hasFinished = true;
			}
		}while(!hasFinished);
		
		System.out.println("Loaded " + this.skeletons.size() + " frames");
	}
	
	int index = 0;
	private void showSkeletonFrame() {
//		this.skelDrawer.drawSkeleton(this.g, this.skeletons.get(index));
		Skeleton showSkel = this.skeletons.get(index);
		this.drawSkeleton(showSkel);
		

		System.out.println("Show Skel " + showSkel + " at index " + index);
		
		index = (index + 1) % skeletons.size();
	}



	private void trySaveSkeleton() {
		if(gesturesEnable && this.context.getUsers().length > 0){
			
//			updater.updateSkeleton(this.context.getUsers()[0], savingSkeleton);
			savingSkeleton = this.userSkeleton;
			if(record){
				try {
					this.drawSkeleton(this.userSkeleton);
					recorder.saveFrame(this.userSkeleton);
					System.out.println("add Skeleton " + this.userSkeleton);
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void keyPressed(){
		if(key == 's' && !record){
			record = true;
			System.out.println("startRecord");
		}
		else if(key == 'e' && record){
			record = false;
		}
		else if(key == 'q'){
			record = false;
			try {
				this.recorder.endRecord();
				
				savedSkeleton = true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void stop(){
		try {
			this.recorder.endRecord();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.stop();
	}
	
	private File createFile() {
		this.createRecordsDir();
		File file = chooseFile(this.makeFilepath());
		if(file != null){
			try {
				file.createNewFile();
			} catch (IOException e) {
				System.err.println(errorFileMsg);
				LOG.log(Level.WARNING, errorFileMsg, e);
				System.exit(1);
			}
		}
		else{
			System.err.println(errorFileMsg);
			System.exit(0);
		}
		return file;
	}
	
	private File chooseFile(String filepath) {
		// create a file chooser
		final JFileChooser fc = new JFileChooser();
		fc.setSelectedFile(new File(filepath));
		// in response to a button click:
		int returnVal = fc.showSaveDialog(this);
		File file = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
		}

		return file;
	}

	private String makeFilename() {
		Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
	     
        int currentDay = localCalendar.get(Calendar.DATE);
        int currentMonth = localCalendar.get(Calendar.MONTH) + 1;
        int currentYear = localCalendar.get(Calendar.YEAR);
       
        int currentHour =  localCalendar.get(Calendar.HOUR);
        int currentMinute = localCalendar.get(Calendar.MINUTE);
        int currentSecond = localCalendar.get(Calendar.SECOND);
	
		return currentYear + "-"+currentMonth + "-"+currentDay + "_"+currentHour+":"+currentMinute+":"+currentSecond + ".skel";
	}

	private String recordsPath(){
		String dirPath = System.getProperty("user.dir");
		return dirPath + File.separator + "skeletonRecords";
	}
	private String makeFilepath(){
		return recordsPath() + File.separator+ this.makeFilename();
	}
	
	private boolean createRecordsDir(){
		File recordsDir = new File(recordsPath());
		if(!recordsDir.exists()){
			return recordsDir.mkdir();
		}
		return false;
	}
}
