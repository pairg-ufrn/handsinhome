package tests;

import gui.screens.AbstractScreen;
import gui.screens.NavigationEvent;
import gui.screens.Screen;
import gui.screens.ScreenManager;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import processing.core.PGraphics;

public class TestScreenManager {
	private ScreenManager screenManager;
	
	@Before
	public void setup(){
		screenManager = new ScreenManager();
		screenManager.addScreen(new StubScreen("screen1", ""));
		screenManager.addScreen(new StubScreen("screen2", ""));
	}

	@Test
	public void testDraw() {
		StubScreen screenToDraw = new StubScreen("toDraw", "");
		screenManager.addScreen(screenToDraw);
		screenManager.setCurrentScreen(screenToDraw.getTitle());
		screenManager.draw(null);
		Assert.assertEquals(1, screenToDraw.getDrawCount());
	}
	@Test
	public void testDrawMultiples() {
		StubScreen screenToDraw = new StubScreen("toDraw", "");
		screenManager.addScreen(screenToDraw);
		screenManager.setCurrentScreen(screenToDraw.getTitle());
		
		int drawCount = 10;
		for(int i=0; i < drawCount; ++i){
			screenManager.draw(null);
		}
		Assert.assertEquals(drawCount, screenToDraw.getDrawCount());
	}
	@Test
	public void testDrawAfterNavigate() {
		StubScreen screenToDraw = new StubScreen("toDraw", "");
		screenManager.addScreen(screenToDraw);
		screenManager.onNavigate(new NavigationEvent(screenManager.getCurrentScreen(),"toDraw"));
		screenManager.draw(null);
		Assert.assertEquals(1, screenToDraw.getDrawCount());
	}
//
//	public void testAddScreen() {
//		
//	}
//
//	public void testRemoveScreen() {
//	}

	@Test
	public void testSetCurrentScreen() {
		screenManager.setCurrentScreen("screen2");
		Assert.assertEquals("screen2", screenManager.getCurrentScreen().getTitle());
	}

	@Test
	public void testGetCurrentScreenDefault() {
		Assert.assertEquals("screen1", screenManager.getCurrentScreen().getTitle());
	}
	
	@Test
	public void testKeyPressed() {
		StubScreen screenToPress = new StubScreen("toPress", "");
		screenManager.addScreen(screenToPress);
		screenManager.setCurrentScreen(screenToPress.getTitle());
		screenManager.keyPressed('a', 100);

		Assert.assertEquals('a', screenToPress.getLastKey());
		Assert.assertEquals(100, screenToPress.getLastKeyCode());
	}
	
	@Test
	public void testOnNavigate() {
		Screen screenOrigin = new StubScreen("origin", "");
		screenManager.addScreen(screenOrigin);
		screenManager.setCurrentScreen(screenOrigin.getTitle());
		screenManager.onNavigate(new NavigationEvent(screenOrigin,"screen2"));
		Assert.assertEquals("screen2", screenManager.getCurrentScreen().getTitle());
	}
	@Test
	public void testOnNavigateMultiples() {
		Screen screenDestA = new StubScreen("destA", "");
		Screen screenDestB = new StubScreen("destB", "");
		screenManager.addScreen(screenDestA);
		screenManager.addScreen(screenDestB);

		testNavigation("screen2");
		testNavigation("destA");
		testNavigation("destB");
	}
	
	private void testNavigation(String dest){
		screenManager.onNavigate(new NavigationEvent(screenManager.getCurrentScreen(),dest));
		Assert.assertEquals(dest, screenManager.getCurrentScreen().getTitle());
	}
	
	class StubScreen extends AbstractScreen{
		char lastKey = '\0';
		int lastKeyCode = -1;
		int drawCount = 0;
		public StubScreen(String title, String nameIcon) {
			super(title, nameIcon);
		}

		@Override
		public void clearSetup() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setup() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void draw(PGraphics graphics) {
			++drawCount;
		}

		@Override
		protected void doLayout() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void keyPressed(char key, int keyCode){
			this.lastKey = key;
			this.lastKeyCode = keyCode;
		}

		public char getLastKey() {
			return lastKey;
		}

		public int getLastKeyCode() {
			return lastKeyCode;
		}

		public int getDrawCount() {
			return drawCount;
		}
		
	}
}
