package tests;

import junit.framework.Assert;

import org.junit.Test;

import processing.core.PVector;

public class TestProjection {
	static final PVector XAXIS = new PVector(1,0,0);
	
	private void testProjection(PVector vec, PVector axis, float expectedProjection) {
		Assert.assertEquals(expectedProjection, calculateProjection(vec,axis));
	}
	
	private float calculateProjection(PVector vec, PVector axis) {
		float projection = PVector.dot(vec, axis);
		System.out.println("vec: " + vec + " axis: " + axis + " proj: " + projection);
		return projection;
	}

	@Test
	public void testProjectionFrontX(){
		PVector vec = new PVector(0,20,0);
		testProjection(vec, XAXIS, 0);
	}
	@Test
	public void testProjectionBackX(){
		PVector vec = new PVector(0,-20,0);
		testProjection(vec, XAXIS, 0);
	}
	@Test
	public void testProjectionRightX(){
		PVector vec = new PVector(20,0,0);
		testProjection(vec, XAXIS, 20);
	}
	@Test
	public void testProjectionLeftX(){
		PVector vec = new PVector(-20,0,0);
		testProjection(vec, XAXIS, -20);
	}
	@Test
	public void testProjectionUpX(){
		PVector vec = new PVector(0,0,20);
		testProjection(vec, XAXIS, 0);
	}
	@Test
	public void testProjectionDownX(){
		PVector vec = new PVector(0,0,-20);
		testProjection(vec, XAXIS, 0);
	}
	@Test
	public void testProjectionDiagonalX(){
		PVector vec = new PVector(20,0,-20);
		testProjection(vec, XAXIS, 20);
	}
	
//TODO: (?) testar projeção com ângulos inclinados
//	@Test
//	public void testProjectionFrontRotatedAxis(){
//		PVector axis = new PVector(10,10,10);
//		axis.normalize();
//		
//		testProjection(vec, new PVector(10,20,10), 0);
//	}
//	@Test
//	public void testProjectionBackRotatedAxis(){
//		PVector vec = new PVector(0,-20,0);
//		testProjection(vec, XAXIS, 0);
//	}
//	@Test
//	public void testProjectionRightRotatedAxis(){
//		PVector vec = new PVector(20,0,0);
//		testProjection(vec, XAXIS, 20);
//	}
//	@Test
//	public void testProjectionLeftRotatedAxis(){
//		PVector vec = new PVector(-20,0,0);
//		testProjection(vec, XAXIS, -20);
//	}
//	@Test
//	public void testProjectionUpRotatedAxis(){
//		PVector vec = new PVector(0,0,20);
//		testProjection(vec, XAXIS, 0);
//	}
//	@Test
//	public void testProjectionDownRotatedAxis(){
//		PVector vec = new PVector(0,0,-20);
//		testProjection(vec, XAXIS, 0);
//	}
//	@Test
//	public void testProjectionDiagonalRotatedAxis(){
//		PVector vec = new PVector(20,0,-20);
//		testProjection(vec, XAXIS, 20);
//	}
}
