package tests.utils;

import gui.components.Component;
import gui.components.Container;

import java.awt.Dimension;
import java.awt.Point;

import junit.framework.Assert;

public class TestListLayoutHelper {
	
	public void addComponents(Container container, int count) {
		for(int i = 0; i < count; ++ i){
		    container.addComponent(new EllipseComponent(0 , 0, 25, 25));
		}
    }
	public void addComponents(Container container, Dimension[] dimensions) {
	    for(Dimension dim : dimensions){
	    	container.addComponent(new EllipseComponent(0,0,(int)dim.getWidth(), (int)dim.getHeight()));
	    }
    }

	public void testPosition(Container container, Point[] expectedPositions){
		int index = 0;
		for(Component comp : container .getComponents()){
			Assert.assertEquals("X position in component " + index + " was not correct"
					, (int)expectedPositions[index].getX(),comp.getX());
			Assert.assertEquals("Y position in component " + index + " was not correct"
					, (int)expectedPositions[index].getY(),comp.getY());
			++index;
		}
	}
}
