package tests.utils;

import gui.components.AbstractComponent;
import processing.core.PGraphics;

public class EllipseComponent extends AbstractComponent{
  private String text;
  public EllipseComponent(int X,int Y, int Width, int Height){
    this("",X,Y,Width,Height);
  }
  public EllipseComponent(String Text, int X,int Y, int Width, int Height){
    super(X,Y,Width,Height);
    this.text = Text;
  }
  
  public void draw(PGraphics graphics){
    //graphics.ellipse(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    graphics.pushStyle();
    graphics.ellipse(this.getX() + this.getWidth()/2, this.getY() + this.getHeight()/2, this.getWidth(), this.getHeight());
    graphics.fill(255,0,0);
    graphics.text(this.text, this.getX(), this.getY()+(this.getHeight()/2));
    graphics.popStyle();
  }
  
  public String toString(){
    return super.toString() + "["+this.text+"]";
  }
}
