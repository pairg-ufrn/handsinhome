package tests.utils;

import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;

import java.util.Map;

import processing.core.PVector;

public class SkeletonCreator {
	public enum Joint{
		NECK, 
		HEAD, 
		TORSO, 
		RIGHT_SHOULDER, LEFT_SHOULDER, 
		RIGHT_ELBOW,    LEFT_ELBOW, 
		RIGHT_HAND,     LEFT_HAND, 
		RIGHT_HIP,      LEFT_HIP,
		RIGHT_KNEE,     LEFT_KNEE,
		RIGHT_FOOT,     LEFT_FOOT
		
	}
	public Skeleton createSkeletonBase() {
		Skeleton skel = new Skeleton();
		
		setJointPositions();
		
//		skel.setPart(SkeletonPart.Head, bodyPart)
		return null;
	}
	
    private void setJointPositions() {
		
	}

	Map<Joint, PVector> jointPos;
    
  
	
	public void updateSkeleton(int userId,Skeleton skeleton){
//		this.updatePart(userId, skeleton, SkeletonPart.Head
//				, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_HEAD);
//		this.updatePart(userId, skeleton, SkeletonPart.Chest
//				, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_TORSO);
//		
//		this.updatePart(userId, skeleton, SkeletonPart.RightShoulder
//				, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
//		this.updatePart(userId, skeleton, SkeletonPart.RightArm
//				, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
//		this.updatePart(userId, skeleton, SkeletonPart.RightForearm
//				, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND);
//		this.updatePart(userId, skeleton, SkeletonPart.RightThigh
//				, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE);
//		this.updatePart(userId, skeleton, SkeletonPart.RightLeg
//				, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT);
//		
//		this.updatePart(userId, skeleton, SkeletonPart.LeftShoulder
//				, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
//		this.updatePart(userId, skeleton, SkeletonPart.LeftArm
//				, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
//		this.updatePart(userId, skeleton, SkeletonPart.LeftForearm
//				, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND);
//		this.updatePart(userId, skeleton, SkeletonPart.LeftThigh
//				, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE);
//		this.updatePart(userId, skeleton, SkeletonPart.LeftLeg
//				, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT);
//		
//		/* **************************Casos Especiais************************** */
//		//Mãos não tem direção (inicio e fim são no mesmo ponto)
//		this.updatePart(userId, skeleton, SkeletonPart.RightHand
//				, SimpleOpenNI.SKEL_RIGHT_HAND, SimpleOpenNI.SKEL_RIGHT_HAND);
//		this.updatePart(userId, skeleton, SkeletonPart.LeftHand
//				, SimpleOpenNI.SKEL_LEFT_HAND, SimpleOpenNI.SKEL_LEFT_HAND);

		//O openNi nao tem uma Joint que represente o fim do abdomen-> calcula o ponto central dos quadris
		this.updateAbdomen(userId, skeleton);
		//A parte que representa a posição e direção do corpo
		this.updateBody(userId, skeleton);
	}

	/**Pré-requisito: o método deve ser chamado após as outras posições do corpo estarem definidas.*/
	private void updateBody(int userId, Skeleton skeleton) {
		PVector bodyPos = new PVector();
		//utiliza o torson como centro do corpo
//		openNi.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,bodyPos);
		PVector bodyEnd = PVector.add(bodyPos, skeleton.getFrontDirection());
		
		skeleton.setPart(SkeletonPart.Body, new BodyPart(bodyPos, bodyEnd));
	}

	private void updateAbdomen(int userId, Skeleton skeleton) {		
		PVector torso = new PVector();
		PVector abdomen = new PVector();
	    PVector leftHip = new PVector();
	    PVector rightHip = new PVector();

//		float confidence = openNi.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,torso);
//	    confidence 		+= openNi.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_HIP,leftHip);
//	    confidence 		+= openNi.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HIP,rightHip);
	    
	    //Posição abdomen) é a média dos dois quadris
	    abdomen = PVector.add(leftHip, rightHip);
	    abdomen.div(2);

    	//Atualizar posições
	    skeleton.setPart(SkeletonPart.Abdomen,  new BodyPart(torso,abdomen));
	    skeleton.setPart(SkeletonPart.RightHip, new BodyPart(abdomen, rightHip));
	    skeleton.setPart(SkeletonPart.LeftHip,  new BodyPart(abdomen, leftHip));
	}

	private void updatePart(Skeleton skeleton, SkeletonPart part, int startJoint, int endJoint) {

		BodyPart bodyPart = new BodyPart();
		PVector startPos = new PVector();
		PVector endPos = new PVector();
		
		if(part != SkeletonPart.Abdomen){
//			float confidence =  openNi.getJointPositionSkeleton(userId,startJoint,startPos);
//				  confidence += openNi.getJointPositionSkeleton(userId,endJoint,endPos);
//				  confidence = confidence/2;
			bodyPart.setPosition(startPos, endPos);
		}

		//FIXME: inserir ou não valores com baixa confiança??
		skeleton.setPart(part, bodyPart);
//		if(confidence > threshold){
//			skeleton.setPart(part, bodyPart);
//		}
//		else{
//			skeleton.setPart(part, null);
//		}
	}
}
