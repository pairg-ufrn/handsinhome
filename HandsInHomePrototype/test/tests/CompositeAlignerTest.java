package tests;

import gui.components.Component;
import gui.components.CompositeAligner;
import gui.components.HorizontalAligner;
import gui.components.HorizontalAlignment;
import gui.components.VerticalAligner;
import gui.components.VerticalAlignment;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import tests.utils.EllipseComponent;

public class CompositeAlignerTest {
	private CompositeAligner aligner;
	
	@Before
	public void setUp() throws Exception {
		aligner = new CompositeAligner();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testSetHorizontalAligner() {
		HorizontalAligner horAligner = new HorizontalAligner(HorizontalAlignment.Right);
		aligner.setHorizontalAligner(horAligner);
		Assert.assertEquals(horAligner, aligner.getHorizontalAligner());
	}

	@Test
	public final void testSetHorizontalAlignment() {
		aligner.setHorizontalAlignment(HorizontalAlignment.Left);
		Assert.assertEquals(HorizontalAlignment.Left, aligner.getHorizontalAlignment());
	}

	@Test
	public final void testSetVerticalAligner() {
		VerticalAligner vertAligner = new VerticalAligner(VerticalAlignment.Bottom);
		aligner.setVerticalAligner(vertAligner);
		Assert.assertEquals(vertAligner, aligner.getVerticalAligner());
	}

	@Test
	public final void testSetVerticalAlignment() {
		aligner.setVerticalAlignment(VerticalAlignment.Top);
		Assert.assertEquals(VerticalAlignment.Top, aligner.getVerticalAlignment());
	}


	@Test
	public final void testAlignComponentPointRightBottom() {
		aligner.setAlignment(HorizontalAlignment.Right, VerticalAlignment.Bottom);
		Component comp = new EllipseComponent(0,0,20,20);
		aligner.align(comp, 50, 50);
		assertLocation(comp, 30,30);
	}
	@Test
	public final void testAlignComponentPointLeftCenter() {
		aligner.setAlignment(HorizontalAlignment.Left, VerticalAlignment.Center);
		Component comp = new EllipseComponent(0,0,20,20);
		aligner.align(comp, 50, 50);
		assertLocation(comp, 50,40);
	}
	@Test
	public final void testAlignComponentPointCenterTop() {
		aligner.setAlignment(HorizontalAlignment.Center, VerticalAlignment.Top);
		Component comp = new EllipseComponent(0,0,20,20);
		aligner.align(comp, 50, 50);
		assertLocation(comp, 40,50);
	}

	@Test
	public final void testAlignComponentArea() {
		aligner.setAlignment(HorizontalAlignment.Center, VerticalAlignment.Center);
		Component comp = new EllipseComponent(0,0,20,20);
		aligner.align(comp, 50, 50,50,50);
		assertLocation(comp, 65,65);
	}
	
	@Test
	public final void testAlignComponentArea2() {
		aligner.setAlignment(HorizontalAlignment.Center, VerticalAlignment.Center);
		Component comp = new EllipseComponent(0,0, 40, 20);
		aligner.align(comp, 0,0,100, 250);
		assertLocation(comp, 30,115);
	}
	
	@Test
	public final void testAlignComponentArea3() {
		aligner.setAlignment(HorizontalAlignment.Center, VerticalAlignment.Center);
		Component comp = new EllipseComponent(0,0, 700, 220);
		aligner.align(comp, 0,0,1100,600);
		assertLocation(comp, 200,190);
	}

	private void assertLocation(Component comp, int expectedX, int expectedY) {
	    Assert.assertEquals("Posição X do componente " + comp + " estava incorreta.",
	    		expectedX,comp.getX());
	    Assert.assertEquals("Posição Y do component" + comp + " estava incorreta.",
	    		expectedY,comp.getY());
    }
}
