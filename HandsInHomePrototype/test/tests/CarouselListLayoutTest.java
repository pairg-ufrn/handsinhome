package tests;

import gui.components.Border;
import gui.components.Component;
import gui.components.Container;
import gui.components.list.CarouselListLayout;
import gui.components.list.HorizontalListLayout;
import gui.components.list.ListLayout;
import gui.components.list.ListSelection;
import gui.components.list.ListView;
import gui.components.list.VerticalListLayout;

import java.awt.Dimension;
import java.util.Iterator;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import processing.core.PApplet;
import tests.utils.EllipseComponent;

@SuppressWarnings("serial")
public class CarouselListLayoutTest extends PApplet{
	public static void main(String[] args){
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", CarouselListLayoutTest.class.getName() });
    }
	
	private ListView container;
	private CarouselListLayout layout;
	
	@Override
	public void setup(){
		this.size(640, 480);
		
		this.container = new ListView(0, height/2);		
		addComponents(container, 5);
		
		layout = new CarouselListLayout(50,50);
	}

	@Override
	public void draw(){
		background(220);
		this.layout.setTypeSelection(ListSelection.Last);
		drawContainer(30,50, new HorizontalListLayout(new Border(0,0,15,15)));
		this.layout.setTypeSelection(ListSelection.Middle);
		drawContainer(60,150, new VerticalListLayout(new Border(10,10,0,0)));
//		drawContainer(30,this.height /2		, new HorizontalListLayout());
//		drawContainer(30,this.height /2 + 50, new HorizontalListLayout());
	}

	private void drawContainer(int x, int y, ListLayout listLayout)
	{
	    this.container.setLocation(x,y);
	    this.layout.setListLayout(listLayout);
		this.layout.doLayout(container, null);
		container.draw(g);
		
		drawComponentsBox(x,y,container.getComponents(), layout.getComponentDimension());
		
		g.pushStyle();
			g.noFill();
			g.rect(x, y, container.getWidth(), container.getHeight());			
		g.popStyle();
    }

	private void drawComponentsBox(int xOrigin, int yOrigin
			, Iterable<Component> components, Dimension compDimension) 
	{
//    	int x = xOrigin ;
//    	int y = yOrigin ;
//
//    	int selectedIndex = this.layout.getTypeSelection().getSelected(this.container);
//    	int index = 0;
//    	Iterator<Component> iterator = components.iterator();
//	    while( iterator.hasNext()) {
//	        iterator.next();
//	        
//	        int width = (int) compDimension.getWidth() * (index != selectedIndex? 1 : 2);
//	    	int height = (int) compDimension.getHeight() * (index != selectedIndex? 1 : 2);
//	    	this.g.pushStyle();
//	    	this.g.noFill();
//	    	
//	    	this.g.stroke(255,x,y);
//	    		this.g.rect(x, y, width, height);
//	    	this.g.popStyle();
//	    	
//	    	x += width;
//	    	++index;
//        }
    }

	@Before
	public void setUp() throws Exception {
		container = new ListView();
		addComponents(container, 10);
		layout = new CarouselListLayout(50,50);
	}

	private void addComponents(Container container, int count) {
		for(int i = 0; i < count; ++ i){
		    container.addComponent(new EllipseComponent(0 , 0, 25, 25));
		}
    }


	@Test
	public final void testSetListLayoutHorizontal() {
		this.layout.setListLayout( new HorizontalListLayout());
		Assert.assertTrue(layout.getListLayout() instanceof HorizontalListLayout);
		
		this.layout.doLayout(container, null);
		//Garantir sequencia (todo componente está mais a direita que o anterior)
		Iterator<Component> iterator = container.getComponents().iterator();
		if(iterator.hasNext()){
			int previousX = iterator.next().getX();
			while(iterator.hasNext()){
				int currentX = iterator.next().getX();
				Assert.assertTrue(currentX > previousX);
				previousX = currentX;
			}
		}
	}
	@Test
	public final void testSetListLayoutVertical() {
		this.layout.setListLayout( new VerticalListLayout());
		Assert.assertTrue(layout.getListLayout() instanceof VerticalListLayout);
		
		this.layout.doLayout(container, null);
		
		Iterator<Component> iterator = container.getComponents().iterator();
		if(iterator.hasNext()){
			int previousY = iterator.next().getY();
			while(iterator.hasNext()){
				int currentY = iterator.next().getY();
				Assert.assertTrue(currentY > previousY);
				previousY = currentY;
			}
		}
	}

	@Test
	public final void testSetTypeSelectionFirst() {
		this.layout.setTypeSelection(ListSelection.First);
		Assert.assertEquals(layout.getTypeSelection(), ListSelection.First);
		this.layout.doLayout(this.container, null);
		
		//garantir que primeiro é o maior
		Component firstComponent = this.container.getComponent(0);
		
		assertSelectedIsBigger(firstComponent);
	}
	@Test
	public final void testSetTypeSelectionMiddle() {
		this.layout.setTypeSelection(ListSelection.Middle);
		Assert.assertEquals(layout.getTypeSelection(), ListSelection.Middle);
		this.layout.doLayout(this.container, null);
		
		//garantir que o componente do meio é o maior
		Component middleComp = this.container.getComponent(container.getComponentCount()/2);
		
		assertSelectedIsBigger(middleComp);
	}
	@Test
	public final void testSetTypeSelectionLast() {
		this.layout.setTypeSelection(ListSelection.Last);
		Assert.assertEquals(layout.getTypeSelection(), ListSelection.Last);
		this.layout.doLayout(this.container, null);
		
		//garantir que primeiro é o maior
		Component lastComponent = this.container.getComponent(container.getComponentCount() -1);
		
		assertSelectedIsBigger(lastComponent);
	}


	private void assertSelectedIsBigger(Component selected) {
	    for(Component comp : this.container.getComponents()){
			if(comp !=  selected){
				Assert.assertTrue(selected.getWidth() > comp.getWidth());
				Assert.assertTrue(selected.getHeight() > comp.getHeight());
			}
		}
    }
	@Test
	public final void testSetComponentDimension() {
		this.layout.setTypeSelection(ListSelection.First);
		
		Dimension expectedDimension = new Dimension(23,26);
		this.layout.setComponentDimension(expectedDimension);

		Assert.assertEquals(expectedDimension , layout.getComponentDimension());
		
		this.layout.doLayout(this.container, null);
		
		//selecionado tem tamanho diferente
		Component selected = this.container.getComponent(0);
		assertComponentDimensions(container, selected, expectedDimension);
	}
	
	private void assertComponentDimensions(Container container, Component selected, 
			Dimension expectedDimension) 
	{
	    for(Component comp : container.getComponents()){
	    	if(comp != selected){
	    		Assert.assertEquals((int)expectedDimension.getWidth(), comp.getWidth());
	    		Assert.assertEquals((int)expectedDimension.getHeight(), comp.getHeight());
	    	}
	    	else{
	    		Assert.assertEquals((int)expectedDimension.getWidth() * 2, comp.getWidth());
	    		Assert.assertEquals((int)expectedDimension.getHeight() * 2, comp.getHeight());
	    	}
	    }
    }
}
