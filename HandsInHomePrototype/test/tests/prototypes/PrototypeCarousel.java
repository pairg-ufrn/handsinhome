package tests.prototypes;

import gui.components.ComponentAligner;
import gui.components.CompositeAligner;
import gui.components.HorizontalAlignment;
import gui.components.ImageManager;
import gui.components.VerticalAlignment;
import gui.components.list.CarouselListContainer;
import gui.home.ApplianceView;
import househub.FakeHomeManager;
import househub.HomeManager;
import househub.appliances.Appliance;

import java.util.Collection;

import processing.core.PApplet;

public class PrototypeCarousel extends PApplet {
	private static final long serialVersionUID = 1L;
	
	CarouselListContainer container;

	static public void main(String args[]) {
		PApplet.main(new String[] { "--bgcolor=#ECE9D8",
				PrototypeCarousel.class.getName() });
	}

	public PrototypeCarousel() {
	}

	public void setup() {
		this.size(1100, 600);
		ImageManager.getInstance().setPApplet(this);
		

		container = new CarouselListContainer(20, this.height/2, this.width - 100, 100);
		container.setComponentDimension(100,100);
		HomeManager.setInstance(new FakeHomeManager());
		Collection<Appliance> appliances = HomeManager.getInstance().loadAppliances();
		
		for(Appliance app : appliances){
//			ApplianceView appView = new ApplianceView(app);
			Appliance copy = (Appliance) app.clone();
			copy.setLabel(copy.getLabel() + "_copy");
			
			container.addComponent(new ApplianceView(app));
//			container.addComponent(new ObjectNodeView(copy,0,0,100,100));
		}
		
		ComponentAligner aligner = new CompositeAligner(HorizontalAlignment.Center, VerticalAlignment.Center);

		aligner.align(container, 0, 0, width, height/2);
		System.out.println("container Location: [" +
				container.getX() +","+ container.getY() + "]");
		System.out.println("container Dimension: [" + container.getWidth() 
				+","+ container.getHeight() + "]");
		System.out.println("app Dimension: [" + this.width 
				+","+ this.height + "]");
	}

	public void draw() {
		background(220);
		this.container.draw(this.g);
	}

	public void keyPressed() {
		if(key == CODED){
			if(keyCode == RIGHT){
				this.container.selectNext();
				System.out.println("NEXT to " + container.getSelectedIndex());
			}
			else if(keyCode == LEFT){
				this.container.selectPrevious();
				System.out.println("PREVIOUS to " + container.getSelectedIndex());
			}
		}
	}
}
