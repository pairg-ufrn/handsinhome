*********************************README*****************************************
Para executar o sistema será necessário instalar anteriormente o OpenNI e os drivers
do Kinect (ou outro dispositivo compatível*), de acordo com o seu sistema. 
Para isso, consulte: http://code.google.com/p/simple-openni/wiki/Installation_PreOpenNI2
Não será necessário, entretanto, instalar o SimpleOpenNI nem o Processing.

Após terminada a instalação, utilize os arquivos gHome.sh (no Linux) e gHome.bat (no 
Windows) para iniciar a aplicação.
Garanta antes que estes arquivos estão com as permissões de execução definidas
para o seu usuário (isso pode não ser necessário no Windows).

Ao iniciar a aplicação, caso o Kinect (ou dispositivo compatível*) seja detectado,
será exibido um pequeno quadrado no canto inferior esquerdo da aplicação com a imagem
capturada pelo dispositivo.
Quando o dispositivo detectar a presença do usuário, será exibido um "esqueleto"
sobre a imagem do usuário. A partir daí, ele pode realizar os gestos para interagir
com a aplicação (consulte documentação para listagem dos gestos definidos para uso
com a aplicação).

- A distância recomendada para uso é de pelo menos 2 metros do aparelho.
- De acordo com as definições de hardware do sistema, podem existir alguns problemas
 no uso da aplicação.

* Para outros dispositivos compatíveis, consulte: http://www.openni.org/3d-sensors/

